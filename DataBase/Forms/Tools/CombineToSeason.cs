﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using DataBase.Source;
using DataBase.Source.Data;
using DataBase.Forms.Main;
using DataBase.Forms.DataManegment;
using DataBase.Forms.Tools.CustomSelectors;

namespace DataBase.Forms.Tools
{
    public partial class CombineToSeason : Form
    {
        // TODO : Refactor This, Its functoning but still needs a little work and cleaning up But the prototype fase is over


        private SqlGeneral.Series _sqlSeries = new SqlGeneral.Series();
        private SerieData _serieDataStruct;

    
        private string _tableName;

        /// <summary>
        /// This opens the Combine to season form
        /// </summary>
        /// <param name="tableName">The table name that we need to edit</param>
        public CombineToSeason(string tableName)
        {
            InitializeComponent();


            _tableName = tableName;

            _sqlSeries.LoadTable(tableName, dataGridView1);
        }


        /// <summary>
        /// Active Search searches true the data grid view and of something does not match then hide it.
        /// </summary>
        /// <param name="dataGridView">The Data Grid View To search True</param>
        /// <param name="search">The Search Textbox</param>
        private void SearchDataGridView(DataGridView dataGridView, TextBox search)
        {
            if (dataGridView.Rows.Count != 0)
            {
                CurrencyManager currencyManager = (CurrencyManager)BindingContext[dataGridView.DataSource];

                // If the textbox is empty then show everything.
                if (!String.IsNullOrEmpty(search.Text.ToString()))
                {
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        foreach (DataGridViewCell cell in row.Cells)
                        {
                            // Makeing sure that the value of the cell is not null
                            if (cell.Value != null)
                            {
                                // If the cell contains a letter that is the same then show it the rest hide
                                if (cell.Value.ToString().ToLower().Contains(search.Text.ToLower()))
                                {
                                    currencyManager.SuspendBinding();
                                    row.Visible = true;
                                    currencyManager.ResumeBinding();
                                    break;
                                }
                                else
                                {
                                    currencyManager.SuspendBinding();
                                    row.Visible = false;
                                    currencyManager.ResumeBinding();
                                }
                            }
                        }
                    }
                }
                else
                {
                    // Show everything when the textbox is empty
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        currencyManager.SuspendBinding();
                        row.Visible = true;
                        currencyManager.ResumeBinding();
                    }
                }
            }
        }



        // Combine to First
        private void button1_Click(object sender, EventArgs e)
        {
            List<int> selectedDataGridViewRowIndex = new List<int>();
            List<int> selectedRowIds = new List<int>();
            List<string> selectedrowNames = new List<string>();
            string selectedIdsStringFormat = "";
            string selectedNamesStringFormat = "";
            bool haveDeletedSomething = false;

            // Getting the row ID by the selected cell
            foreach (DataGridViewCell cell in dataGridView1.SelectedCells)
            {
                // Checking if that row index is not already in the list
                if (!selectedDataGridViewRowIndex.Contains(cell.RowIndex))
                {
                    // Add the row index to the list
                    selectedDataGridViewRowIndex.Add(cell.RowIndex);
                }
            }

            // Checking each row index and getting the id of the row
            foreach (int index in selectedDataGridViewRowIndex)
            {
                // Selecting the whole row
                DataGridViewRow row = dataGridView1.Rows[index];

                // Getiing the value at Id
                selectedRowIds.Add((int)row.Cells["Id"].Value);

                // Getting the names and storing them in a array
                selectedrowNames.Add(row.Cells["Name"].Value.ToString());
            }

            // Reversing the arrays
            selectedRowIds.Reverse();
            selectedrowNames.Reverse();
            selectedDataGridViewRowIndex.Reverse();

            // Joining the arrays to make one string
            selectedIdsStringFormat = String.Join(", ", selectedRowIds);
            selectedNamesStringFormat = String.Join(", ", selectedrowNames);

            // Asking the question if your sure you want to delete this and comebine then in to the first value
            DialogResult result = MessageBox.Show("Are you sure you want to combine this: " + selectedIdsStringFormat, "Do you want to combine", MessageBoxButtons.YesNo);

            // Yes than check if all the names are the same if not than ask again but this time with the names
            if (result == DialogResult.Yes)
            {
                bool areNamesTheSame = false;

                // Checking each name in the array
                foreach (string name in selectedrowNames)
                {
                    // If the name does not match the name in the first element than Return false Else contue
                    if (name.ToLower() != selectedrowNames[0].ToLower() || !name.ToLower().Contains(selectedrowNames[0].ToLower()))
                    {
                        areNamesTheSame = true;
                        break;
                    }
                }

                // The names do not all match
                if (areNamesTheSame == true)
                {
                    // Ask again if you sure you wanted to delete this. Also showing the names
                    DialogResult areYouSureDialogResult = MessageBox.Show(
                        "Are you really sure you wnat ot delete thease? : " + selectedNamesStringFormat,
                        "Are you really sure",
                        MessageBoxButtons.YesNo
                    );

                    // If he is sure than delete and bring them to a single row
                    if (areYouSureDialogResult == DialogResult.Yes)
                    {
                        // Selecting each Id except the first one and deleteing them
                        foreach (int id in selectedRowIds.Skip(1))
                        {
                            // Deleteing the row indexes
                            _sqlSeries.RemoveFromTable(_tableName, id);

                            // Now we are sure we have deleted someting
                            haveDeletedSomething = true;
                        }
                    }
                }
                else
                {
                    // Else just delete becuase the names already match
                    foreach (int id in selectedRowIds.Skip(1))
                    {
                        // Deleteing the row indexes
                        _sqlSeries.RemoveFromTable(_tableName, id);

                        // Now we are sure we have deleted someting
                        haveDeletedSomething = true;
                    }
                }

                // If it has deleted something than create a new datastruct with the new infomation
                if (haveDeletedSomething == true)
                {
                    #region CreatingStruct

                    // Createing the datastruct that has to go the the Edit form or that will be used to change the database entry
                    _serieDataStruct.Name = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Name"].Value.ToString();
                    _serieDataStruct.Studio = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Studio"].Value.ToString();
                    _serieDataStruct.Episode = "All";
                    _serieDataStruct.Season = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Season"].Value.ToString();
                    _serieDataStruct.Size = Convert.ToInt32(dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Size"].Value);
                    _serieDataStruct.Quality = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Quality"].Value.ToString();
                    _serieDataStruct.Lenght = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Lenght"].Value.ToString();
                    _serieDataStruct.Genre = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Genre"].Value.ToString();
                    _serieDataStruct.Genre1 = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Genre1"].Value.ToString();
                    _serieDataStruct.Language = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Language"].Value.ToString();
                    _serieDataStruct.CheckNewLink = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["CheakNewlink"].Value.ToString();
                    _serieDataStruct.NextSeasonData = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Nextseasondate"].Value.ToString();
                    _serieDataStruct.Custom = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Custom"].Value.ToString();
                    _serieDataStruct.Custom1 = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Custom1"].Value.ToString();
                    _serieDataStruct.Location = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Location"].Value.ToString();
                    _serieDataStruct.PictureLocation = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["PictureLocation"].Value.ToString();
#endregion
                }

                // Asking if you want to edit the entry
                DialogResult doYouWantToEditDialogResult = MessageBox.Show("Do you also want to edit this row", "", MessageBoxButtons.YesNo);

                // Ask if you want to edit the row that was not deleted. Only ask if it has deleted something
                if (doYouWantToEditDialogResult == DialogResult.Yes && haveDeletedSomething == true)
                {                  
                    // Getting the edit form
                    Edit edit = new Edit(4, _tableName, selectedRowIds.First(), _serieDataStruct);

                    // Showing the edit form and closeing this form
                    DialogResult editDialogResult = edit.ShowDialog();

                    // Makeing sure that the new data has been enterd in the database
                    if (editDialogResult == DialogResult.OK)
                    {
                        this.DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        // If the database was not edited than edit it here
                        _sqlSeries.UpdateRowInTable(_tableName, selectedRowIds.First(), _serieDataStruct);
                        this.DialogResult = DialogResult.OK;
                    }

                }
                else if (doYouWantToEditDialogResult == DialogResult.No && haveDeletedSomething == true)
                {
                    // Now edit the row
                    _sqlSeries.UpdateRowInTable(_tableName, selectedRowIds.First(), _serieDataStruct);
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    // Do nothing
                }
            }
        }

        // Cancel
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }




        // combine to Custom
        private void button3_Click(object sender, EventArgs e)
        {
            List<int> selectedDataGridViewRowIndex = new List<int>();
            List<int> selectedRowIds = new List<int>();
            List<string> selectedrowNames = new List<string>();
            string selectedIdsStringFormat = "";
            string selectedNamesStringFormat = "";
            bool haveDeletedSomething = false;
            int rowNotToDelete;

            // Getting the row ID by the selected cell
            foreach (DataGridViewCell cell in dataGridView1.SelectedCells)
            {
                // Checking if that row index is not already in the list
                if (!selectedDataGridViewRowIndex.Contains(cell.RowIndex))
                {
                    // Add the row index to the list
                    selectedDataGridViewRowIndex.Add(cell.RowIndex);
                }
            }

            // Checking each row index and getting the id of the row
            foreach (int index in selectedDataGridViewRowIndex)
            {
                // Selecting the whole row
                DataGridViewRow row = dataGridView1.Rows[index];

                // Getiing the value at Id
                selectedRowIds.Add((int)row.Cells["Id"].Value);

                // Getting the names and storing them in a array
                selectedrowNames.Add(row.Cells["Name"].Value.ToString());
            }

            // Reversing the arrays
            selectedRowIds.Reverse();
            selectedrowNames.Reverse();
            selectedDataGridViewRowIndex.Reverse();


            // Joining the arrays to make one string
            selectedIdsStringFormat = String.Join(", ", selectedRowIds);
            selectedNamesStringFormat = String.Join(", ", selectedrowNames);

            // Asking the question if your sure you want to delete this and comebine then in to the first value
            DialogResult result = MessageBox.Show("Are you sure you want to combine this: " + selectedIdsStringFormat, "Do you want to combine", MessageBoxButtons.YesNo);

            // Yes than check if all the names are the same if not than ask again but this time with the names
            if (result == DialogResult.Yes)
            {
                bool areNamesTheSame = false;

                // Checking eacht name in the array
                foreach (string name in selectedrowNames)
                {
                    // If the name does not match the name in the first element than Return false Else contue
                    if (name.ToLower() != selectedrowNames[0].ToLower() || !name.ToLower().Contains(selectedrowNames[0].ToLower()))
                    {
                        areNamesTheSame = true;
                        break;
                    }
                }

                //(selectedrowNames.ToArray() + selectedRowIds.Select(i => i.ToString()).ToArray())

                // Ask with Id Sould stay
                RadioButtonSelector radioButtonSelector = new RadioButtonSelector(selectedRowIds.Select(i => i.ToString()).ToArray());
                radioButtonSelector.ShowDialog();

                // Make sure you cant cancel so you have to select a value
                radioButtonSelector.CancelButtonEnabeld = false;

                // Gets the selected result of the dialog
                rowNotToDelete = Convert.ToInt32(radioButtonSelector.Result);

                // The names do not all match
                if (areNamesTheSame == true)
                {
                    // Ask again if you sure you wanted to delete this. Also showing the names
                    DialogResult areYouSureResult = MessageBox.Show(
                        "Are you really sure you wnat ot delete thease? : " + selectedNamesStringFormat,
                        "Are you really sure",
                        MessageBoxButtons.YesNo
                    );

                    // If he is sure than delete and bring them to a single row
                    if (areYouSureResult == DialogResult.Yes)
                    {
                        // Selecting each Id except the first one and deleteing them
                        foreach (int id in selectedRowIds)
                        {
                            if (rowNotToDelete != id)
                            {
                                // Deleteing the row indexes
                                _sqlSeries.RemoveFromTable(_tableName, id);

                                // Now we are sure we have deleted someting
                                haveDeletedSomething = true;
                            }
                        }
                    }
                }
                else
                {
                    // Else just delete becuase the names already match
                    foreach (int id in selectedRowIds)
                    {
                        if (rowNotToDelete != id)
                        {
                            // Deleteing the row indexes
                            _sqlSeries.RemoveFromTable(_tableName, id);

                            // Now we are sure we have deleted someting
                            haveDeletedSomething = true;
                        }
                    }
                }

                // If it has deleted something than create a new datastruct with the new infomation
                if (haveDeletedSomething == true)
                {
                    // Createing the datastruct that has to go the the Edit form or that will be used to change the database entry
                    _serieDataStruct.Name = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Name"].Value.ToString();
                    _serieDataStruct.Studio = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Studio"].Value.ToString();
                    _serieDataStruct.Episode = "All";
                    _serieDataStruct.Season = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Season"].Value.ToString();
                    _serieDataStruct.Size = Convert.ToInt32(dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Size"].Value);
                    _serieDataStruct.Quality = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Quality"].Value.ToString();
                    _serieDataStruct.Lenght = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Lenght"].Value.ToString();
                    _serieDataStruct.Genre = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Genre"].Value.ToString();
                    _serieDataStruct.Genre1 = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Genre1"].Value.ToString();
                    _serieDataStruct.Language = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Language"].Value.ToString();
                    _serieDataStruct.CheckNewLink = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["CheakNewlink"].Value.ToString();
                    _serieDataStruct.NextSeasonData = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Nextseasondate"].Value.ToString();
                    _serieDataStruct.Custom = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Custom"].Value.ToString();
                    _serieDataStruct.Custom1 = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Custom1"].Value.ToString();
                    _serieDataStruct.Location = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["Location"].Value.ToString();
                    _serieDataStruct.PictureLocation = dataGridView1.Rows[selectedDataGridViewRowIndex.First()].Cells["PictureLocation"].Value.ToString();
                }

                // Asking if you want to edit the entry
                DialogResult doYouWantToEditDialogResult = MessageBox.Show("Do you also want to edit this row", "", MessageBoxButtons.YesNo);

                // Ask if you want to edit the row that was not deleted. Only ask if it has deleted something
                if (doYouWantToEditDialogResult == DialogResult.Yes && haveDeletedSomething == true)
                {
                    // Getting the edit form
                    Edit edit = new Edit(4, _tableName, rowNotToDelete, _serieDataStruct);

                    // Showing the edit form and closeing this form
                    DialogResult editDialogResult = edit.ShowDialog();

                    // Makeing sure that the new data has been enterd in the database
                    if (editDialogResult == DialogResult.OK)
                    {
                        this.DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        // If the database was not edited than edit it here
                        _sqlSeries.UpdateRowInTable(_tableName, rowNotToDelete, _serieDataStruct);
                        this.DialogResult = DialogResult.OK;
                    }

                }
                else if (doYouWantToEditDialogResult == DialogResult.No && haveDeletedSomething == true)
                {
                    // Now edit the row
                    _sqlSeries.UpdateRowInTable(_tableName, rowNotToDelete, _serieDataStruct);
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    // Do nothing
                }
            }
        }


        // Dynamic search
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            SearchDataGridView(dataGridView1, textBox1);
        }
    }




}
