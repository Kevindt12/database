﻿using DataBase.Forms.Main;
using DataBase.Source.Settings;


using System;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Security;
using System.Runtime.CompilerServices;

namespace DataBase.Source
{



    static class Program
    {
        
        private const bool _showConsole = false;


        // The main entry point for the application.
        [STAThread]
        static void Main()
        {
            // HACK : Later set the console a but better up for debuging

            // Open the console
            if (_showConsole == true)
            {
                ConsoleManager.Show();
            }

            Program.LOG("Application starting");

            // Showing a splash screen so everything else can load
            // The splash screen will be closed in the main from
            SplashScreen.RunSplash();

            // Setting the new buold of the version and setting a static varible in the class to the version
            GeneralSettings.GetVersion();

            // Loading the Main Form
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());

        }


        /// <summary>
        /// Logging to the Standard Output Stream, Also known as the Console
        /// </summary>
        /// <param name="message">The Message you want to log</param>
        public static void LOG(string message, bool timeStamp = true)
        {
            string dateTime = "[" + DateTime.Now + "]";

            if (timeStamp)
            {
                Console.WriteLine(dateTime + message);
            }
            else
            {
                Console.WriteLine(message);
            }

        }

        /// <summary>
        /// Logging to the Standard Output Stream, Also known as the Console
        /// </summary>
        /// <param name="message">The Message you wnat to display</param>
        /// <param name="color">The Color of the Output</param>
        public static void LOG(string message, LogColor color, bool timeStamp = true)
        {
            string dateTime = "[" + DateTime.Now + "]";

            // Checking with color needs to be used and setting the color
            if (color == LogColor.Log)
            {
                Console.ForegroundColor = ConsoleColor.White;
            }
            else if (color == LogColor.Error)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            else if (color == LogColor.Warning)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
            }

            // Checks if it sould add a time stamp
            if (timeStamp)
            {
                Console.WriteLine(dateTime + message);
            }
            else
            {
                Console.WriteLine(message);
            }

            // Reseting the color
            Console.ResetColor();
        }



        /// <summary>
        /// Emptying picture boxes
        /// </summary>
        /// <param name="pictureBox">The picutre box that need disposting of its content</param>
        public static void EmptyingPictureBoxes(PictureBox pictureBox)
        {
            // Checking if there is something in the picture box before emptying it
            if (pictureBox.Image != null)
            {
                // Emptying the textbox's
                pictureBox.Image.Dispose();
                pictureBox.Image = null;
            }
        }

    }



    [SuppressUnmanagedCodeSecurity]
    public static class ConsoleManager
    {
        private const string Kernel32_DllName = "kernel32.dll";

        [DllImport(Kernel32_DllName)]
        private static extern bool AllocConsole();

        [DllImport(Kernel32_DllName)]
        private static extern bool FreeConsole();

        [DllImport(Kernel32_DllName)]
        private static extern IntPtr GetConsoleWindow();

        [DllImport(Kernel32_DllName)]
        private static extern int GetConsoleOutputCP();

        public static bool HasConsole
        {
            get { return GetConsoleWindow() != IntPtr.Zero; }
        }

        /// <summary>
        /// Creates a new console instance if the process is not attached to a console already.
        /// </summary>
        public static void Show()
        {
            //#if DEBUG
            if (!HasConsole)
            {
                AllocConsole();
                InvalidateOutAndError();
            }
            //#endif
        }
        /// <summary>
        /// If the process has a console attached to it, it will be detached and no longer visible. Writing to the System.Console is still possible, but no output will be shown.
        /// </summary>
        public static void Hide()
        {
            //#if DEBUG
            if (HasConsole)
            {
                SetOutAndErrorNull();
                FreeConsole();
            }
            //#endif
        }

        public static void Toggle()
        {
            if (HasConsole)
            {
                Hide();
            }
            else
            {
                Show();
            }
        }

        static void InvalidateOutAndError()
        {
            Type type = typeof(System.Console);

            System.Reflection.FieldInfo _out = type.GetField("_out",
                System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);

            System.Reflection.FieldInfo _error = type.GetField("_error",
                System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);

            System.Reflection.MethodInfo _InitializeStdOutError = type.GetMethod("InitializeStdOutError",
                System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);

            Debug.Assert(_out != null);
            Debug.Assert(_error != null);

            Debug.Assert(_InitializeStdOutError != null);

            _out.SetValue(null, null);
            _error.SetValue(null, null);

            _InitializeStdOutError.Invoke(null, new object[] { true });
        }

        static void SetOutAndErrorNull()
        {
            Console.SetOut(TextWriter.Null);
            Console.SetError(TextWriter.Null);
        }
    }



    /// <summary>
    /// Log Color For Console
    /// </summary>
    public enum LogColor
    {
        Log,
        Error,
        Warning
    }







    public class FatalErrorExeption : Exception
    {

        public override string Message
        {
            get
            {
                return "There was a fatal error, the program will close now";
            }
        }

        public FatalErrorExeption() : base()
        {
            Application.Exit();
        }

        public FatalErrorExeption(string message) : base(message)
        {
            Application.Exit();
        }
    }









    // Makeing sure it can be seen everywhere becouse it is very inportant
    [ComVisibleAttribute(true)]
    public struct SoftwareData
    {
        public string Name;
        public string Developer;
        public string Publisher;
        public string Version;
        public float Size;
        public string SubCategory;
        public string CrackType;
        public string TorrentWebLink;
        public String Custom;
        public string Custom1;
        public string Location;
        public string PictureLocation;
    }

    // Makeing sure it can be seen everywhere becouse it is very inportant
    [ComVisibleAttribute(true)]
    public struct GameData
    {
        public string Name;
        public string Developer;
        public string Publisher;
        public string Version;
        public float Size;
        public string Genre;
        public string CrackType;
        public string TorrentWebLink;
        public String Custom;
        public string Custom1;
        public string Location;
        public string PictureLocation;
    }

    // Makeing sure it can be seen everywhere becouse it is very inportant
    [ComVisibleAttribute(true)]
    public struct MovieData
    {
        public string Name;
        public string Studio;
        public int Lenght;
        public string Subtitles;
        public float Size;
        public string Subtitles1;
        public string Genre;
        public string Genre1;
        public string Genre2;
        public string Language;
        public string Language1;
        public string Custom;
        public string Custom1;
        public string Location;
        public string PictureLocation;
        public string ReleaseDate;
    }

    // Makeing sure it can be seen everywhere becouse it is very inportant
    [ComVisibleAttribute(true)]
    public struct SerieData
    {
        public string Name;
        public string Studio;
        public string Episode;
        public string Season;
        public float Size;
        public string Quality;
        public string Lenght;
        public string Genre;
        public string Genre1;
        public string Language;
        public string CheckNewLink;
        public string NextSeasonData;
        public string Custom;
        public string Custom1;
        public string Location;
        public string PictureLocation;
    }










}




