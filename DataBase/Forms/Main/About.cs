﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DataBase.Source.Settings;

namespace DataBase.Forms.Main
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();

            string version;

            version = Properties.Settings.Default.Version.ToString() + "." + GeneralSettings.MajorVersion.ToString() + "." + GeneralSettings.MinorVersion.ToString() + "." + GeneralSettings.BuildVersion.ToString();


            label6.Text = version;

            // Fixing the size
            this.MinimumSize = this.Size;
            this.MaximumSize = this.Size;
            this.MaximizeBox = false;
        }
    }
}
