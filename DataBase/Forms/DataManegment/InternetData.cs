﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DataBase.Source.Data;
using DataBase.Source;


namespace DataBase.Forms.DataManegment
{
    public partial class InternetData : Form
    {
        private MovieData _movieData;
        private SerieData _serieData;
        private GameData _gameData;

        private string _selectedMovieTable;
        private string _selectedSerieTable;
        private string _selectedGameTable;
        private int _rowId = 0;



        // Constructor
        /// <summary>
        /// Getting data from website IMDB or Metacritic
        /// </summary>
        /// <param name="selectedMoviesTable">The Selected Movie Table</param>
        /// <param name="selectedSeriesTable">The Selected Serie Table</param>
        /// <param name="selectedGamesTable">The Selected Game Table</param>
        public InternetData(string selectedMoviesTable, string selectedSeriesTable, string selectedGamesTable, int rowId)
        {
            InitializeComponent();

            // Setting the tables to the correct position
            _selectedMovieTable = selectedMoviesTable;
            _selectedSerieTable = selectedSeriesTable;
            _selectedGameTable = selectedGamesTable;

            _rowId = rowId;
        }




        private void rbMovieSelector_CheckedChanged(object sender, EventArgs e)
        {
            // makeing sure that one or the other is selected
            rbSerieSelector.Checked = false;
            if (rbMovieSelector.Checked == false)
            {
                rbSerieSelector.Checked = true;
            }

            // enableingh the labels and radio buttons that can be used
            icbLenght.Enabled = true;
            IcbReleaseDate.Enabled = true;
            lbLenght.Enabled = true;
            lbReleaseDate.Enabled = true;
        }

        private void rbSerieSelector_CheckedChanged(object sender, EventArgs e)
        {
            // makeing sure that one or the other is selected
            rbMovieSelector.Checked = false;
            if (rbSerieSelector.Checked == false)
            {
                rbMovieSelector.Checked = true;
            }

            // Disableing the labels and radio buttons that cant be used
            icbLenght.Enabled = false;
            IcbReleaseDate.Enabled = false;
            lbLenght.Enabled = false;
            lbReleaseDate.Enabled = false;
        }




        // Checking wich data sould go out with the datastuct based on the checked varibles
        public MovieData SelectedDataByRadioButtons(MovieData movieData)
        {
            if (!IcbName.Checked)
            {
                movieData.Name = null;
            }
            if (!icbLenght.Checked)
            {
                movieData.Lenght = 0;
            }
            if (!icbLanguage.Checked)
            {
                movieData.Language = null;
            }
            if (!IcbGenre.Checked)
            {
                movieData.Genre = null;
            }
            if (!IcbGenre1.Checked)
            {
                movieData.Genre1 = null;
            }
            if (!IcbPicture.Checked)
            {
                movieData.PictureLocation = null;
            }
            if (!IcbReleaseDate.Checked)
            {
                movieData.ReleaseDate = null;
            }

            return movieData;
        }

        // Checking wich data sould go out with the datastuct based on the checked varibles
        public SerieData SelectedDataByRadioButtons(SerieData serieData)
        {
            if (!IcbName.Checked)
            {
                serieData.Name = null;
            }
            if (!icbLanguage.Checked)
            {
                serieData.Language = null;
            }
            if (!IcbGenre.Checked)
            {
                serieData.Genre = null;
            }
            if (!IcbGenre1.Checked)
            {
                serieData.Genre1 = null;
            }
            if (!IcbPicture.Checked)
            {
                serieData.PictureLocation = null;
            }


            return serieData;
        }

        // Checking wich data sould go out with the datastuct based on the checked varibles
        public GameData SelectedDataByRadioButtons(GameData gamedata)
        {
            if (!mcbName.Checked)
            {
                gamedata.Name = null;
            }
            if (!McbDeveloper.Checked)
            {
                gamedata.Developer = null;
            }
            if (!mcbGenre.Checked)
            {
                gamedata.Genre = null;
            }
            if (!McbPicture.Checked)
            {
                gamedata.PictureLocation = null;
            }


            return gamedata;
        }





        /// <summary>
        /// Setting the labels to the data in the structs
        /// </summary>
        /// <param name="_MovieData">the DataStruct</param>
        private void SetLabelWithStructData(MovieData _MovieData)
        {
            lbName.Text = _MovieData.Name;
            lbLenght.Text = _MovieData.Lenght.ToString();
            lbLanguage.Text = _MovieData.Language;
            lbGenre.Text = _MovieData.Genre;
            lbGenre1.Text = _MovieData.Genre1;
            lbpicture.Text = _MovieData.PictureLocation;
            lbReleaseDate.Text = _MovieData.ReleaseDate;
        }

        /// <summary>
        /// Setting the labels to the data in the structs
        /// </summary>
        /// <param name="_MovieData">the DataStruct</param>
        private void SetLabelWithStructData(SerieData _SerieData)
        {
            lbName.Text = _SerieData.Name;
            lbLanguage.Text = _SerieData.Language;
            lbGenre.Text = _SerieData.Genre;
            lbGenre1.Text = _SerieData.Genre1;
            lbpicture.Text = _SerieData.PictureLocation;
        }

        /// <summary>
        /// Setting the labels to the data in the structs
        /// </summary>
        /// <param name="_MovieData">the DataStruct</param>
        private void SetLabelWithStructData(GameData _GamesData)
        {
            MlbName.Text = _GamesData.Name;
            MlbDeveloper.Text = _GamesData.Developer;
            MlbGenre.Text = _GamesData.Genre;
        }





        // IMDB - Getting the website
        private void IbtnGetWebsite_Click(object sender, EventArgs e)
        {
            try
            {
                // Checking wich radio button is selected
                if (rbMovieSelector.Checked == true)
                {

                    // Getting the data              
                    _movieData = new IMDB.GetMovieData(itbURL.Text).GetData();

                    // Set the labels with the text.
                    SetLabelWithStructData(_movieData);

                    // Loading the picture box.
                    ipbPicture.Load(_movieData.PictureLocation);

                }
                else if (rbSerieSelector.Checked == true)
                {
                    // Getting the data
                    _serieData = new IMDB.GetSeriesData(itbURL.Text).GetData();

                    // Set the labels with the text
                    SetLabelWithStructData(_serieData);

                    // Loading the picture box
                    ipbPicture.Load(_serieData.PictureLocation);
                }
                else
                {
                    // Show a error that there was no radio button selected - Could happen but rare
                    MessageBox.Show("Error there was no radio button selected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        // When Get Website is pressed
        private void MbtnGetWebsite_Click(object sender, EventArgs e)
        {
            try
            {
                // Setting the struct to a local varible for other use
                _gameData = new Metacridic(mtbURL.Text).GetData();

                //Loading the picture in the picture box
                pbmPicture.Load(_gameData.PictureLocation);

                // filling the labels in with the data
                SetLabelWithStructData(_gameData);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        // IMDB -> Send To new - Sending the data to a new entry from for the database
        private void IbtnSendToNew_Click(object sender, EventArgs e)
        {
            Add add;
            MovieData movieData;
            SerieData serieData;
            DialogResult result;

            // Checking wich radio button is selected
            if (rbMovieSelector.Checked == true)
            {
                // Selecting wich data sould go to the add form
                movieData = SelectedDataByRadioButtons(_movieData);

                // Sending the data to the add from and opeing it
                add = new Add(2, _selectedMovieTable, movieData);
                result = add.ShowDialog();

                // Checking if dialog result was good
                if (result == DialogResult.OK)
                {
                    this.DialogResult = DialogResult.OK;

                    this.Close();
                }

            }
            else if (rbSerieSelector.Checked == true)
            {
                // Selecting wich data sould go to the add form
                serieData = SelectedDataByRadioButtons(_serieData);

                // Sending the data to the add from and opeing it
                add = new Add(3, _selectedSerieTable, serieData);
                result = add.ShowDialog();

                // Checking if dialog result was good
                if (result == DialogResult.OK)
                {
                    this.DialogResult = DialogResult.OK;

                    this.Close();
                }
            }
        }

        // MetaCritic -> Send To new - Sending the data to a new entry from for the database
        private void btnmGoToNew_Click(object sender, EventArgs e)
        {
            GameData gameData;
            DialogResult result;

            // Selecting wich data sould go out to he add form
            gameData = SelectedDataByRadioButtons(_gameData);

            // Showing the add form
            Add add = new Add(1, _selectedGameTable, gameData);
            result = add.ShowDialog();

            // Checking if dialog result was good
            if (result == DialogResult.OK)
            {
                this.DialogResult = DialogResult.OK;

                this.Close();
            }
        }


        // IMDB -> Send To Edit - Sending the data to a edit form so it can edit data that already is in the database
        private void IbtnSendToEdit_Click(object sender, EventArgs e)
        {
            Edit add;
            MovieData movieData;
            SerieData serieData;
            DialogResult result;

            // Checking wich radio button is selected
            if (rbMovieSelector.Checked == true)
            {
                // Selecting wich data sould go to the add form
                movieData = SelectedDataByRadioButtons(_movieData);

                // Sending the data to the add from and opeing it
                add = new Edit(2, _selectedMovieTable, _rowId, movieData);
                result = add.ShowDialog();

                // Checking if dialog result was good
                if (result == DialogResult.OK)
                {
                    this.DialogResult = DialogResult.OK;

                    this.Close();
                }

            }
            else if (rbSerieSelector.Checked == true)
            {
                // Selecting wich data sould go to the add form
                serieData = SelectedDataByRadioButtons(_serieData);

                // Sending the data to the add from and opeing it
                add = new Edit(3, _selectedSerieTable, _rowId, serieData);
                result = add.ShowDialog();

                // Checking if dialog result was good
                if (result == DialogResult.OK)
                {
                    this.DialogResult = DialogResult.OK;

                    this.Close();
                }
            }
        }

        // MetaCritic -> Send To Edit - Sending the data to a edit form so it can edit data that already is in the database
        private void MbtnSendToEdit_Click(object sender, EventArgs e)
        {
            GameData gameData;
            DialogResult result;

            // Selecting wich data sould go out to he add form
            gameData = SelectedDataByRadioButtons(_gameData);

            // Showing the add form
            Edit add = new Edit(1, _selectedGameTable, _rowId, gameData);
            result = add.ShowDialog();

            // Checking if dialog result was good
            if (result == DialogResult.OK)
            {
                this.DialogResult = DialogResult.OK;

                this.Close();
            }

        }


        // Cancel - Closes the fro       
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
