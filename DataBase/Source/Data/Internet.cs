﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Configuration;
using System.Net;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;

namespace DataBase.Source.Data
{
    public class IMDB
    {

        private string _uri;
        private HtmlAgilityPack.HtmlDocument _document;



        public HtmlAgilityPack.HtmlDocument Document
        {
            get
            {
                return _document;
            }
            private set
            {
                _document = value;
            }
        }



        /// <summary>
        /// Getting Data from the imdb website
        /// </summary>
        /// <param name="url">the link to the website</param>
        public IMDB(string url)
        {
            // Checking if the fromat of the string is right
            if (String.IsNullOrEmpty(url))
            {
                throw new NullReferenceException("This String Cant be Null");
            }
            else if (!url.Contains("title"))
            {
                throw new ArgumentException("This is not a movie or serie");
            }
            else
            {
                _uri = url;
            }

            // Loading the website and storing it
            Document = LoadWebsite();

        }



        /// <summary>
        /// Loading the website
        /// </summary>
        /// <returns>The HTML Website</returns>
        private HtmlAgilityPack.HtmlDocument LoadWebsite()
        {
            HtmlWeb web = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

            // Setting the website settings
            doc.OptionFixNestedTags = true;
            // Loading the website
            doc = web.Load(_uri);

            return doc;
        }



        // Getting the movie data in a subclass
        public class GetMovieData : IMDB
        {
            /// <summary>
            /// Getting movie data
            /// </summary>
            /// <param name="url">The URL of the IMDB website</param>
            public GetMovieData(string url) : base(url)
            {

            }



            // Getting the name
            private string GetName()
            {
                // Loading and getting the document
                HtmlAgilityPack.HtmlDocument doc = base.Document;

                // Getting the node
                HtmlNode node = doc.DocumentNode.SelectSingleNode("//*[@id=\"title-overview-widget\"]/div[2]/div[2]/div/div[2]/div[2]/h1");


                string titleName = EditTitleName(node.InnerText.ToString());

                // Retuning the text of the node
                return titleName;
            }


            // Getting the Lenght
            private int Getlength()
            {
                // Loading and getting the document
                HtmlAgilityPack.HtmlDocument doc = base.Document;

                // Getting the node
                HtmlNode node = doc.DocumentNode.SelectSingleNode("//*[@id=\"titleDetails\"]//div/time[1]");


                int lenght = EditMovielenght(node.InnerText.ToString());

                // Retuning the text of the node
                return lenght;
            }


            // Getting the language
            private string GetLanguage()
            {
                // Loading and getting the document
                HtmlAgilityPack.HtmlDocument doc = base.Document;

                // Getting the node
                HtmlNode node = doc.DocumentNode.SelectSingleNode("//*[@id=\"titleDetails\"]/div[3]/a[1]");

                // Retuning the text of the node
                return node.InnerText.ToString();
            }


            // Getting the genre
            private string GetGenre()
            {
                // Loading and getting the document
                HtmlAgilityPack.HtmlDocument doc = base.Document;

                // Getting the node
                HtmlNode node = doc.DocumentNode.SelectSingleNode("//*[@id=\"titleStoryLine\"]/div[4]/a[1]");

                // Retuning the text of the node
                return node.InnerText.ToString();
            }


            // Getting the second genre
            private string GetGenre1()
            {
                // Loading and getting the document
                HtmlAgilityPack.HtmlDocument doc = base.Document;

                // Getting the node
                HtmlNode node = doc.DocumentNode.SelectSingleNode("//*[@id=\"titleStoryLine\"]/div[4]/a[2]");

                // Retuning the text of the node
                return node.InnerText.ToString();
            }


            // Getting the picture location
            private string GetPicture()
            {
                // Loading and getting the document
                HtmlAgilityPack.HtmlDocument doc = base.Document;

                // Getting the node
                HtmlNode node = doc.DocumentNode.SelectSingleNode("//div[@class='poster']//img");

                // Gettign the attribute data
                string poster = node.Attributes["src"].Value;

                // Retuning the text of the node
                return poster;
            }


            // Getting the release date
            private string ReleaseDate()
            {
                // Loading and getting the document
                HtmlAgilityPack.HtmlDocument doc = base.Document;

                // Getting the node
                HtmlNode node = doc.DocumentNode.SelectSingleNode("//div[@class='subtext']//meta[@itemprop=\"datePublished\"]");

                string releaseDate = node.Attributes["content"].Value;

                // Setting the string to the european value
                releaseDate = ConvertReleaseDateToEuropeanStanderd(releaseDate, true);

                return releaseDate;
            }




            /// <summary>
            /// Converting the release date to the european standed
            /// </summary>
            /// <param name="releaseDate">the release date</param>
            /// <param name="textBase">if the month sould be text based IE : 01 or January</param>
            /// <returns></returns>
            private string ConvertReleaseDateToEuropeanStanderd(string releaseDate, bool textBase = true)
            {
                string[] release = releaseDate.Split('-');

                string day = release[2];
                string month = release[1];
                string year = release[0];

                // Checking if the relase date sould be text based else just use the number for the release date
                if (textBase)
                {
                    switch (month)
                    {
                        case "01":
                            month = "January";
                            break;
                        case "02":
                            month = "February";
                            break;
                        case "03":
                            month = "March";
                            break;
                        case "04":
                            month = "April";
                            break;
                        case "05":
                            month = "May";
                            break;
                        case "06":
                            month = "June";
                            break;
                        case "07":
                            month = "July";
                            break;
                        case "08":
                            month = "August";
                            break;
                        case "09":
                            month = "September";
                            break;
                        case "10":
                            month = "October";
                            break;
                        case "11":
                            month = "November";
                            break;
                        case "12":
                            month = "December";
                            break;
                    }
                }

                string normalReleaseDate = day + " " + month + " " + year;

                return normalReleaseDate;
            }


            /// <summary>
            /// Edits the title name so there are no strange things
            /// </summary>
            /// <param name="titleName">a Clean title</param>
            /// <returns></returns>
            private string EditTitleName(string titleName)
            {
                if (titleName.Contains("nbsp"))
                {
                    titleName = titleName.Split('&')[0];
                }
                return titleName;
            }


            /// <summary>
            /// Makeing a int of the lenght and removieng the min string
            /// </summary>
            /// <param name="movieLenght"></param>
            /// <returns>INT Lenght</returns>
            private int EditMovielenght(string movieLenght)
            {
                movieLenght = movieLenght.Split(' ')[0];

                int lenght = Convert.ToInt32(movieLenght);

                return lenght;
            }


            /// <summary>
            /// Filling in the struct with all the data
            /// </summary>
            /// <returns>The Data Struct</returns>
            public MovieData GetData()
            {
                MovieData data = new MovieData();

                // Getting all the values
                data.Name = GetName();
                data.Lenght = Getlength();
                data.Language = GetLanguage();
                data.Genre = GetGenre();
                data.Genre1 = GetGenre1();
                data.PictureLocation = GetPicture();
                data.ReleaseDate = ReleaseDate();

                return data;
            }
        }


        // Getting the serie Data in a subclass
        public class GetSeriesData : IMDB
        {
            /// <summary>
            /// Getting all the serie data
            /// </summary>
            /// <param name="URL">The URL of the IMDB Webpage</param>
            public GetSeriesData(string URL) : base(URL)
            {

            }



            // Getting the name
            private string GetName()
            {
                // Loading and getting the document
                HtmlAgilityPack.HtmlDocument doc = base.Document;

                // Getting the node
                HtmlNode node = doc.DocumentNode.SelectSingleNode("//*[@id=\"title-overview-widget\"]/div[2]/div[2]/div/div[2]/div[2]/h1");

                string titleName = EditTitleName(node.InnerText.ToString());

                // Retuning the text of the node
                return titleName;
            }


            // Getting the language
            private string GetLanguage()
            {
                // Loading and getting the document
                HtmlAgilityPack.HtmlDocument doc = base.Document;

                // Getting the node
                HtmlNode node = doc.DocumentNode.SelectSingleNode("//*[@id=\"titleDetails\"]/div[3]/a[1]");

                // Retuning the text of the node
                return node.InnerText.ToString();
            }


            // Getting the genre
            private string GetGenre()
            {
                // Loading and getting the document
                HtmlAgilityPack.HtmlDocument doc = base.Document;

                // Getting the node
                HtmlNode node = doc.DocumentNode.SelectSingleNode("//*[@id=\"titleStoryLine\"]/div[4]/a[1]");

                // Retuning the text of the node
                return node.InnerText.ToString();
            }


            // Getting the other Genre
            private string GetGenre1()
            {
                // Loading and getting the document
                HtmlAgilityPack.HtmlDocument doc = base.Document;

                // Getting the node
                HtmlNode node = doc.DocumentNode.SelectSingleNode("//*[@id=\"titleStoryLine\"]/div[4]/a[2]");

                // Retuning the text of the node
                return node.InnerText.ToString();
            }


            // Getting the picture location
            private string GetPicture()
            {
                // Loading and getting the document
                HtmlAgilityPack.HtmlDocument doc = base.Document;

                // Getting the node
                HtmlNode node = doc.DocumentNode.SelectSingleNode("//div[@class='poster']//img");

                // Gettign the attribute data
                string poster = node.Attributes["src"].Value;

                // Retuning the text of the node
                return poster;
            }



            /// <summary>
            /// Makeing a clean title
            /// </summary>
            /// <param name="titleName">Clean Title</param>
            /// <returns></returns>
            private string EditTitleName(string titleName)
            {
                if (titleName.Contains("nbsp"))
                {
                    titleName = titleName.Split('&')[0];
                }
                return titleName;
            }


            /// <summary>
            /// Fills all the data to a struct
            /// </summary>
            /// <returns>Struct for all the data</returns>
            public SerieData GetData()
            {
                SerieData data = new SerieData();

                // Filling in all the data
                data.Name = GetName();
                data.Language = GetLanguage();
                data.Genre = GetGenre();
                data.Genre1 = GetGenre1();
                data.PictureLocation = GetPicture();

                return data;
            }
        }
    }




    public class Metacridic
    {


        private string _uri;
        private HtmlAgilityPack.HtmlDocument _document;


        public Metacridic(string url)
        {
            // CHekcing and makeing sure there is a good url
            if (String.IsNullOrEmpty(url.ToLower()))
            {
                throw new NullReferenceException("This String Cant Be Empty");
            }
            else if (!url.ToLower().Contains("game"))
            {
                throw new ArgumentException("This is not a game");
            }
            else if (!url.ToLower().Contains("meta"))
            {
                throw new ArgumentException("This is not a metacritic website");
            }
            else
            {
                _uri = url;
            }

            // Loads the webpage and saves it in a varible
            _document = LoadWebsite();
        }


        /// <summary>
        /// Loading the website
        /// </summary>
        /// <returns>The HTML Website</returns>
        private HtmlAgilityPack.HtmlDocument LoadWebsite()
        {
            HtmlWeb web = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

            // Setting the website settings
            doc.OptionFixNestedTags = true;
            // Loading the website
            doc = web.Load(_uri);

            return doc;
        }


        /// <summary>
        /// Getting the Name back of the game
        /// </summary>
        /// <returns>Game Name</returns>
        private string GetName()
        {
            // Getting the node
            HtmlNode node = _document.DocumentNode.SelectSingleNode("//*[@id=\"main\"]//h1[@class=\"product_title\"]/a");

            // Removeing the next line text and triming the name
            string name = node.InnerText.ToString().Replace("/n", " ").Trim();

            // Retuning the text of the node
            return name;
        }


        /// <summary>
        /// Getting the dev From the game the name
        /// </summary>
        /// <returns>Dev Name</returns>
        private string GetDeveloper()
        {
            // Getting the node
            HtmlNode node = _document.DocumentNode.SelectSingleNode("//*[@id=\"main\"]//div[@class=\"details side_details\"]//li[@class=\"summary_detail developer\"]//span[@class=\"data\"]");

            // Removeing the next line text and triming the name
            string deleloper = node.InnerText.ToString().Replace("/n", " ").Trim();

            // Retuning the text of the node
            return deleloper;
        }


        /// <summary>
        /// Getting one genre back only one genre from the website
        /// </summary>
        /// <returns>Game Genre</returns>
        private string GetGenre()
        {
            // Getting the node
            HtmlNode node = _document.DocumentNode.SelectSingleNode("//*[@id=\"main\"]//div[@class=\"details side_details\"]//li[@class=\"summary_detail product_genre\"]//span[@class=\"data\"]");

            // Removeing the next line text and triming the name
            string genre = node.InnerText.ToString().Replace("/n", " ").Trim();

            // Getting the first genre
            genre = genre.Split(' ')[0];

            // Retuning the text of the node
            return genre;
        }


        /// <summary>
        /// Getting the picture URL
        /// </summary>
        /// <returns>The URL of the picture</returns>
        private string GetPicture()
        {
            // Getting the node
            HtmlNode node = _document.DocumentNode.SelectSingleNode("//*[@id=\"main\"]//div[@class=\"product_image large_image\"]/img");

            string picture = node.Attributes["src"].Value;

            // Retuning the text of the node
            return picture;
        }


        /// <summary>
        /// Retuns the data from the webpage
        /// </summary>
        /// <returns>Retuns the data from the webpage</returns>
        public GameData GetData()
        {
            GameData data = new GameData();

            data.Name = GetName();
            data.Developer = GetDeveloper();
            data.Genre = GetGenre();
            data.PictureLocation = GetPicture();

            return data;
        }

    }



}


