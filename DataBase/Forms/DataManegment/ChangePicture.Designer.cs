﻿namespace DataBase.Forms.DataManegment
{
    partial class ChangePicture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PbNewPicture = new System.Windows.Forms.PictureBox();
            this.pbSourcePicture = new System.Windows.Forms.PictureBox();
            this.btnDone = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnGetPicture = new System.Windows.Forms.Button();
            this.lbSourcePictureLocation = new System.Windows.Forms.Label();
            this.tbNewPictureLocation = new System.Windows.Forms.TextBox();
            this.btnDownloadPicture = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PbNewPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSourcePicture)).BeginInit();
            this.SuspendLayout();
            // 
            // PbNewPicture
            // 
            this.PbNewPicture.Location = new System.Drawing.Point(254, 12);
            this.PbNewPicture.Name = "PbNewPicture";
            this.PbNewPicture.Size = new System.Drawing.Size(217, 309);
            this.PbNewPicture.TabIndex = 1;
            this.PbNewPicture.TabStop = false;
            // 
            // pbSourcePicture
            // 
            this.pbSourcePicture.Location = new System.Drawing.Point(12, 12);
            this.pbSourcePicture.Name = "pbSourcePicture";
            this.pbSourcePicture.Size = new System.Drawing.Size(217, 309);
            this.pbSourcePicture.TabIndex = 2;
            this.pbSourcePicture.TabStop = false;
            // 
            // btnDone
            // 
            this.btnDone.Location = new System.Drawing.Point(14, 401);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(75, 23);
            this.btnDone.TabIndex = 3;
            this.btnDone.Text = "Done";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(95, 401);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 4;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            // 
            // BtnGetPicture
            // 
            this.BtnGetPicture.Location = new System.Drawing.Point(254, 401);
            this.BtnGetPicture.Name = "BtnGetPicture";
            this.BtnGetPicture.Size = new System.Drawing.Size(75, 23);
            this.BtnGetPicture.TabIndex = 5;
            this.BtnGetPicture.Text = "Get Picture";
            this.BtnGetPicture.UseVisualStyleBackColor = true;
            this.BtnGetPicture.Click += new System.EventHandler(this.BtnGetPicture_Click);
            // 
            // lbSourcePictureLocation
            // 
            this.lbSourcePictureLocation.AutoSize = true;
            this.lbSourcePictureLocation.Location = new System.Drawing.Point(12, 337);
            this.lbSourcePictureLocation.Name = "lbSourcePictureLocation";
            this.lbSourcePictureLocation.Size = new System.Drawing.Size(0, 13);
            this.lbSourcePictureLocation.TabIndex = 6;
            // 
            // tbNewPictureLocation
            // 
            this.tbNewPictureLocation.Location = new System.Drawing.Point(14, 365);
            this.tbNewPictureLocation.Name = "tbNewPictureLocation";
            this.tbNewPictureLocation.Size = new System.Drawing.Size(217, 20);
            this.tbNewPictureLocation.TabIndex = 7;
            // 
            // btnDownloadPicture
            // 
            this.btnDownloadPicture.Location = new System.Drawing.Point(335, 401);
            this.btnDownloadPicture.Name = "btnDownloadPicture";
            this.btnDownloadPicture.Size = new System.Drawing.Size(99, 23);
            this.btnDownloadPicture.TabIndex = 8;
            this.btnDownloadPicture.Text = "Download Picture";
            this.btnDownloadPicture.UseVisualStyleBackColor = true;
            this.btnDownloadPicture.Click += new System.EventHandler(this.btnDownloadPicture_Click);
            // 
            // ChangePicture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 447);
            this.Controls.Add(this.btnDownloadPicture);
            this.Controls.Add(this.tbNewPictureLocation);
            this.Controls.Add(this.lbSourcePictureLocation);
            this.Controls.Add(this.BtnGetPicture);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.pbSourcePicture);
            this.Controls.Add(this.PbNewPicture);
            this.Name = "ChangePicture";
            this.Text = "ChangePicture";
            ((System.ComponentModel.ISupportInitialize)(this.PbNewPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSourcePicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PbNewPicture;
        private System.Windows.Forms.PictureBox pbSourcePicture;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnGetPicture;
        private System.Windows.Forms.Label lbSourcePictureLocation;
        private System.Windows.Forms.TextBox tbNewPictureLocation;
        private System.Windows.Forms.Button btnDownloadPicture;
    }
}