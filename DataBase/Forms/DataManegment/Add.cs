﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Windows.Forms;
using System.IO;

using DataBase.Source;
using DataBase.Source.Data;
using DataBase.Source.Settings;



namespace DataBase.Forms.DataManegment
{
    public partial class Add : Form
    {
        private SqlGeneral.Software _sqlSoftware = new SqlGeneral.Software();
        private SqlGeneral.Games _sqlGames = new SqlGeneral.Games();
        private SqlGeneral.Movies _sqlMovies = new SqlGeneral.Movies();
        private SqlGeneral.Series _sqlSeries = new SqlGeneral.Series();

        private string _softwareCurrentTable;
        private string _gamesCurrentTable;
        private string _moviesCurrentTable;
        private string _seriesCurrentTable;

        private SoftwareData _softwareData;
        private MovieData _movieData;
        private SerieData _serieData;
        private GameData _gameData;



        // Constuctors
        /// <summary>
        /// The Add Form If you want to add something to the database.
        /// </summary>
        /// <param name="selectedTabIndex">The current Tab that selected on the Main Form</param>
        /// <param name="tableName">The Overall Current Table Name</param>
        public Add(int selectedTabIndex, string tableName)
        {
            InitializeComponent();

            // Setting the datasources for the tables
            SettingTableDataSources();

            // Selecting the current tab same as Main Form
            tabControl1.SelectedIndex = selectedTabIndex;

            // Checking wich tab is seleceted
            SettingTableInfoByTabPageIndex(selectedTabIndex, tableName);
        }


        // Adding a new form with already some data.

        /// <summary>
        /// Shows the defualt Add Form. And Also adds some Data to the Form
        /// </summary>
        /// <param name="selectedTabIndex">The current Tab that selected on the Main Form</param>
        /// <param name="tableName">The Overall Current Table Name</param>
        /// <param name="seriedata">A Software Data struct</param>
        public Add(int selectedTabIndex, string tableName, SoftwareData softwareData)
        {
            InitializeComponent();

            // Setting the Table data in the Comboboxes
            SettingTableDataSources();

            // Selecting the current tab index same as the Main Form
            tabControl1.SelectedIndex = selectedTabIndex;

            // Checking witch tab page is seleceted. And sets a varible and a combobox to the current table
            SettingTableInfoByTabPageIndex(selectedTabIndex, tableName);

            // Setting the datastruct and filling the textboxes
            _softwareData = softwareData;
            FillTextBoxesWithDataFromStruct(softwareData);

        }

        /// <summary>
        /// Shows the defualt Add Form. And Also adds some Data to the Form
        /// </summary>
        /// <param name="selectedTabIndex">The current Tab that selected on the Main Form</param>
        /// <param name="tableName">The Overall Current Table Name</param>
        /// <param name="seriedata">A Game Data Struct</param>
        public Add(int selectedTabIndex, string tableName,  GameData gameData)
        {
            InitializeComponent();

            // Setting the Table data in the Comboboxes
            SettingTableDataSources();

            // Selecting the current tab same as Main Form
            tabControl1.SelectedIndex = selectedTabIndex;

            // Checking wich tab is seleceted
            SettingTableInfoByTabPageIndex(selectedTabIndex, tableName);

            // Setting the datastruct and filling the textboxes
            _gameData = gameData;
            FillTextBoxesWithDataFromStruct(gameData);

        }

        /// <summary>
        /// Shows the defualt Add Form. And Also adds some Data to the Form
        /// </summary>
        /// <param name="selectedTabIndex">The current Tab that selected on the Main Form</param>
        /// <param name="tableName">The Overall Current Table Name</param>
        /// <param name="movieData">A Movie Data Struct</param>
        public Add(int selectedTabIndex, string tableName, MovieData movieData)
        {
            InitializeComponent();

            // Setting the Table data in the Comboboxes
            SettingTableDataSources();

            // Setting the tabcontrol tab to the tab of the main form
            tabControl1.SelectedIndex = selectedTabIndex;

            // Checking wich tab is seleceted
            SettingTableInfoByTabPageIndex(selectedTabIndex, tableName);

            // Setting the datastruct and filling the textboxes
            _movieData = movieData;
            FillTextBoxesWithDataFromStruct(movieData);
        }

        /// <summary>
        /// Shows the defualt Add Form. And Also adds some Data to the Form
        /// </summary>
        /// <param name="selectedTabIndex">The current Tab that selected on the Main Form</param>
        /// <param name="tableName">The Overall Current Table Name</param>
        /// <param name="serieData">A Serie Data Struct</param>
        public Add(int selectedTabIndex, string tableName, SerieData serieData)
        {
            InitializeComponent();

            // Setting the Table data in the Comboboxes
            SettingTableDataSources();

            // Selecting the current tab same as Main Form
            tabControl1.SelectedIndex = selectedTabIndex;

            // Checking wich tab is seleceted
            SettingTableInfoByTabPageIndex(selectedTabIndex, tableName);

            // Setting the datastruct and filling the textboxes
            _serieData = serieData;
            FillTextBoxesWithDataFromStruct(serieData);
        }



        // When the from loads - Setting the dialog result, And setting the size.
        private void Add_Load(object sender, EventArgs e)
        {
            //// Setting the dialog results
            SbtnCancel.DialogResult = DialogResult.Cancel;
            GbtnCancel.DialogResult = DialogResult.Cancel;
            MbtnCancel.DialogResult = DialogResult.Cancel;
            SEbtnCancel.DialogResult = DialogResult.Cancel;

            // Setting the size combobox text
            ScbSizeMetric.Text = "MB";
            GcbSizeMetric.Text = "MB";
            McbSizeMetric.Text = "MB";
            SEcbSizeMetric.Text = "MB";

            ScbCrackType.DataSource = Properties.Settings.Default.CrackTypes;
            GcbCrackType.DataSource = Properties.Settings.Default.CrackTypes;

            // Fixing the size. Makeing it a fixed size window.
            this.MinimumSize = this.Size;
            this.MaximumSize = this.Size;
            this.MaximizeBox = false;
        }



        /// <summary>
        /// Fills the Textboxes with data from a struct
        /// </summary>
        /// <param name="softwareData">A Software Data Struct</param>
        public void FillTextBoxesWithDataFromStruct(SoftwareData softwareData)
        {
            StbName.Text = softwareData.Name;
            StbDeveloper.Text = softwareData.Developer;
            StbPublisher.Text = softwareData.Publisher;
            StbVersion.Text = softwareData.Version;
            StbSubCategory.Text = softwareData.SubCategory;
            ScbCrackType.Text = softwareData.CrackType;
            StbTorrentLinkSearch.Text = softwareData.TorrentWebLink;
            StbCustom.Text = softwareData.Custom;
            StbCustom1.Text = softwareData.Custom1;
            StbSize.Text = softwareData.Size.ToString();
            StbLocation.Text = softwareData.Location;
        }

        /// <summary>
        /// Fills the Textboxes with data from a struct
        /// </summary>
        /// <param name="gameData">A Game Data Struct</param>
        public void FillTextBoxesWithDataFromStruct(GameData gameData)
        {
            GtbName.Text = gameData.Name;
            GtbDeveloper.Text = gameData.Developer;
            GtbPublisher.Text = gameData.Publisher;
            GtbVersion.Text = gameData.Version;
            GtbGenre.Text = gameData.Genre;
            GcbCrackType.Text = gameData.CrackType;
            GtbTorrentWebLink.Text = gameData.TorrentWebLink;
            GtbCustom.Text = gameData.Custom;
            GtbCustom1.Text = gameData.Custom1;
            GtbSize.Text = gameData.Size.ToString();
            GtbLocation.Text = gameData.Location;
        }

        /// <summary>
        /// Fills the Textboxes with data from a struct
        /// </summary>
        /// <param name="movieData">A Movie Data Struct</param>
        public void FillTextBoxesWithDataFromStruct(MovieData movieData)
        {
            MtbName.Text = movieData.Name;
            MtbStudio.Text = movieData.Studio;
            MtbLenght.Text = movieData.Lenght.ToString();
            MtbGenre.Text = movieData.Genre;
            MtbGenre1.Text = movieData.Genre1;
            MtbGenre2.Text = movieData.Genre2;
            MtbLanuage.Text = movieData.Language;
            MtbLanguage1.Text = movieData.Language1;
            MtbSubtitles.Text = movieData.Subtitles;
            MtbSubtitles1.Text = movieData.Subtitles1;
            MtbCustom.Text = movieData.Custom;
            MtbCustom1.Text = movieData.Custom1;
            MtbSize.Text = movieData.Size.ToString();
            MtbLocation.Text = movieData.Location;
        }

        /// <summary>
        /// Fills the Textboxes with data from a struct
        /// </summary>
        /// <param name="serieData">A Serie Data Struct</param>
        public void FillTextBoxesWithDataFromStruct(SerieData serieData)
        {
            SEtbName.Text = serieData.Name;
            SEtbSutdio.Text = serieData.Studio;
            SEtbEpisode.Text = serieData.Episode;
            SEtbSeason.Text = serieData.Season;
            SEcbQuality.Text = serieData.Quality;
            SEtbLenght.Text = serieData.Lenght;
            SEtbGenre.Text = serieData.Genre;
            SetbGenre1.Text = serieData.Genre1;
            SEtbLanguage.Text = serieData.Language;
            SEtbCheckNewLink.Text = serieData.CheckNewLink;
            //SEdtpNextSeasonDate.Value = Convert.ToDateTime(serieData.NextSeasonData);
            SEtbCustom.Text = serieData.Custom;
            SEtbCustom1.Text = serieData.Custom1;


        }



        /// <summary>
        /// Sets the Combobox DataSources for the tables
        /// </summary>
        private void SettingTableDataSources()
        {
            // Loading all the table names to the comboboxes
            ScbTable.DataSource = _sqlSoftware.AllTablesNames();
            GcbTable.DataSource = _sqlGames.AllTablesNames();
            McbTable.DataSource = _sqlMovies.AllTablesNames();
            SEcbTable.DataSource = _sqlMovies.AllTablesNames();
        }


        /// <summary>
        /// Setting the Table text to a varible and to the Combobox. Depending on witch Tabpage is selected
        /// </summary>
        /// <param name="selectedTabPageIndex">The Tabpage Index</param>
        /// <param name="tableName">The Table name.</param>
        private void SettingTableInfoByTabPageIndex(int selectedTabPageIndex, string tableName)
        {
            // Checking wich Tab is selected at the moment
            switch (selectedTabPageIndex)
            {
                // Filling in the current table and setting a varible for further use
                case 0:
                    ScbTable.Text = tableName;
                    _softwareCurrentTable = tableName;
                    break;
                case 1:
                    GcbTable.Text = tableName;
                    _gamesCurrentTable = tableName;
                    break;
                case 2:
                    McbTable.Text = tableName;
                    _moviesCurrentTable = tableName;
                    break;
                case 3:
                    SEcbTable.Text = tableName;
                    _seriesCurrentTable = tableName;
                    break;
            }
        }


        /// <summary>
        /// Gets a Picture location with the open file dialog
        /// </summary>
        /// <returns>A image path</returns>
        private string GetPictureLocation()
        {
            string filePath = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();

            // setting the parameters for the open file dialog
            openFileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";

            // selecting the photo
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Gets the file path of the picture
                filePath = openFileDialog.FileName;
            }

            return filePath;
        }


        /// <summary>
        /// Moves the picture and renames the picture to a unique number
        /// </summary>
        /// <param name="sourceFileLocation">The location of the picture</param>
        private string MoveAndRenamePicture(string sourceFileLocation)
        {
            string pictureNumber;
            string pictureFolderLocation;
            string destationPictureLocation;
            string extention;

            // Getting the extention
            extention = Path.GetExtension(sourceFileLocation);

            // Getting the number for the picture
            pictureNumber = GeneralSettings.GetPictureName();

            // getting the picture folder where all the pictures are
            pictureFolderLocation = Environment.CurrentDirectory + "\\Pictures\\";

            // Building the destenation and name string
            destationPictureLocation = pictureFolderLocation + pictureNumber + extention;

            // Moveing the picture to the new location
            File.Move(sourceFileLocation, destationPictureLocation);

            return destationPictureLocation;
        }


        /// <summary>
        /// Downloads the picture from the interent and saves it wit ha unic number as name
        /// </summary>
        /// <param name="uri">the url of the picture website</param>
        private string DownloadAndRenamePictureFromInternet(string url)
        {

            WebClient client;
            Uri uri = new Uri(url);
            string folderDestenation;
            string pictureFinalPath;
            string pictureNumber;
            string extention;

            // Getting the extention
            extention = Path.GetExtension(url);

            // Getting the location of were the pictcture is gowing to be downloaded to
            folderDestenation = Environment.CurrentDirectory + "\\Pictures\\";

            // Getting the number
            Source.Settings.GeneralSettings.GetPictureName();
            pictureNumber = Source.Settings.GeneralSettings.GetPictureName();

            // Makeing the full Location string
            pictureFinalPath = folderDestenation + pictureNumber + extention;

            // Downloadng and moveing
            using (client = new WebClient())
            {
                // Checking if the Directory exist if it doesn't than create one.
                if (Directory.Exists(folderDestenation))
                {
                    // Download the picture and save it
                    client.DownloadFile(uri, pictureFinalPath);
                }
                else
                {
                    // Create a directory
                    Directory.CreateDirectory(folderDestenation);

                    // Download the picture and save it
                    client.DownloadFile(uri, pictureFinalPath);
                }
            }

            return pictureFinalPath;
        }


        /// <summary>
        /// Gets the folder location with the open folder dialog
        /// </summary>
        /// <returns>The Path to the folder</returns>
        public string GetFolerLocation()
        {
            FolderSelect.FolderSelectDialog folderBrowserDialog = new FolderSelect.FolderSelectDialog();

            // Showing the new dialoog
            bool result = folderBrowserDialog.ShowDialog();

            // Checking if "okay" was pressed
            if (result == true)
            {
                // Getting the file location and returning it.
                return folderBrowserDialog.FolderLocation;
            }
            else
            {
                // Retun nothing
                return null;
            }
        }


        /// <summary>
        /// Converts KB - GB - TB - To MB
        /// </summary>
        /// <param name="metric">The metric used in UpperCase</param>
        /// <param name="size">The Size of that metric</param>
        /// <returns>a MB size value</returns>
        public float ConvertToMB(string metric, string size)
        {
            float finalSize = 0.0f;

            // Makeing sure that there are not letters in the string
            foreach (char ch in size.ToArray())
            {
                // Checking if it is digit
                if (!char.IsDigit(ch) && ch != '.' && ch != ',')
                {
                    // Sending a error becouse that is not a digit
                    throw new FormatException("There cant be a letter in a float value");
                }
            }

            // replacing the dots with comma's
            size = size.Replace('.', ',');

            // Checking witch Metric has been used and recalculating to MB
            switch (metric)
            {
                case "KB":
                    finalSize = Convert.ToSingle(size) / 1024.0f;
                    break;
                case "MB":
                    finalSize = Convert.ToSingle(size);
                    break;
                case "GB":
                    finalSize = Convert.ToSingle(size) * 1024.0f;
                    break;
                case "TB":
                    finalSize = Convert.ToSingle(size) * 1024.0f * 1024.0f;
                    break;
            }

            // Rounding it to 2 digits
            finalSize = (float)Math.Round((double)finalSize, 2);

            return finalSize;
        }



        // Software -> Done - Done make a new database entry
        private void SbtnDone_Click(object sender, EventArgs e)
        {
            float size = 0;
            string pictureLocation = "";

            // Checking of there was a entry in the name field
            if (StbName.Text == "" || StbName.Text == null)
            {
                // Show a message saying there is no entry
                MessageBox.Show("There was no name with this entry", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (!String.IsNullOrEmpty(StbSize.Text))
                {
                    // Converting everything back to MB so that can be used in the size
                    size = ConvertToMB(ScbSizeMetric.Text, StbSize.Text);
                }

                size = (float)Math.Round((double)size, 2);

                // Picture

                // makeing sure the string is not empty
                if (!String.IsNullOrEmpty(StbPictureLocation.Text))
                {
                    // Checking if there is a picture
                    if (StbPictureLocation.Text.ToLower().Contains("png") || StbPictureLocation.Text.ToLower().Contains("jpg"))
                    {
                        // Checking wich radio button is selected
                        if (srbLocal.Checked == true)
                        {
                            // Renameing and moveing the picutre localy
                            pictureLocation = MoveAndRenamePicture(StbPictureLocation.Text);
                        }
                        else if (srbInternet.Checked == true)
                        {
                            // Downloading and saveing the picture from the interent
                            pictureLocation = DownloadAndRenamePictureFromInternet(StbPictureLocation.Text);
                        }
                    }
                }

                // Adding all the data to a struct
                _softwareData.Name = StbName.Text;
                _softwareData.Developer = StbDeveloper.Text;
                _softwareData.Publisher = StbPublisher.Text;
                _softwareData.Version = StbVersion.Text;
                _softwareData.SubCategory = StbSubCategory.Text;
                _softwareData.CrackType = ScbCrackType.Text;
                _softwareData.TorrentWebLink = StbTorrentLinkSearch.Text;
                _softwareData.Custom = StbCustom.Text;
                _softwareData.Custom1 = StbCustom1.Text;
                _softwareData.Location = StbLocation.Text;
                _softwareData.PictureLocation = pictureLocation;
                _softwareData.Size = size;

                // Adding a new row to the table
                _sqlSoftware.AddToTable(_softwareCurrentTable, _softwareData);

                // Adding cracktype if need it

                // If crack type is already exsits
                if (!Properties.Settings.Default.CrackTypes.Contains(ScbCrackType.Text))
                {
                    Properties.Settings.Default.CrackTypes.Add(ScbCrackType.Text);
                }

            }
        }

        // Games -> Done - Done make a new database entry
        private void GbttnDone_Click(object sender, EventArgs e)
        {
            float size = 0;
            string pictureLocation = "";

            // Checking if the string is not null or empty
            if (String.IsNullOrEmpty(GtbName.Text))
            {
                // if it isshow a message box saying it cant be empty
                MessageBox.Show("There was no name with this entry", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (!String.IsNullOrEmpty(GtbSize.Text))
                {
                    // Converting the size to MB for use in the database
                    size = ConvertToMB(GcbSizeMetric.Text, GtbSize.Text);
                }

                size = (float)Math.Round((double)size, 2);

                // makeing sure the string is not empty
                if (!String.IsNullOrEmpty(GtbPictureLocation.Text))
                {

                    // Checking if there is a picture
                    if (GtbPictureLocation.Text.ToLower().Contains("png") || GtbPictureLocation.Text.ToLower().Contains("jpg"))
                    {
                        // Checking wich radio button is selected
                        if (GrbLocal.Checked == true)
                        {
                            pictureLocation = MoveAndRenamePicture(GtbPictureLocation.Text);
                        }
                        else if (GrbInternet.Checked == true)
                        {
                            pictureLocation = DownloadAndRenamePictureFromInternet(GtbPictureLocation.Text);
                        }
                    }
                }

                // Filling the datastuct
                _gameData.Name = GtbName.Text;
                _gameData.Developer = GtbDeveloper.Text;
                _gameData.Publisher = GtbPublisher.Text;
                _gameData.Version = GtbVersion.Text;
                _gameData.Genre = GtbGenre.Text;
                _gameData.CrackType = GcbCrackType.Text;
                _gameData.TorrentWebLink = GtbTorrentWebLink.Text;
                _gameData.Custom = GtbCustom.Text;
                _gameData.Custom1 = GtbCustom1.Text;
                _gameData.Location = GtbLocation.Text;
                _gameData.PictureLocation = pictureLocation;
                _gameData.Size = size;

                // Adding a new row to the table
                _sqlGames.AddToTable(_gamesCurrentTable, _gameData);


                // Checking if creack type used already exists else add it to the list
                if (!Properties.Settings.Default.CrackTypes.Contains(GcbCrackType.Text))
                {
                    Properties.Settings.Default.CrackTypes.Add(GcbCrackType.Text);
                }
            }



        }

        // Movies -> Dene - Done make a new database entry
        private void MbtnDone_Click(object sender, EventArgs e)
        {
            float size = 0;
            int length = 0;
            string pictureLocation = "";

            // Checking if the name entry has a text
            if (String.IsNullOrEmpty(MtbName.Text))
            {
                MessageBox.Show("There was no entry in the name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            // If it has then make a net entry
            else
            {
                // Makeing sure there is something in the string.
                if (!String.IsNullOrEmpty(MtbSize.Text))
                {
                    size = ConvertToMB(McbSizeMetric.Text, MtbSize.Text);
                }
                size = (float)Math.Round((double)size, 2);

                // Makeing sure there is something in the string
                if (!String.IsNullOrEmpty(MtbLenght.Text))
                {
                    length = Convert.ToInt32(MtbLenght.Text);
                }



                // Picture

                // makeing sure the string is not empty
                if (!String.IsNullOrEmpty(MtbPictureLocation.Text))
                {

                    // Checking if there is a picture
                    if (MtbPictureLocation.Text.ToLower().Contains("png") || MtbPictureLocation.Text.ToLower().Contains("jpg"))
                    {
                        // Checking wich radio button is selected
                        if (MrbLocal.Checked == true)
                        {
                            pictureLocation = MoveAndRenamePicture(MtbPictureLocation.Text);
                        }
                        else if (MrbInternet.Checked == true)
                        {
                            pictureLocation = DownloadAndRenamePictureFromInternet(MtbPictureLocation.Text);
                        }
                    }
                }

                // Filling the datastuct
                _movieData.Name = MtbName.Text;
                _movieData.Studio = MtbStudio.Text;
                _movieData.Lenght = length;
                _movieData.Subtitles = MtbSubtitles.Text;
                _movieData.Size = size;
                _movieData.Subtitles1 = MtbSubtitles1.Text;
                _movieData.Genre = MtbGenre.Text;
                _movieData.Genre1 = MtbGenre1.Text;
                _movieData.Genre2 = MtbGenre2.Text;
                _movieData.Language = MtbLanuage.Text;
                _movieData.Language1 = MtbLanguage1.Text;
                _movieData.Custom = MtbCustom.Text;
                _movieData.Custom1 = MtbCustom1.Text;
                _movieData.Location = MtbLocation.Text;
                _movieData.PictureLocation = pictureLocation;

                // Makeing a new row entry in the movies database
                _sqlMovies.AddToTable(_moviesCurrentTable, _movieData);


            }
        }

        // Series -> Done - Done make a new database entry
        private void SEbtnDone_Click(object sender, EventArgs e)
        {
            float size = 0;
            string pictureLocation = "";

            // Checking if the string is empty or not it it is
            if (String.IsNullOrEmpty(SEtbName.Text))
            {
                MessageBox.Show("There was no entry in the name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            // If not - Make a new entry in the table
            else
            {

                // HACK: There is no size in series so this is not to not brake it but this has to go.
                if (!String.IsNullOrEmpty(SEtbSize.Text))
                {
                    size = ConvertToMB(SEcbSizeMetric.Text, SEtbSize.Text);
                }

                // makeing sure the string is not empty
                if (!String.IsNullOrEmpty(MtbPictureLocation.Text))
                {
                    // Checking if there is a picture
                    if (SEtbPictureLocation.Text.ToLower().Contains("png") || SEtbPictureLocation.Text.ToLower().Contains("jpg"))
                    {
                        // Checking wich radio button is selected
                        if (SErbLocal.Checked == true)
                        {
                            pictureLocation = MoveAndRenamePicture(SEtbPictureLocation.Text);
                        }
                        else if (SErbInternet.Checked == true)
                        {
                            pictureLocation = DownloadAndRenamePictureFromInternet(SEtbPictureLocation.Text);
                        }
                    }
                }

                // Filling the datastruct
                _serieData.Name = SEtbName.Text;
                _serieData.Studio = SEtbSutdio.Text;
                _serieData.Lenght = SEtbLenght.Text;

                // HACK : This is now commented out Because we are not useing size
                //_serieData.Size = Convert.ToSingle(SEtbSize.Text);
                _serieData.Genre = SEtbGenre.Text;
                _serieData.Genre1 = SetbGenre1.Text;
                _serieData.Language = SEtbLanguage.Text;
                _serieData.CheckNewLink = SEtbCheckNewLink.Text;
                _serieData.NextSeasonData = SEdtpNextSeasonDate.Value.ToString();
                _serieData.Quality = SEcbQuality.Text;
                _serieData.Season = SEtbSeason.Text;
                _serieData.Episode = SEtbEpisode.Text;
                _serieData.Location = SEtbLocation.Text;
                _serieData.PictureLocation = pictureLocation;
                _serieData.Custom = SEtbCustom.Text;
                _serieData.Custom1 = SEtbCustom1.Text;


                // Makeing a new row entry in the Series database
                _sqlSeries.AddToTable(_seriesCurrentTable, _serieData);

            }
        }




        // When The text of the name textbox has changed - setting the dialog results
        private void StbName_TextChanged(object sender, EventArgs e)
        {
            // if there was no entry then make sure that it dose not retun anything
            if (StbName.Text == "" || StbName.Text == null)
            {
                SbtnDone.DialogResult = DialogResult.None;
            }
            else
            // if there was a entry then make sure that that button has a ok dialogresult
            {
                SbtnDone.DialogResult = DialogResult.OK;
            }
        }

        // When The text of the name textbox has changed - setting the dialog results
        private void GtbName_TextChanged(object sender, EventArgs e)
        {
            // If there is text in textbox name then dialog result sould be ok else none
            if (!String.IsNullOrEmpty(GtbName.Text))
            {
                GbtnDone.DialogResult = DialogResult.OK;
            }
            else
            {
                GbtnDone.DialogResult = DialogResult.None;
            }
        }

        // When The text of the name textbox has changed - setting the dialog results
        private void MtbName_TextChanged(object sender, EventArgs e)
        {
            // If there is text in textbox name then dialog result sould be ok else none
            if (!String.IsNullOrEmpty(MtbName.Text))
            {
                MbtnDone.DialogResult = DialogResult.OK;
            }
            else
            {
                MbtnDone.DialogResult = DialogResult.None;
            }
        }

        // When The text of the name textbox has changed - setting the dialog results
        private void SEtbName_TextChanged(object sender, EventArgs e)
        {
            // If there is text in textbox name then dialog result sould be ok else none
            if (!String.IsNullOrEmpty(SEtbName.Text))
            {
                SEbtnDone.DialogResult = DialogResult.OK;
            }
            else
            {
                SEbtnDone.DialogResult = DialogResult.None;
            }
        }



        // Software -> Folder Location - Sets the folder location in the textbox via a open file dialog
        private void SbtnGetFolderLocation_Click(object sender, EventArgs e)
        {
            StbLocation.Text = GetFolerLocation();
        }

        // Games -> Folder Location - Sets the folder location in the textbox via a open file dialog
        private void GbtnGetFolderLocation_Click(object sender, EventArgs e)
        {
            GtbLocation.Text = GetFolerLocation();
        }

        // Movies -> Folder Location - Sets the folder location in the textbox via a open file dialog
        private void MbtnGetFolderLocation_Click(object sender, EventArgs e)
        {
            MtbLocation.Text = GetFolerLocation();
        }

        // Series -> Folder Location - Sets the folder location in the textbox via a open file dialog
        private void SEbtnGetFolderLocation_Click(object sender, EventArgs e)
        {
            SEtbLanguage.Text = GetFolerLocation();
        }



        // Software -> Select Picture - Gets a picture url via a open file dialog
        private void SbtnGetPicture_Click(object sender, EventArgs e)
        {

            StbPictureLocation.Text = GetPictureLocation();

        }

        // Games -> Select Picture - Gets a picture url via a open file dialog
        private void GbtnGetMovePicture_Click(object sender, EventArgs e)
        {
            GtbPictureLocation.Text = GetPictureLocation();
        }

        // Movies -> Select Picture - Gets a picture url via a open file dialog
        private void MbtnGetPicture_Click(object sender, EventArgs e)
        {
            MtbPictureLocation.Text = GetPictureLocation();
        }

        // Series -> Select Picture - Gets a picture url via a open file dialog
        private void SEbtnGetPicture_Click(object sender, EventArgs e)
        {
            SEtbPictureLocation.Text = GetPictureLocation();
        }



        // All -> Cancel - When the cancal button is clicked
        private void Cancel_Button_Clicked(object sender, EventArgs e)
        {
            DialogResult result;
            List<TextBox> textBoxes = new List<TextBox>();

            int tabCount = this.tabControl1.TabCount;
            bool closeForm = false;
            DialogResult cancelResult = DialogResult.Cancel;

            // Getting all the textboxs and adding them to a list
            foreach (TabPage tabPage in tabControl1.TabPages.OfType<TabPage>())
            {
                foreach (TextBox textBox in tabPage.Controls.OfType<TextBox>())
                {
                    textBoxes.Add(textBox);
                }
            }

            // selecting each textbox
            foreach (TextBox tb in textBoxes)
            {
                // Checking if the string are empty
                if (!String.IsNullOrEmpty(tb.Text))
                {
                    // Asking if your sure you want to close
                    result = MessageBox.Show("Are you sure you want to cancel this operation", "Waring", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    // If yes then close the tabcontrol
                    if (result == DialogResult.Yes)
                    {
                        closeForm = true;
                        break;
                    }
                    else
                    {
                        closeForm = false;
                        break;
                    }
                }
                // If all the string are empty then just close without asking
                else
                {
                    closeForm = true;
                }
            }

            if (closeForm == true)
            {
                this.DialogResult = cancelResult;
                this.Close();
            }
            else
            {
                // do nothing
            }
        }


        // When mouse enters the textbox if pastes the content of the clipboard to the textbox. If its null
        private void tbPictureLocation_MouseEnter(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(StbPictureLocation.Text))
            {              
                ((TextBox)sender).Text = Clipboard.GetText();
            }
        }



    }


}
