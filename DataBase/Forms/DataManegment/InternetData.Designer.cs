﻿namespace DataBase.Forms.DataManegment
{
    partial class InternetData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpIMDB = new System.Windows.Forms.TabPage();
            this.rbSerieSelector = new System.Windows.Forms.RadioButton();
            this.rbMovieSelector = new System.Windows.Forms.RadioButton();
            this.btnGetWebsite = new System.Windows.Forms.Button();
            this.itbURL = new System.Windows.Forms.TextBox();
            this.IbtnSendToEdit = new System.Windows.Forms.Button();
            this.ibtnSendToNew = new System.Windows.Forms.Button();
            this.IbtnCancel = new System.Windows.Forms.Button();
            this.lbReleaseDate = new System.Windows.Forms.Label();
            this.lbpicture = new System.Windows.Forms.Label();
            this.lbGenre1 = new System.Windows.Forms.Label();
            this.lbGenre = new System.Windows.Forms.Label();
            this.lbLanguage = new System.Windows.Forms.Label();
            this.lbLenght = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.ipbPicture = new System.Windows.Forms.PictureBox();
            this.tpMetacritic = new System.Windows.Forms.TabPage();
            this.MlbPicture = new System.Windows.Forms.Label();
            this.btnmGetWebsite = new System.Windows.Forms.Button();
            this.mtbURL = new System.Windows.Forms.TextBox();
            this.MbtnSendToEdit = new System.Windows.Forms.Button();
            this.btnmGoToNew = new System.Windows.Forms.Button();
            this.MbtnCancel = new System.Windows.Forms.Button();
            this.MlbGenre = new System.Windows.Forms.Label();
            this.MlbDeveloper = new System.Windows.Forms.Label();
            this.MlbName = new System.Windows.Forms.Label();
            this.pbmPicture = new System.Windows.Forms.PictureBox();
            this.IcbName = new System.Windows.Forms.CheckBox();
            this.icbLenght = new System.Windows.Forms.CheckBox();
            this.IcbGenre1 = new System.Windows.Forms.CheckBox();
            this.icbLanguage = new System.Windows.Forms.CheckBox();
            this.IcbGenre = new System.Windows.Forms.CheckBox();
            this.IcbPicture = new System.Windows.Forms.CheckBox();
            this.IcbReleaseDate = new System.Windows.Forms.CheckBox();
            this.McbPicture = new System.Windows.Forms.CheckBox();
            this.McbDeveloper = new System.Windows.Forms.CheckBox();
            this.mcbName = new System.Windows.Forms.CheckBox();
            this.mcbGenre = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tpIMDB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ipbPicture)).BeginInit();
            this.tpMetacritic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbmPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tpIMDB);
            this.tabControl1.Controls.Add(this.tpMetacritic);
            this.tabControl1.Location = new System.Drawing.Point(-3, -1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(95, 8);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(461, 413);
            this.tabControl1.TabIndex = 1;
            // 
            // tpIMDB
            // 
            this.tpIMDB.BackColor = System.Drawing.SystemColors.Control;
            this.tpIMDB.Controls.Add(this.IcbReleaseDate);
            this.tpIMDB.Controls.Add(this.IcbPicture);
            this.tpIMDB.Controls.Add(this.IcbGenre);
            this.tpIMDB.Controls.Add(this.icbLanguage);
            this.tpIMDB.Controls.Add(this.IcbGenre1);
            this.tpIMDB.Controls.Add(this.icbLenght);
            this.tpIMDB.Controls.Add(this.IcbName);
            this.tpIMDB.Controls.Add(this.rbSerieSelector);
            this.tpIMDB.Controls.Add(this.rbMovieSelector);
            this.tpIMDB.Controls.Add(this.btnGetWebsite);
            this.tpIMDB.Controls.Add(this.itbURL);
            this.tpIMDB.Controls.Add(this.IbtnSendToEdit);
            this.tpIMDB.Controls.Add(this.ibtnSendToNew);
            this.tpIMDB.Controls.Add(this.IbtnCancel);
            this.tpIMDB.Controls.Add(this.lbReleaseDate);
            this.tpIMDB.Controls.Add(this.lbpicture);
            this.tpIMDB.Controls.Add(this.lbGenre1);
            this.tpIMDB.Controls.Add(this.lbGenre);
            this.tpIMDB.Controls.Add(this.lbLanguage);
            this.tpIMDB.Controls.Add(this.lbLenght);
            this.tpIMDB.Controls.Add(this.lbName);
            this.tpIMDB.Controls.Add(this.ipbPicture);
            this.tpIMDB.Location = new System.Drawing.Point(4, 32);
            this.tpIMDB.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.tpIMDB.Name = "tpIMDB";
            this.tpIMDB.Size = new System.Drawing.Size(453, 377);
            this.tpIMDB.TabIndex = 0;
            this.tpIMDB.Text = "IMDB";
            // 
            // rbSerieSelector
            // 
            this.rbSerieSelector.AutoSize = true;
            this.rbSerieSelector.Location = new System.Drawing.Point(318, 53);
            this.rbSerieSelector.Name = "rbSerieSelector";
            this.rbSerieSelector.Size = new System.Drawing.Size(49, 17);
            this.rbSerieSelector.TabIndex = 30;
            this.rbSerieSelector.Text = "Serie";
            this.rbSerieSelector.UseVisualStyleBackColor = true;
            this.rbSerieSelector.CheckedChanged += new System.EventHandler(this.rbSerieSelector_CheckedChanged);
            // 
            // rbMovieSelector
            // 
            this.rbMovieSelector.AutoSize = true;
            this.rbMovieSelector.Checked = true;
            this.rbMovieSelector.Location = new System.Drawing.Point(233, 53);
            this.rbMovieSelector.Name = "rbMovieSelector";
            this.rbMovieSelector.Size = new System.Drawing.Size(54, 17);
            this.rbMovieSelector.TabIndex = 29;
            this.rbMovieSelector.TabStop = true;
            this.rbMovieSelector.Text = "Movie";
            this.rbMovieSelector.UseVisualStyleBackColor = true;
            this.rbMovieSelector.CheckedChanged += new System.EventHandler(this.rbMovieSelector_CheckedChanged);
            // 
            // btnGetWebsite
            // 
            this.btnGetWebsite.Location = new System.Drawing.Point(297, 340);
            this.btnGetWebsite.Name = "btnGetWebsite";
            this.btnGetWebsite.Size = new System.Drawing.Size(90, 23);
            this.btnGetWebsite.TabIndex = 28;
            this.btnGetWebsite.Text = "Get Website";
            this.btnGetWebsite.UseVisualStyleBackColor = true;
            this.btnGetWebsite.Click += new System.EventHandler(this.IbtnGetWebsite_Click);
            // 
            // itbURL
            // 
            this.itbURL.Location = new System.Drawing.Point(11, 15);
            this.itbURL.Name = "itbURL";
            this.itbURL.Size = new System.Drawing.Size(435, 20);
            this.itbURL.TabIndex = 27;
            // 
            // IbtnSendToEdit
            // 
            this.IbtnSendToEdit.Location = new System.Drawing.Point(201, 340);
            this.IbtnSendToEdit.Name = "IbtnSendToEdit";
            this.IbtnSendToEdit.Size = new System.Drawing.Size(90, 23);
            this.IbtnSendToEdit.TabIndex = 26;
            this.IbtnSendToEdit.Text = "Send To Edit";
            this.IbtnSendToEdit.UseVisualStyleBackColor = true;
            this.IbtnSendToEdit.Click += new System.EventHandler(this.IbtnSendToEdit_Click);
            // 
            // ibtnSendToNew
            // 
            this.ibtnSendToNew.Location = new System.Drawing.Point(98, 340);
            this.ibtnSendToNew.Name = "ibtnSendToNew";
            this.ibtnSendToNew.Size = new System.Drawing.Size(97, 23);
            this.ibtnSendToNew.TabIndex = 25;
            this.ibtnSendToNew.Text = "Send To New";
            this.ibtnSendToNew.UseVisualStyleBackColor = true;
            this.ibtnSendToNew.Click += new System.EventHandler(this.IbtnSendToNew_Click);
            // 
            // IbtnCancel
            // 
            this.IbtnCancel.Location = new System.Drawing.Point(11, 340);
            this.IbtnCancel.Name = "IbtnCancel";
            this.IbtnCancel.Size = new System.Drawing.Size(75, 23);
            this.IbtnCancel.TabIndex = 24;
            this.IbtnCancel.Text = "Cancel";
            this.IbtnCancel.UseVisualStyleBackColor = true;
            this.IbtnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lbReleaseDate
            // 
            this.lbReleaseDate.AutoSize = true;
            this.lbReleaseDate.Location = new System.Drawing.Point(352, 279);
            this.lbReleaseDate.Name = "lbReleaseDate";
            this.lbReleaseDate.Size = new System.Drawing.Size(35, 13);
            this.lbReleaseDate.TabIndex = 22;
            this.lbReleaseDate.Text = "label1";
            // 
            // lbpicture
            // 
            this.lbpicture.AutoSize = true;
            this.lbpicture.Location = new System.Drawing.Point(352, 247);
            this.lbpicture.Name = "lbpicture";
            this.lbpicture.Size = new System.Drawing.Size(41, 13);
            this.lbpicture.TabIndex = 14;
            this.lbpicture.Text = "label13";
            // 
            // lbGenre1
            // 
            this.lbGenre1.AutoSize = true;
            this.lbGenre1.Location = new System.Drawing.Point(352, 217);
            this.lbGenre1.Name = "lbGenre1";
            this.lbGenre1.Size = new System.Drawing.Size(35, 13);
            this.lbGenre1.TabIndex = 10;
            this.lbGenre1.Text = "label9";
            // 
            // lbGenre
            // 
            this.lbGenre.AutoSize = true;
            this.lbGenre.Location = new System.Drawing.Point(352, 187);
            this.lbGenre.Name = "lbGenre";
            this.lbGenre.Size = new System.Drawing.Size(35, 13);
            this.lbGenre.TabIndex = 8;
            this.lbGenre.Text = "label7";
            // 
            // lbLanguage
            // 
            this.lbLanguage.AutoSize = true;
            this.lbLanguage.Location = new System.Drawing.Point(352, 159);
            this.lbLanguage.Name = "lbLanguage";
            this.lbLanguage.Size = new System.Drawing.Size(35, 13);
            this.lbLanguage.TabIndex = 6;
            this.lbLanguage.Text = "label5";
            // 
            // lbLenght
            // 
            this.lbLenght.AutoSize = true;
            this.lbLenght.Location = new System.Drawing.Point(352, 132);
            this.lbLenght.Name = "lbLenght";
            this.lbLenght.Size = new System.Drawing.Size(35, 13);
            this.lbLenght.TabIndex = 4;
            this.lbLenght.Text = "label3";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(352, 107);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(35, 13);
            this.lbName.TabIndex = 2;
            this.lbName.Text = "label2";
            // 
            // ipbPicture
            // 
            this.ipbPicture.Location = new System.Drawing.Point(11, 78);
            this.ipbPicture.Name = "ipbPicture";
            this.ipbPicture.Size = new System.Drawing.Size(162, 242);
            this.ipbPicture.TabIndex = 0;
            this.ipbPicture.TabStop = false;
            // 
            // tpMetacritic
            // 
            this.tpMetacritic.BackColor = System.Drawing.SystemColors.Control;
            this.tpMetacritic.Controls.Add(this.mcbGenre);
            this.tpMetacritic.Controls.Add(this.McbPicture);
            this.tpMetacritic.Controls.Add(this.McbDeveloper);
            this.tpMetacritic.Controls.Add(this.mcbName);
            this.tpMetacritic.Controls.Add(this.MlbPicture);
            this.tpMetacritic.Controls.Add(this.btnmGetWebsite);
            this.tpMetacritic.Controls.Add(this.mtbURL);
            this.tpMetacritic.Controls.Add(this.MbtnSendToEdit);
            this.tpMetacritic.Controls.Add(this.btnmGoToNew);
            this.tpMetacritic.Controls.Add(this.MbtnCancel);
            this.tpMetacritic.Controls.Add(this.MlbGenre);
            this.tpMetacritic.Controls.Add(this.MlbDeveloper);
            this.tpMetacritic.Controls.Add(this.MlbName);
            this.tpMetacritic.Controls.Add(this.pbmPicture);
            this.tpMetacritic.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tpMetacritic.Location = new System.Drawing.Point(4, 32);
            this.tpMetacritic.Name = "tpMetacritic";
            this.tpMetacritic.Size = new System.Drawing.Size(453, 377);
            this.tpMetacritic.TabIndex = 1;
            this.tpMetacritic.Text = "Metacritic";
            // 
            // MlbPicture
            // 
            this.MlbPicture.AutoSize = true;
            this.MlbPicture.Location = new System.Drawing.Point(350, 181);
            this.MlbPicture.Name = "MlbPicture";
            this.MlbPicture.Size = new System.Drawing.Size(35, 13);
            this.MlbPicture.TabIndex = 44;
            this.MlbPicture.Text = "label1";
            // 
            // btnmGetWebsite
            // 
            this.btnmGetWebsite.Location = new System.Drawing.Point(295, 339);
            this.btnmGetWebsite.Name = "btnmGetWebsite";
            this.btnmGetWebsite.Size = new System.Drawing.Size(90, 23);
            this.btnmGetWebsite.TabIndex = 42;
            this.btnmGetWebsite.Text = "Get Website";
            this.btnmGetWebsite.UseVisualStyleBackColor = true;
            this.btnmGetWebsite.Click += new System.EventHandler(this.MbtnGetWebsite_Click);
            // 
            // mtbURL
            // 
            this.mtbURL.Location = new System.Drawing.Point(9, 14);
            this.mtbURL.Name = "mtbURL";
            this.mtbURL.Size = new System.Drawing.Size(435, 20);
            this.mtbURL.TabIndex = 41;
            // 
            // MbtnSendToEdit
            // 
            this.MbtnSendToEdit.Location = new System.Drawing.Point(199, 339);
            this.MbtnSendToEdit.Name = "MbtnSendToEdit";
            this.MbtnSendToEdit.Size = new System.Drawing.Size(90, 23);
            this.MbtnSendToEdit.TabIndex = 40;
            this.MbtnSendToEdit.Text = "Send To Edit";
            this.MbtnSendToEdit.UseVisualStyleBackColor = true;
            this.MbtnSendToEdit.Click += new System.EventHandler(this.MbtnSendToEdit_Click);
            // 
            // btnmGoToNew
            // 
            this.btnmGoToNew.Location = new System.Drawing.Point(96, 339);
            this.btnmGoToNew.Name = "btnmGoToNew";
            this.btnmGoToNew.Size = new System.Drawing.Size(97, 23);
            this.btnmGoToNew.TabIndex = 39;
            this.btnmGoToNew.Text = "Send To New";
            this.btnmGoToNew.UseVisualStyleBackColor = true;
            this.btnmGoToNew.Click += new System.EventHandler(this.btnmGoToNew_Click);
            // 
            // MbtnCancel
            // 
            this.MbtnCancel.Location = new System.Drawing.Point(9, 339);
            this.MbtnCancel.Name = "MbtnCancel";
            this.MbtnCancel.Size = new System.Drawing.Size(75, 23);
            this.MbtnCancel.TabIndex = 38;
            this.MbtnCancel.Text = "Cancel";
            this.MbtnCancel.UseVisualStyleBackColor = true;
            this.MbtnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // MlbGenre
            // 
            this.MlbGenre.AutoSize = true;
            this.MlbGenre.Location = new System.Drawing.Point(350, 158);
            this.MlbGenre.Name = "MlbGenre";
            this.MlbGenre.Size = new System.Drawing.Size(35, 13);
            this.MlbGenre.TabIndex = 32;
            this.MlbGenre.Text = "label5";
            // 
            // MlbDeveloper
            // 
            this.MlbDeveloper.AutoSize = true;
            this.MlbDeveloper.Location = new System.Drawing.Point(350, 131);
            this.MlbDeveloper.Name = "MlbDeveloper";
            this.MlbDeveloper.Size = new System.Drawing.Size(35, 13);
            this.MlbDeveloper.TabIndex = 31;
            this.MlbDeveloper.Text = "label3";
            // 
            // MlbName
            // 
            this.MlbName.AutoSize = true;
            this.MlbName.Location = new System.Drawing.Point(350, 106);
            this.MlbName.Name = "MlbName";
            this.MlbName.Size = new System.Drawing.Size(35, 13);
            this.MlbName.TabIndex = 30;
            this.MlbName.Text = "label2";
            // 
            // pbmPicture
            // 
            this.pbmPicture.Location = new System.Drawing.Point(9, 77);
            this.pbmPicture.Name = "pbmPicture";
            this.pbmPicture.Size = new System.Drawing.Size(162, 242);
            this.pbmPicture.TabIndex = 29;
            this.pbmPicture.TabStop = false;
            // 
            // IcbName
            // 
            this.IcbName.AutoSize = true;
            this.IcbName.Checked = true;
            this.IcbName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IcbName.Location = new System.Drawing.Point(223, 104);
            this.IcbName.Name = "IcbName";
            this.IcbName.Size = new System.Drawing.Size(54, 17);
            this.IcbName.TabIndex = 31;
            this.IcbName.Text = "Name";
            this.IcbName.UseVisualStyleBackColor = true;
            // 
            // icbLenght
            // 
            this.icbLenght.AutoSize = true;
            this.icbLenght.Checked = true;
            this.icbLenght.CheckState = System.Windows.Forms.CheckState.Checked;
            this.icbLenght.Location = new System.Drawing.Point(223, 130);
            this.icbLenght.Name = "icbLenght";
            this.icbLenght.Size = new System.Drawing.Size(59, 17);
            this.icbLenght.TabIndex = 32;
            this.icbLenght.Text = "Lenght";
            this.icbLenght.UseVisualStyleBackColor = true;
            // 
            // IcbGenre1
            // 
            this.IcbGenre1.AutoSize = true;
            this.IcbGenre1.Checked = true;
            this.IcbGenre1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IcbGenre1.Location = new System.Drawing.Point(223, 213);
            this.IcbGenre1.Name = "IcbGenre1";
            this.IcbGenre1.Size = new System.Drawing.Size(61, 17);
            this.IcbGenre1.TabIndex = 33;
            this.IcbGenre1.Text = "Genre1";
            this.IcbGenre1.UseVisualStyleBackColor = true;
            // 
            // icbLanguage
            // 
            this.icbLanguage.AutoSize = true;
            this.icbLanguage.Checked = true;
            this.icbLanguage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.icbLanguage.Location = new System.Drawing.Point(223, 157);
            this.icbLanguage.Name = "icbLanguage";
            this.icbLanguage.Size = new System.Drawing.Size(74, 17);
            this.icbLanguage.TabIndex = 33;
            this.icbLanguage.Text = "Language";
            this.icbLanguage.UseVisualStyleBackColor = true;
            // 
            // IcbGenre
            // 
            this.IcbGenre.AutoSize = true;
            this.IcbGenre.Checked = true;
            this.IcbGenre.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IcbGenre.Location = new System.Drawing.Point(223, 183);
            this.IcbGenre.Name = "IcbGenre";
            this.IcbGenre.Size = new System.Drawing.Size(55, 17);
            this.IcbGenre.TabIndex = 34;
            this.IcbGenre.Text = "Genre";
            this.IcbGenre.UseVisualStyleBackColor = true;
            // 
            // IcbPicture
            // 
            this.IcbPicture.AutoSize = true;
            this.IcbPicture.Checked = true;
            this.IcbPicture.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IcbPicture.Location = new System.Drawing.Point(223, 243);
            this.IcbPicture.Name = "IcbPicture";
            this.IcbPicture.Size = new System.Drawing.Size(59, 17);
            this.IcbPicture.TabIndex = 35;
            this.IcbPicture.Text = "Picture";
            this.IcbPicture.UseVisualStyleBackColor = true;
            // 
            // IcbReleaseDate
            // 
            this.IcbReleaseDate.AutoSize = true;
            this.IcbReleaseDate.Checked = true;
            this.IcbReleaseDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IcbReleaseDate.Location = new System.Drawing.Point(223, 276);
            this.IcbReleaseDate.Name = "IcbReleaseDate";
            this.IcbReleaseDate.Size = new System.Drawing.Size(88, 17);
            this.IcbReleaseDate.TabIndex = 36;
            this.IcbReleaseDate.Text = "ReleaseDate";
            this.IcbReleaseDate.UseVisualStyleBackColor = true;
            // 
            // McbPicture
            // 
            this.McbPicture.AutoSize = true;
            this.McbPicture.Checked = true;
            this.McbPicture.CheckState = System.Windows.Forms.CheckState.Checked;
            this.McbPicture.Location = new System.Drawing.Point(224, 177);
            this.McbPicture.Name = "McbPicture";
            this.McbPicture.Size = new System.Drawing.Size(59, 17);
            this.McbPicture.TabIndex = 47;
            this.McbPicture.Text = "Picture";
            this.McbPicture.UseVisualStyleBackColor = true;
            // 
            // McbDeveloper
            // 
            this.McbDeveloper.AutoSize = true;
            this.McbDeveloper.Checked = true;
            this.McbDeveloper.CheckState = System.Windows.Forms.CheckState.Checked;
            this.McbDeveloper.Location = new System.Drawing.Point(224, 130);
            this.McbDeveloper.Name = "McbDeveloper";
            this.McbDeveloper.Size = new System.Drawing.Size(75, 17);
            this.McbDeveloper.TabIndex = 46;
            this.McbDeveloper.Text = "Developer";
            this.McbDeveloper.UseVisualStyleBackColor = true;
            // 
            // mcbName
            // 
            this.mcbName.AutoSize = true;
            this.mcbName.Checked = true;
            this.mcbName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mcbName.Location = new System.Drawing.Point(225, 106);
            this.mcbName.Name = "mcbName";
            this.mcbName.Size = new System.Drawing.Size(54, 17);
            this.mcbName.TabIndex = 45;
            this.mcbName.Text = "Name";
            this.mcbName.UseVisualStyleBackColor = true;
            // 
            // mcbGenre
            // 
            this.mcbGenre.AutoSize = true;
            this.mcbGenre.Checked = true;
            this.mcbGenre.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mcbGenre.Location = new System.Drawing.Point(224, 153);
            this.mcbGenre.Name = "mcbGenre";
            this.mcbGenre.Size = new System.Drawing.Size(55, 17);
            this.mcbGenre.TabIndex = 48;
            this.mcbGenre.Text = "Genre";
            this.mcbGenre.UseVisualStyleBackColor = true;
            // 
            // InternetData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 414);
            this.Controls.Add(this.tabControl1);
            this.Name = "InternetData";
            this.Text = "InternetData";
            this.tabControl1.ResumeLayout(false);
            this.tpIMDB.ResumeLayout(false);
            this.tpIMDB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ipbPicture)).EndInit();
            this.tpMetacritic.ResumeLayout(false);
            this.tpMetacritic.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbmPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpIMDB;
        private System.Windows.Forms.Button IbtnSendToEdit;
        private System.Windows.Forms.Button ibtnSendToNew;
        private System.Windows.Forms.Button IbtnCancel;
        private System.Windows.Forms.Label lbReleaseDate;
        private System.Windows.Forms.Label lbpicture;
        private System.Windows.Forms.Label lbGenre1;
        private System.Windows.Forms.Label lbGenre;
        private System.Windows.Forms.Label lbLanguage;
        private System.Windows.Forms.Label lbLenght;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.PictureBox ipbPicture;
        private System.Windows.Forms.TabPage tpMetacritic;
        private System.Windows.Forms.RadioButton rbSerieSelector;
        private System.Windows.Forms.RadioButton rbMovieSelector;
        private System.Windows.Forms.Button btnGetWebsite;
        private System.Windows.Forms.TextBox itbURL;
        private System.Windows.Forms.Button btnmGetWebsite;
        private System.Windows.Forms.TextBox mtbURL;
        private System.Windows.Forms.Button MbtnSendToEdit;
        private System.Windows.Forms.Button btnmGoToNew;
        private System.Windows.Forms.Button MbtnCancel;
        private System.Windows.Forms.Label MlbGenre;
        private System.Windows.Forms.Label MlbDeveloper;
        private System.Windows.Forms.Label MlbName;
        private System.Windows.Forms.PictureBox pbmPicture;
        private System.Windows.Forms.Label MlbPicture;
        private System.Windows.Forms.CheckBox IcbReleaseDate;
        private System.Windows.Forms.CheckBox IcbPicture;
        private System.Windows.Forms.CheckBox IcbGenre;
        private System.Windows.Forms.CheckBox icbLanguage;
        private System.Windows.Forms.CheckBox IcbGenre1;
        private System.Windows.Forms.CheckBox icbLenght;
        private System.Windows.Forms.CheckBox IcbName;
        private System.Windows.Forms.CheckBox mcbGenre;
        private System.Windows.Forms.CheckBox McbPicture;
        private System.Windows.Forms.CheckBox McbDeveloper;
        private System.Windows.Forms.CheckBox mcbName;
    }
}