﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataBase.Forms.Main
{
    public partial class SecuryCheck : Form
    {
        public SecuryCheck()
        {
            InitializeComponent();

            // Fixing the size
            this.MinimumSize = this.Size;
            this.MaximumSize = this.Size;
            this.MaximizeBox = false;
        }

        private void BrnOk_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "Delete")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Your entry was not correct", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }

        private void SecuryCheck_Load(object sender, EventArgs e)
        {
            btnCancel.DialogResult = DialogResult.Cancel;
        }
    }
}
