﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace DataBase.Source.Data
{
    public class SqlGeneral
    {

        /// <summary>
        /// Tests a connection with the Database. By opneing and closing a connection
        /// </summary>
        /// <param name="connectionString"></param>
        public void ConnectionTest(string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                    // Opening the connection
                    connection.Open();

                    // Closeing the connection
                    connection.Close();     
            }
        }


      
        public class Software
        {

            /////// Varibles ///////

            // Connection string for the software Database
            private string _connectionString = ConfigurationManager.ConnectionStrings["DataBase.Properties.Settings.SoftwareConnectionString"].ConnectionString;

            /////// Propoties //////
            
            public string ConnectionString
            {
                get
                {
                    return _connectionString;
                }
                private set
                {
                    _connectionString = value;
                }
            }

            /////// Methods ///////

            /// <summary>
            /// Getting the Data from a table
            /// </summary>
            /// <param name="tableName">The table where to get the data from</param>
            /// <param name="dataGridView">The Data Grid View</param>
            public void LoadTable(string tableName, DataGridView dataGridView)
            {
                // Makeing the query
                string _query = "SELECT * FROM " + tableName;

                // Makeing the connection with the sql databasee
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlDataAdapter adapter = new SqlDataAdapter(_query, connection))
                {
                    try
                    {
                        // Opening the connection
                        connection.Open();

                        // Creating a new data table to store the data
                        DataTable _DataTable = new DataTable();

                        // Puting tthe info in the datatable sourece
                        adapter.Fill(_DataTable);

                        // Loading the data in the datagrid view
                        dataGridView.DataSource = _DataTable;

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }                 
                }
            }



            /// <summary>
            /// Adding a new row to a selected table
            /// </summary>
            /// <param name="softwareData">The Software Data that need to entered. Dont forget to add the table to this</param>
            public void AddToTable(string tableName, SoftwareData softwareData)
            {
                // Makeing the query
                string query = "INSERT INTO " + tableName + " VALUES (@Name, @Developer, @Publisher, @Version, @SubCategory, @CrackType, @TorrentWebLink, @Custom, @Custom1, @Size, @Location, @PictureLocation)";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Name", softwareData.Name);
                        command.Parameters.AddWithValue("@Developer", softwareData.Developer);
                        command.Parameters.AddWithValue("@Publisher", softwareData.Publisher);
                        command.Parameters.AddWithValue("@Version", softwareData.Version);
                        command.Parameters.AddWithValue("@SubCategory", softwareData.SubCategory);
                        command.Parameters.AddWithValue("@CrackType", softwareData.CrackType);
                        command.Parameters.AddWithValue("@TorrentWebLink", softwareData.TorrentWebLink);
                        command.Parameters.AddWithValue("@Custom", softwareData.Custom);
                        command.Parameters.AddWithValue("@Custom1", softwareData.Custom1);
                        command.Parameters.AddWithValue("@Size", softwareData.Size);
                        command.Parameters.AddWithValue("@Location", softwareData.Location);
                        command.Parameters.AddWithValue("@PictureLocation", softwareData.PictureLocation);

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Removes a row from a table
            /// </summary>
            /// <param name="table">The Selected Table</param>
            /// <param name="rowIndex">Selected row index Id</param>
            public void RemoveFromTable(string table, int rowIndex)
            {
                try
                {
                    // Makeing the query
                    string query = "DELETE FROM " + table + " WHERE Id=@ID";

                    // Makeing the sql connection
                    using (SqlConnection connection = new SqlConnection(_connectionString))
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing data to the command
                        command.Parameters.AddWithValue("@ID", rowIndex.ToString());

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }


            /// <summary>
            /// Updates A row Entety in the Selected Table
            /// </summary>
            /// <param name="softwareData">The filled software data struct dont forget for this you need the table and the row id</param>
            public void UpdateRowInTable(string tableName, int rowId, SoftwareData softwareData)
            {
                // Makeing the query
                string query = "UPDATE " + tableName + " SET Name = @Name, Developer = @Developer, Publisher = @Publisher, Version = @Version, SubCategory = @SubCategory, CrackType = @CrackType, TorrentWebSiteLink = @TorrentWebSiteLink, Custom = @Custom, Custom1 = @Custom1, Size = @Size, Location = @Location WHERE Id = @Id";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Id", rowId);
                        command.Parameters.AddWithValue("@Name", softwareData.Name);
                        command.Parameters.AddWithValue("@Developer", softwareData.Developer);
                        command.Parameters.AddWithValue("@Publisher", softwareData.Publisher);
                        command.Parameters.AddWithValue("@Version", softwareData.Version);
                        command.Parameters.AddWithValue("@SubCategory", softwareData.SubCategory);
                        command.Parameters.AddWithValue("@CrackType", softwareData.CrackType);
                        command.Parameters.AddWithValue("@TorrentWebSiteLink", softwareData.TorrentWebLink);
                        command.Parameters.AddWithValue("@Custom", softwareData.Custom);
                        command.Parameters.AddWithValue("@Custom1", softwareData.Custom1);
                        command.Parameters.AddWithValue("@Size", softwareData.Size);
                        command.Parameters.AddWithValue("@Location", softwareData.Location);
                        command.Parameters.AddWithValue("@PictureLocation", softwareData.PictureLocation);

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Setting the new PictureLocation of a row
            /// </summary>
            /// <param name="softwareData">Just the picture has to filled and row id, table name have to be avalible</param>
            public void UpdatePictureLocation(string tableName, int rowId, string pictureLocation)
            {
                // Makeing the query
                string query = "UPDATE " + tableName + " SET PictureLocation = @PictureLocation WHERE Id = @Id";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Id", rowId);
                        command.Parameters.AddWithValue("@PictureLocation", pictureLocation);

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }  


            /// <summary>
            /// Setting the new PictureLocation of a row
            /// </summary>
            /// <param name="tableName"></param>
            /// <param name="rowId"></param>
            /// <param name="folderLocation"></param>
            public void UpdateFolderLocation(string tableName, int rowId, string folderLocation)
            {
                // Makeing the query
                string query = "UPDATE " + tableName + " SET Location = @Location WHERE Id = @Id";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Id", rowId);
                        command.Parameters.AddWithValue("@Location", folderLocation);

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Removeing a table form the Database
            /// </summary>
            /// <param name="tableName">Table Name to Remove</param>
            public void RemoveTable(string tableName)
            {
                // Makeing the query
                string query = "DROP TABLE " + tableName;

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Get a list of names of all the table name's in the database
            /// </summary>
            /// <returns>List Table Name's</returns>
            public List<string> AllTablesNames()
            {
                // Makeing a sql Connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    // Makeing a list array
                    List<string> tables = new List<string>();

                    try
                    {
                        // Opening the connection
                        connection.Open();

                        // Getting the data from the database
                        DataTable dataTable = connection.GetSchema("Tables");

                        // Setting the values of the array
                        foreach (DataRow row in dataTable.Rows)
                        {
                            string tableName = (string)row[2];
                            tables.Add(tableName);
                        }

                        // Close the connection
                        connection.Close();
                        return tables;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return tables;
                    }
                }    
            }


            /// <summary>
            /// Creates a new table in the database
            /// </summary>
            /// <param name="tableName">The new name of the new table</param>
            public void CreateTable(string tableName)
            {
                // Makeing the query
                string query = "CREATE TABLE " + tableName +
                    "(Id INT IDENTITY NOT NULL, Name VARCHAR (90) NOT NULL, Developer VARCHAR (90), Publisher VARCHAR (90), Version VARCHAR (100), SubCategory VARCHAR (90), CrackType VARCHAR (90), TorrentWebSiteLink VARCHAR (300), Custom VARCHAR (140), Custom1 VARCHAR (140), Size DECIMAL(10,2), Location VARCHAR (400), PictureLocation VARCHAR (400))";

                // Crateing a new sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection
                        connection.Open();

                        // Run the command
                        command.ExecuteNonQuery();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }
        }



        public class Games
        {


            // Connection string for the Games Database
            private string _connectionString = ConfigurationManager.ConnectionStrings["DataBase.Properties.Settings.GamesConnectionString"].ConnectionString;


            public string ConnectionString
            {
                get
                {
                    return _connectionString;
                }
                private set
                {
                    _connectionString = value;
                }
            }
            

            /// <summary>
            /// Getting the Data from a table
            /// </summary>
            /// <param name="tableName">The table where to get the data from</param>
            /// <param name="dataGridView">The Data Grid View</param>
            public void LoadTable(string tableName, DataGridView dataGridView)
            {
                // Makeing the query
                string query = "SELECT * FROM " + tableName;

                // Makeing the connection with the sql databasee
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
                {
                    try
                    {
                        // Opening the connection
                        connection.Open();

                        // Creating a new data table to store the data
                        DataTable dataTable = new DataTable();

                        // Puting tthe info in the datatable sourece
                        adapter.Fill(dataTable);

                        // Loading the data in the datagrid view
                        dataGridView.DataSource = dataTable;

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Adding a row to the table.
            /// </summary>
            /// <param name="tableName">The selected table name.</param>
            /// <param name="gameData">The data That sould be past true.</param>
            public void AddToTable(string tableName, GameData gameData)
            {
                // Makeing the query
                string query = "INSERT INTO " + tableName + " VALUES (@Name, @Developer, @Publisher, @Version, @Genre, @CrackType, @TorrentWebLink, @Custom, @Custom1, @Size, @Location, @PictureLocation)";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Name", gameData.Name);
                        command.Parameters.AddWithValue("@Developer", gameData.Developer);
                        command.Parameters.AddWithValue("@Publisher", gameData.Publisher);
                        command.Parameters.AddWithValue("@Version", gameData.Version);
                        command.Parameters.AddWithValue("@Genre", gameData.Genre);
                        command.Parameters.AddWithValue("@CrackType", gameData.CrackType);
                        command.Parameters.AddWithValue("@TorrentWebLink", gameData.TorrentWebLink);
                        command.Parameters.AddWithValue("@Custom", gameData.Custom);
                        command.Parameters.AddWithValue("@Custom1", gameData.Custom1);
                        command.Parameters.AddWithValue("@Size", gameData.Size);
                        command.Parameters.AddWithValue("@Location", gameData.Location);
                        command.Parameters.AddWithValue("@PictureLocation", gameData.PictureLocation);

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Removes a row from a table
            /// </summary>
            /// <param name="tableName">The Selected Table</param>
            /// <param name="rowId">Selected row index Id</param>
            public void RemoveFromTable(string tableName, int rowId)
            {
                // Makeing the query
                string query = "DELETE FROM " + tableName + " WHERE Id=@ID";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing data to the command
                        command.Parameters.AddWithValue("@ID", rowId.ToString());

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Updates A row Entety in the Selected Table
            /// </summary>
            /// <param name="tableName">The Selected Table</param>
            /// <param name="rowId">The Selected Row</param>
            /// <param name="name"></param>
            /// <param name="developer"></param>
            /// <param name="publisher"></param>
            /// <param name="version"></param>
            /// <param name="_SubCategory"></param>
            /// <param name="crackType"></param>
            /// <param name="torrentWebsiteLink"></param>
            /// <param name="custom"></param>
            /// <param name="custom1"></param>
            /// <param name="size"></param>
            public void UpdateRowInTable(string tableName, int rowId, GameData gameData)
            {
                // Makeing the query
                string query = "UPDATE " + tableName + " SET Name = @Name, Developer = @Developer, Publisher = @Publisher, Version = @Version, Genre = @Genre, CrackType = @CrackType, TorrentWebSiteLink = @TorrentWebSiteLink, Custom = @Custom, Custom1 = @Custom1, Size = @Size, Location = @Location WHERE Id = @Id";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Id", rowId);
                        command.Parameters.AddWithValue("@Name", gameData.Name);
                        command.Parameters.AddWithValue("@Developer", gameData.Developer);
                        command.Parameters.AddWithValue("@Publisher", gameData.Publisher);
                        command.Parameters.AddWithValue("@Version", gameData.Version);
                        command.Parameters.AddWithValue("@Genre", gameData.Genre);
                        command.Parameters.AddWithValue("@CrackType", gameData.CrackType);
                        command.Parameters.AddWithValue("@TorrentWebSiteLink", gameData.TorrentWebLink);
                        command.Parameters.AddWithValue("@Custom", gameData.Custom);
                        command.Parameters.AddWithValue("@Custom1", gameData.Custom1);
                        command.Parameters.AddWithValue("@Size", gameData.Size);
                        command.Parameters.AddWithValue("@Location", gameData.Location);

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Setting the new Picture location
            /// </summary>
            /// <param name="tableName"></param>
            /// <param name="rowId"></param>
            /// <param name="pictureLocation"></param>
            public void UpdatePictureLocation(string tableName, int rowId, string pictureLocation)
            {
                // Makeing the query
                string query = "UPDATE " + tableName + " SET PictureLocation = @PictureLocation WHERE Id = @Id";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Id", rowId);
                        command.Parameters.AddWithValue("@PictureLocation", pictureLocation);

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Changeing the folder Location
            /// </summary>
            /// <param name="tableName"></param>
            /// <param name="rowId"></param>
            /// <param name="pictureLocation"></param>
            public void UpdateFolderLocation(string tableName, int rowId, string folderLocation)
            {
                // Makeing the query
                string query = "UPDATE " + tableName + " SET Location = @Location WHERE Id = @Id";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Id", rowId);
                        command.Parameters.AddWithValue("@Location", folderLocation);

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Removeing a table form the Database
            /// </summary>
            /// <param name="TableName">Table Name to Remove</param>
            public void RemoveTable(string TableName)
            {
                // Makeing the query
                string _query = "DROP TABLE " + TableName;

                // Makeing the sql connection
                using (SqlConnection _Connection = new SqlConnection(_connectionString))
                using (SqlCommand _Command = new SqlCommand(_query, _Connection))
                {
                    try
                    {
                        // Opeing the connection 
                        _Connection.Open();

                        // Executeing the query
                        _Command.ExecuteScalar();

                        // Closeing the connection
                        _Connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Get a list of names of all the table name's in the database
            /// </summary>
            /// <returns>List Table Name's</returns>
            public List<string> AllTablesNames()
            {
                // Makeing a sql Connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    // Makeing a list array
                    List<string> tables = new List<string>();

                    try
                    {
                        // Opening the connection
                        connection.Open();

                        // Getting the data from the database
                        DataTable dataTable = connection.GetSchema("Tables");

                        // Setting the values of the array
                        foreach (DataRow row in dataTable.Rows)
                        {
                            string tableName = (string)row[2];
                            tables.Add(tableName);
                        }

                        // Close the connection
                        connection.Close();
                        return tables;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return tables;
                    }
                }
            }


            /// <summary>
            /// Creates a new table in the database
            /// </summary>
            /// <param name="tableName">The new name of the new table</param>
            public void CreateTable(string tableName)
            {
                // Makeing the query
                string query = "CREATE TABLE " + tableName +
                    "(Id INT IDENTITY NOT NULL, Name VARCHAR (90) NOT NULL, Developer VARCHAR (90), Publisher VARCHAR (90), Version VARCHAR (100), Genre VARCHAR (90), CrackType VARCHAR (90), TorrentWebSiteLink VARCHAR (300), Custom VARCHAR (140), Custom1 VARCHAR (140), Size DECIMAL(10,2), Location VARCHAR (400), PictureLocation VARCHAR (400))";

                // Crateing a new sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection
                        connection.Open();

                        // Run the command
                        command.ExecuteNonQuery();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

        }



        public class Movies
        {

            // Connection string for the Movies Database
            private string _connectionString = ConfigurationManager.ConnectionStrings["DataBase.Properties.Settings.MoviesConnectionString"].ConnectionString;



            /////// Propoties //////

            public string ConnectionString
            {
                get
                {
                    return _connectionString;
                }
                private set
                {
                    _connectionString = value;
                }
            }



            /////// Methods ///////


            /// <summary>
            /// Getting the Data from a table
            /// </summary>
            /// <param name="tableName">The table where to get the data from</param>
            /// <param name="dataGridView">The Data Grid View</param>
            public void LoadTable(string tableName, DataGridView dataGridView)
            {
                // Makeing the query
                string query = "SELECT * FROM " + tableName;

                // Makeing the connection with the sql databasee
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
                {
                    try
                    {
                        // Opening the connection
                        connection.Open();

                        // Creating a new data table to store the data
                        DataTable dataTable = new DataTable();

                        // Puting tthe info in the datatable sourece
                        adapter.Fill(dataTable);

                        // Loading the data in the datagrid view
                        dataGridView.DataSource = dataTable;

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Adding a new row to a selected table
            /// </summary>
            /// <param name="tableName">The selected or wished table to add the row to</param>
            /// <param name="name"></param>
            /// <param name="_Developer"></param>
            /// <param name="_Publisher"></param>
            /// <param name="_Version"></param>
            /// <param name="_SubCategory"></param>
            /// <param name="_CrackType"></param>
            /// <param name="_TorrentWebLink"></param>
            /// <param name="custom"></param>
            /// <param name="_custom1"></param>
            public void AddToTable(string tableName, MovieData movieData)
            {
                // Makeing the query
                string query = "INSERT INTO " + tableName + " VALUES (@Name, @Studio, @Lenght, @Genre, @Genre1, @Genre2, @Language, @Language1, @subtitles, @subtitles1, @Custom, @Custom1, @Size, @Location, @PictureLocation)";


                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Name", movieData.Name);
                        command.Parameters.AddWithValue("@Studio", movieData.Studio);
                        command.Parameters.AddWithValue("@Lenght", movieData.Lenght);
                        command.Parameters.AddWithValue("@Genre", movieData.Genre);
                        command.Parameters.AddWithValue("@Genre1", movieData.Genre1);
                        command.Parameters.AddWithValue("@Genre2", movieData.Genre2);
                        command.Parameters.AddWithValue("@Language", movieData.Language);
                        command.Parameters.AddWithValue("@Language1", movieData.Language1);
                        command.Parameters.AddWithValue("@subtitles", movieData.Subtitles);
                        command.Parameters.AddWithValue("@subtitles1", movieData.Subtitles1);
                        command.Parameters.AddWithValue("@Custom", movieData.Custom);
                        command.Parameters.AddWithValue("@Custom1", movieData.Custom1);
                        command.Parameters.AddWithValue("@Size", movieData.Size);
                        command.Parameters.AddWithValue("@Location", movieData.Location);
                        command.Parameters.AddWithValue("@PictureLocation", movieData.PictureLocation);

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Removes a row from a table
            /// </summary>
            /// <param name="tableName">The Selected Table</param>
            /// <param name="rowId">Selected row index Id</param>
            public void RemoveFromTable(string tableName, int rowId)
            {
                // Makeing the query
                string query = "DELETE FROM " + tableName + " WHERE Id=@ID";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing data to the command
                        command.Parameters.AddWithValue("@ID", rowId.ToString());

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Updates A row Entety in the Selected Table
            /// </summary>
            /// <param name="tableName">The Selected Table</param>
            /// <param name="rowId">The Selected Row</param>
            /// <param name="name"></param>
            /// <param name="_Developer"></param>
            /// <param name="_Publisher"></param>
            /// <param name="_Version"></param>
            /// <param name="_SubCategory"></param>
            /// <param name="_CrackType"></param>
            /// <param name="_TorrentWebLink"></param>
            /// <param name="custom"></param>
            /// <param name="custom1"></param>
            /// <param name="size"></param>
            public void UpdateRowInTable(string tableName, int rowId, MovieData movieData)
            {
                // Makeing the query
                string query = "UPDATE " + tableName + " SET Name = @Name, Studio = @Studio, Lenght = @Lenght, Genre = @Genre, Genre1 = @Genre1, Genre2 = @Genre2, Language = @Language, Language1 = @Language1, subtitles = @subtitles, subtitles1 = @subtitles1, Custom = @Custom, Custom1 = @Custom1, Size = @Size, Location = @Location WHERE Id = @Id";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Id", rowId);
                        command.Parameters.AddWithValue("@Name", movieData.Name);
                        command.Parameters.AddWithValue("@Studio", movieData.Studio);
                        command.Parameters.AddWithValue("@Lenght", movieData.Lenght);
                        command.Parameters.AddWithValue("@Genre", movieData.Genre);
                        command.Parameters.AddWithValue("@Genre1", movieData.Genre1);
                        command.Parameters.AddWithValue("@Genre2", movieData.Genre2);
                        command.Parameters.AddWithValue("@Language", movieData.Language);
                        command.Parameters.AddWithValue("@Language1", movieData.Language1);
                        command.Parameters.AddWithValue("@subtitles", movieData.Subtitles);
                        command.Parameters.AddWithValue("@subtitles1", movieData.Subtitles1);
                        command.Parameters.AddWithValue("@Custom", movieData.Custom);
                        command.Parameters.AddWithValue("@Custom1", movieData.Custom1);
                        command.Parameters.AddWithValue("@Size", movieData.Size);
                        command.Parameters.AddWithValue("@Location", movieData.Location);

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Setting the new picture location of the new picture
            /// </summary>
            /// <param name="tableName"></param>
            /// <param name="rowId"></param>
            /// <param name="pictureLocation"></param>
            public void UpdatePictureLocation(string tableName, int rowId, string pictureLocation)
            {
                // Makeing the query
                string query = "UPDATE " + tableName + " SET PictureLocation = @PictureLocation WHERE Id = @Id";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Id", rowId);
                        command.Parameters.AddWithValue("@PictureLocation", pictureLocation);

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Setting the new folderlocation
            /// </summary>
            /// <param name="tableName"></param>
            /// <param name="rowId"></param>
            /// <param name="pictureLocation"></param>
            public void UpdateFolderLocation(string tableName, int rowId, string folderLocation)
            {
                // Makeing the query
                string query = "UPDATE " + tableName + " SET Location = @Location WHERE Id = @Id";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Id", rowId);
                        command.Parameters.AddWithValue("@Location", folderLocation);

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Removeing a table form the Database
            /// </summary>
            /// <param name="tableName">Table Name to Remove</param>
            public void RemoveTable(string tableName)
            {
                // Makeing the query
                string query = "DROP TABLE " + tableName;

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Get a list of names of all the table name's in the database
            /// </summary>
            /// <returns>List Table Name's</returns>
            public List<string> AllTablesNames()
            {
                // Makeing a sql Connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    // Makeing a list array
                    List<string> tables = new List<string>();

                    try
                    {
                        // Opening the connection
                        connection.Open();

                        // Getting the data from the database
                        DataTable dataTable = connection.GetSchema("Tables");

                        // Setting the values of the array
                        foreach (DataRow row in dataTable.Rows)
                        {
                            string tablename = (string)row[2];
                            tables.Add(tablename);
                        }

                        // Close the connection
                        connection.Close();
                        return tables;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return tables;
                    }
                }
            }


            /// <summary>
            /// Creates a new table in the database
            /// </summary>
            /// <param name="tableName">The new name of the new table</param>
            public void CreateTable(string tableName)
            {
                // Makeing the query
                string query = "CREATE TABLE " + tableName +
                    "(Id INT IDENTITY NOT NULL, Name VARCHAR (90) NOT NULL, Studio VARCHAR (90), Lenght INT, Genre VARCHAR (100), Genre1 VARCHAR (90), Genre2 VARCHAR (90), Language VARCHAR (90), Language1 VARCHAR (90), subtitles VARCHAR (90), subtitles1 VARCHAR (90), Custom VARCHAR (140), Custom1 VARCHAR (140), Size DECIMAL(10,2), Location VARCHAR (400), PictureLocation VARCHAR (255))";

                // Crateing a new sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection
                        connection.Open();

                        // Run the command
                        command.ExecuteNonQuery();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }
        }



        public class Series
        {

            // Connection string for the Series Database
            private string _connectionString = ConfigurationManager.ConnectionStrings["DataBase.Properties.Settings.SeriesConnectionString"].ConnectionString;


            /////// Propoties //////

            public string ConnectionString
            {
                get
                {
                    return _connectionString;
                }
                private set
                {
                    _connectionString = value;
                }
            }



            /////// Methods ///////


            /// <summary>
            /// Getting the Data from a table
            /// </summary>
            /// <param name="tableName">The table where to get the data from</param>
            /// <param name="dataGridView">The Data Grid View</param>
            public void LoadTable(string tableName, DataGridView dataGridView)
            {
                // Makeing the query
                string query = "SELECT * FROM " + tableName;

                // Makeing the connection with the sql databasee
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
                {
                    try
                    {
                        // Opening the connection
                        connection.Open();

                        // Creating a new data table to store the data
                        DataTable dataTable = new DataTable();

                        // Puting tthe info in the datatable sourece
                        adapter.Fill(dataTable);

                        // Loading the data in the datagrid view
                        dataGridView.DataSource = dataTable;

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Adding a new row to a selected table
            /// </summary>
            /// <param name="tableName">The selected or wished table to add the row to</param>
            /// <param name="name"></param>
            /// <param name="_Developer"></param>
            /// <param name="_Publisher"></param>
            /// <param name="_Version"></param>
            /// <param name="_SubCategory"></param>
            /// <param name="_CrackType"></param>
            /// <param name="_TorrentWebLink"></param>
            /// <param name="custom"></param>
            /// <param name="_custom1"></param>
            public void AddToTable(string tableName, SerieData serieData)
            {
                // Makeing the query
                string query = "INSERT INTO " + tableName + " VALUES (@Name, @Studio, @Episode, @Season, @Quality, @Lenght, @Genre, @Genre1, @Language, @CheakNewlink, @Nextseasondate, @Custom, @Custom1, @Size, @Location, @PictureLocation)";




                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Name", serieData.Name);
                        command.Parameters.AddWithValue("@Studio", serieData.Studio);
                        command.Parameters.AddWithValue("@Episode", serieData.Episode);
                        command.Parameters.AddWithValue("@Season", serieData.Season);
                        command.Parameters.AddWithValue("@Quality", serieData.Quality);
                        command.Parameters.AddWithValue("@Lenght", serieData.Lenght);
                        command.Parameters.AddWithValue("@Genre", serieData.Genre);
                        command.Parameters.AddWithValue("@Genre1", serieData.Genre1);
                        command.Parameters.AddWithValue("@Language", serieData.Language);
                        command.Parameters.AddWithValue("@CheakNewlink", serieData.CheckNewLink);
                        command.Parameters.AddWithValue("@Nextseasondate", serieData.NextSeasonData);
                        command.Parameters.AddWithValue("@Custom", serieData.Custom);
                        command.Parameters.AddWithValue("@Custom1", serieData.Custom1);
                        command.Parameters.AddWithValue("@Size", serieData.Size);
                        command.Parameters.AddWithValue("@Location", serieData.Location);
                        command.Parameters.AddWithValue("@PictureLocation", serieData.PictureLocation);

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Updates A row Entety in the Selected Table
            /// </summary>
            /// <param name="tableName">The Selected Table</param>
            /// <param name="rowId">The Selected Row</param>
            /// <param name="name"></param>
            /// <param name="_Developer"></param>
            /// <param name="_Publisher"></param>
            /// <param name="_Version"></param>
            /// <param name="_SubCategory"></param>
            /// <param name="_CrackType"></param>
            /// <param name="_TorrentWebLink"></param>
            /// <param name="custom"></param>
            /// <param name="custom1"></param>
            /// <param name="size"></param>
            public void UpdateRowInTable(string tableName, int rowId, SerieData serieData)
            {
                // Makeing the query
                string query = "UPDATE " + tableName + " SET Name = @Name, Studio = @Studio, Episode = @Episode, Season = @Season, Quality = @Quality, Lenght = @Lenght, Genre = @Genre, Genre1 = @Genre1, Language = @Language, CheakNewlink = @CheakNewlink, Nextseasondate = @Nextseasondate, Custom = @Custom, Custom1 = @Custom1, Size = @Size, Location = @Location WHERE Id = @Id";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Id", rowId);
                        command.Parameters.AddWithValue("@Name", serieData.Name);
                        command.Parameters.AddWithValue("@Studio", serieData.Studio);
                        command.Parameters.AddWithValue("@Episode", serieData.Episode);
                        command.Parameters.AddWithValue("@Season", serieData.Season);
                        command.Parameters.AddWithValue("@Quality", serieData.Quality);
                        command.Parameters.AddWithValue("@Lenght", serieData.Lenght);
                        command.Parameters.AddWithValue("@Genre", serieData.Genre);
                        command.Parameters.AddWithValue("@Genre1", serieData.Genre1);
                        command.Parameters.AddWithValue("@Language", serieData.Language);
                        command.Parameters.AddWithValue("@CheakNewlink", serieData.CheckNewLink);
                        command.Parameters.AddWithValue("@Nextseasondate", serieData.NextSeasonData);
                        command.Parameters.AddWithValue("@Custom", serieData.Custom);
                        command.Parameters.AddWithValue("@Custom1", serieData.Custom1);
                        command.Parameters.AddWithValue("@Size", serieData.Size);
                        command.Parameters.AddWithValue("@Location", serieData.Location);


                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Resetting the picturelocation
            /// </summary>
            /// <param name="tableName">The Name of the table where the value sould be changed</param>
            /// <param name="rwId">The row Id</param>
            /// <param name="pictureLocation">The new pictureLocation</param>
            public void UpdatePictureLocation(string tableName, int rwId, string pictureLocation)
            {
                // Makeing the query
                string query = "UPDATE " + tableName + " SET PictureLocation = @PictureLocation WHERE Id = @Id";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Id", rwId);
                        command.Parameters.AddWithValue("@PictureLocation", pictureLocation);


                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Setting the new folderlocation
            /// </summary>
            /// <param name="tableName"></param>
            /// <param name="rowId"></param>
            /// <param name="pictureLocation"></param>
            public void UpdateFolderLocation(string tableName, int rowId, string folderLocation)
            {
                // Makeing the query
                string query = "UPDATE " + tableName + " SET Location = @Location WHERE Id = @Id";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing Data to the command
                        command.Parameters.AddWithValue("@Id", rowId);
                        command.Parameters.AddWithValue("@Location", folderLocation);

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }



            /// <summary>
            /// Removes a row from a table
            /// </summary>
            /// <param name="tableName">The Selected Table</param>
            /// <param name="rowIndex">Selected row index Id</param>
            public void RemoveFromTable(string tableName, int rowIndex)
            {
                // Makeing the query
                string query = "DELETE FROM " + tableName + " WHERE Id=@ID";

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Parseing data to the command
                        command.Parameters.AddWithValue("@ID", rowIndex.ToString());

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Removeing a table form the Database
            /// </summary>
            /// <param name="tableName">Table Name to Remove</param>
            public void RemoveTable(string tableName)
            {
                // Makeing the query
                string query = "DROP TABLE " + tableName;

                // Makeing the sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection 
                        connection.Open();

                        // Executeing the query
                        command.ExecuteScalar();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


            /// <summary>
            /// Get a list of names of all the table name's in the database
            /// </summary>
            /// <returns>List Table Name's</returns>
            public List<string> AllTablesNames()
            {
                // Makeing a sql Connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    // Makeing a list array
                    List<string> tables = new List<string>();

                    try
                    {
                        // Opening the connection
                        connection.Open();

                        // Getting the data from the database
                        DataTable dataTable = connection.GetSchema("Tables");

                        // Setting the values of the array
                        foreach (DataRow row in dataTable.Rows)
                        {
                            string tablename = (string)row[2];
                            tables.Add(tablename);
                        }

                        // Close the connection
                        connection.Close();
                        return tables;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return tables;
                    }
                    
                }
            }


            /// <summary>
            /// Creates a new table in the database
            /// </summary>
            /// <param name="tableName">The new name of the new table</param>
            public void CreateTable(string tableName)
            {
                // Makeing the query
                string query = "CREATE TABLE " + tableName +
                    "(Id INT IDENTITY NOT NULL, Name VARCHAR (90) NOT NULL, Studio VARCHAR (90), Episode VARCHAR (90), Season VARCHAR (100), Quality VARCHAR (90), Lenght VARCHAR (90), Genre VARCHAR (90), Genre1 VARCHAR (90), Language VARCHAR (90), CheakNewlink VARCHAR (300), Nextseasondate VARCHAR (90), Custom VARCHAR (140), Custom1 VARCHAR (140), Size DECIMAL(10,2), Location VARCHAR (400), PictureLocation VARCHAR (400))";

                // Crateing a new sql connection
                using (SqlConnection connection = new SqlConnection(_connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        // Opeing the connection
                        connection.Open();

                        // Run the command
                        command.ExecuteNonQuery();

                        // Closeing the connection
                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


        }
    }






    // Exeptions
    [SerializableAttribute]
    [ComVisibleAttribute(true)]
    public class NoConnectionToDatabaseException : Exception
    {
        // Overrride the message so it shows this message when the execption is thrown
        public override string Message
        {
            get
            {
                return "There was a error with connecing to the database";
            }
        }


        /// <summary>
        /// Shows the expeption when there is a error connecing to the database
        /// </summary>
        public NoConnectionToDatabaseException() : base()
        {
            
        }

        /// <summary>
        /// Shows the expeption when there is a error connecing to the database
        /// </summary>
        /// <param name="message">Message you want to display</param>
        public NoConnectionToDatabaseException(string message) : base(message)
        {
            Console.WriteLine(message);
        }


    }


}
