﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DataBase.Source.Settings
{
    public static class GeneralSettings
    {

        private static UInt64 _majorVersion;
        private static UInt64 _minorVersion;
        private static UInt64 _buildVersion;


        public static UInt64 MajorVersion
        {
            get
            {
                return _majorVersion;
            }
            set
            {
                _majorVersion = value;
            }
        }

        public static UInt64 MinorVersion
        {
            get
            {
                return _minorVersion;
            }
            set
            {
                _minorVersion = value;
            }
        }

        public static UInt64 BuildVersion
        {
            get
            {
                return _buildVersion;
            }
            set
            {
                _buildVersion = value;
            }
        }



        static GeneralSettings()
        {
           
        }


        // Gets the version of the program
        public static void GetVersion()
        {

            uint major = Properties.Settings.Default.Major;
            uint minor = Properties.Settings.Default.Minor;
            uint build = Properties.Settings.Default.Build;

            _majorVersion = Convert.ToUInt64(major);
            _minorVersion = Convert.ToUInt64(minor);
            _buildVersion = Convert.ToUInt64(build);

#if DEBUG
            // Increment the build and save it
            Properties.Settings.Default.Build++;
            Properties.Settings.Default.Save();
#endif
        }


        /// <summary>
        /// Getting a picture location number
        /// </summary>
        public static string GetPictureName()
        {
            string pictureNumber;

            // Getting the Picture Number
            pictureNumber = Properties.Settings.Default.PictureName.ToString();

            // Adding a to the picture location
            Properties.Settings.Default.PictureName++;
            Properties.Settings.Default.Save();

            return pictureNumber;
        }

    }
}
