﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;

using DataBase.Source;


namespace DataBase.Forms.DataManegment
{
    public partial class ChangePicture : Form
    {

        private string _sourcePictureLocation = null;
        private string _newPictureLocation = null;
        private bool _downloadPicture = false;



        /// <summary>
        /// Change The picture Form
        /// </summary>
        /// <param name="sourcePictureLocation">The Old Picture location</param>
        public ChangePicture(string sourcePictureLocation)
        {
            InitializeComponent();

            string sourceLocation = sourcePictureLocation;
            const int maxSourcePictureLabelSize = 70;

            // Setting the old picture location
            _sourcePictureLocation = sourcePictureLocation;

            try
            {
                // Loading the old picture
                using (FileStream stream = new FileStream(_sourcePictureLocation, FileMode.Open, FileAccess.Read))
                {
                    pbSourcePicture.Image = Image.FromStream(stream);
                    stream.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            // If the picture string is too long for the label Then shorin it
            if (sourcePictureLocation.Length > maxSourcePictureLabelSize)
            {
                // Checks if the string is greather than 50 loop
                while (sourceLocation.Length > maxSourcePictureLabelSize)
                {
                    // Shorten the string by removeing a folder location from the begin to end
                    sourceLocation = sourceLocation.Substring(sourceLocation.IndexOf("\\") + 1);                  
                }
            }

            // Set the label to the current location of the picture
            lbSourcePictureLocation.Text = sourceLocation;

            // Setting the cancel button dialog result
            BtnCancel.DialogResult = DialogResult.Cancel;

            // Fixing the size to sure the form cant be resized
            this.MaximumSize = this.Size;
            this.MinimumSize = this.Size;
            this.MaximizeBox = false;

            // Settings for the picture boxes
            pbSourcePicture.SizeMode = PictureBoxSizeMode.StretchImage;
            PbNewPicture.SizeMode = PictureBoxSizeMode.StretchImage;
        }



        // Get Picture - Getting the new picture location via the open file dialog
        private void BtnGetPicture_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            //Setting the filers for the open file dialog
            openFileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";

            // Opeing the open file dialog
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Setting the new picture location to the file selected
                _newPictureLocation = openFileDialog.FileName;

                try
                {
                    // Loading the picture in the picture box
                    using (FileStream stream = new FileStream(_newPictureLocation, FileMode.Open, FileAccess.Read))
                    {
                        PbNewPicture.Image = Image.FromStream(stream);
                        stream.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                // Is the picture downloaded or not
                _downloadPicture = false;
            }
        }


        // Done - Copys or download the new picture and saves it in the old location
        private void btnDone_Click(object sender, EventArgs e)
        {
            // Clears the picture boxes
            Program.EmptyingPictureBoxes(pbSourcePicture);
            Program.EmptyingPictureBoxes(PbNewPicture);

            // This is moveing the picture
            if (_downloadPicture == false)
            {
                // Moveing the picture
                File.Move(_sourcePictureLocation, _newPictureLocation);
            }
            // This is downloading the picture
            else if (_downloadPicture == true)
            {
                // Downloading the picture
                using (WebClient client = new WebClient())
                {
                    try
                    {
                        client.DownloadFile(_newPictureLocation, _sourcePictureLocation);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n \n" + ex.InnerException);
                    }
                    finally
                    {
                        client.Dispose();
                    }
                }
            }

        }


        // Download Picture - Download the picture and saves the link to a varible
        private void btnDownloadPicture_Click(object sender, EventArgs e)
        {
            WebClient client;

            // Loading the picture from internet

            using (client = new WebClient())
            {
                try
                {
                    // loading the picture box from the internet
                    PbNewPicture.Load(tbNewPictureLocation.Text);

                    // setting the varible if the locading of the picture worked
                    _newPictureLocation = tbNewPictureLocation.Text;

                    // The picture is downloaded
                    _downloadPicture = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "\n \n" + ex.InnerException);
                }
                finally
                {
                    client.Dispose();
                }
            }
        }


    }
}
