﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataBase.Forms.Main
{
    public partial class SimpleTextBoxForm : Form
    {
        private string _returnText;

        public string ReturnText
        {
            get { return _returnText; }
            private set { _returnText = value; }
        }



        public SimpleTextBoxForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ReturnText = textBox1.Text;
            this.DialogResult = DialogResult.OK;
        }
    }
}
