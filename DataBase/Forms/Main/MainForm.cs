﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

using DataBase.Forms.DataManegment;
using DataBase.Forms.Tools;
using DataBase.Source;
using DataBase.Source.Data;
using DataBase.Source.Settings;



namespace DataBase.Forms.Main
{



    public partial class MainForm : Form
    {
        #region Inheritance

        // Classes varibles
        private SqlGeneral.Software _sqlSoftware = new SqlGeneral.Software();
        private SqlGeneral.Games _sqlGames = new SqlGeneral.Games();
        private SqlGeneral.Movies _sqlMovies = new SqlGeneral.Movies();
        private SqlGeneral.Series _sqlSeries = new SqlGeneral.Series();

        // Struct data
        SoftwareData _softwareDataStruct = new SoftwareData();
        GameData _gameDataStruct = new GameData();
        MovieData _movieDataStruct = new MovieData();
        SerieData _serieDataStruct = new SerieData();

        #endregion

        #region Varibles

        // General
        private int _selectedTabIndex;
        private List<string> _dataGridViewCellIndivitualRow = new List<string>();
        private bool _fillLabelsErrorShowAgain = true;
        private static bool _connectivity;
        private Connectivity _internetConnectivity;

        // Software
        private string _selectedSoftwareTable;
        private int _selectedSoftareDataGridRowIndex;
        private string _softwarePictureLocation;

        // Games
        private string _selectedGamesTable;
        private int _selectedGamesDataGridRowIndex;
        private string _gamesPictureLocation;

        // Movies
        private string _selectedMoviesTable;
        private int _selectedMoviesDataGridRowIndex;
        private string _moviesPictureLocation;

        // Series
        private string _selectedSeriesTable;
        private int _selectedSeriesDataGridRowIndex;
        private string _seriesPictureLocation;

        #endregion


        // Constuctor
        /// <summary>
        /// Form | Opens the Main Form
        /// </summary>
        public MainForm()
        {
            // Constucting all the componets.
            InitializeComponent();

            // Setting all the base settings in the form.
            BaseLoadSettings();
        }


        // When Form load's
        private void MainForm_Load(object sender, EventArgs e)
        {
            Program.LOG("Trying to make connection with database");

            // Checks if there is a connection with the database or else close the program.
            if (MakeConnection() == false)
            {
                Program.LOG("There was a error with connecting to the database", LogColor.Error);
                // If it cant make a connection then throw a exception the program wont work witout the database
                throw new NoConnectionToDatabaseException("Cant connect to database. This is a faital error");
            }

            // Load's all the table names to the combobox's.
            LoadTableNames();

            // Loading all the data in the data grid views.
            LoadTableDataGridView();

            // Checking if there interent and then setting the label.
            SetConnectivityStatus(new Connectivity().CheckForInternetConnection());

            // Close the spash screen when the form has loaded.
            SplashScreen.CloseThread();

            // Test the internet Connection
            _internetConnectivity = new Connectivity();
            _internetConnectivity.TimerInterval = Properties.Settings.Default.CheckingInternetIntervalTimer;
            _internetConnectivity.TimerStart();

            // Subsribeing to this method. Its called when the internet connectiity changes.
            _internetConnectivity.InternetConnectionChanged += Checking;
        }


        ////// Startup //////


        // Intial propoty settings for all the objects.
        private void BaseLoadSettings()
        {

            // Setting the labels.
            SlbCustom.Text = "";
            SlbDeveloper.Text = "";
            SlbName.Text = "";
            SlbPublisher.Text = "";
            SlbSize.Text = "";
            SlbSubCategory.Text = "";
            SlbTorrentWebSiteSearch.Text = "";
            SlbVersion.Text = "";

            GlbCustom.Text = "";
            GlbDeveloper.Text = "";
            GlbGenre.Text = "";
            GlbName.Text = "";
            GlbPublisher.Text = "";
            GlbSize.Text = "";
            GlbTorrentWebsiteSearch.Text = "";
            GlbVersion.Text = "";

            MlbCustom.Text = "";
            MlbGenre.Text = "";
            MlbLanguage.Text = "";
            MlbLenght.Text = "";
            MlbName.Text = "";
            MlbSize.Text = "";
            MlbStudio.Text = "";
            MlbSubtitles.Text = "";

            SElbCustom.Text = "";
            SElbEpisode.Text = "";
            SElbLanguage.Text = "";
            SElbName.Text = "";
            SElbNextSeasonDate.Text = "";
            SElbQuality.Text = "";
            SElbSeason.Text = "";
            SElbStudio.Text = "";


            // Setting the data grid view row index.
            _selectedSoftareDataGridRowIndex = 1;
            _selectedGamesDataGridRowIndex = 1;
            _selectedMoviesDataGridRowIndex = 1;
            _selectedSeriesDataGridRowIndex = 1;


            // Setting the picutrebox size mode's. So that the image will resize for the picture box.
            SpbPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            GpbPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            MpbPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            SEpbPicture.SizeMode = PictureBoxSizeMode.StretchImage;
        }



        // When The internet connection changes this is invoked
        private void Checking(object sender, System.EventArgs e)
        {
            Program.LOG("The Internet Connectivity has changed");

            SetConnectivityStatus(_internetConnectivity.CurrentInternetConnection);
        }

        // When TabControl Tab has Changed - Sets the index of the current selected tab and resizes the collums
        private void MainTC_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Sends a index of that tabcontrol back to a varible
            _selectedTabIndex = MainTC.SelectedIndex;

            // Resizes all the collums in all the data grid views.
            AutoResizeCollums();



            // HERE : Put everything you want to enable when the tab index changes
            combineToSeasonToolStripMenuItem.Enabled = false;



            // HERE : Put in when you want to change it This sould be enable or disable
            if (_selectedTabIndex == 0) // Software
            {

            }
            else if (_selectedTabIndex == 1) // Games
            {

            }
            else if (_selectedTabIndex == 2) // Movies
            {

            }
            else if (_selectedTabIndex == 3) // Series
            {
                combineToSeasonToolStripMenuItem.Enabled = true;
            }

        }

        // Form Size Changed - Setting the settings sizes for the objects in the form.
        private void MainForm_SizeChanged(object sender, EventArgs e)
        {
            double mainSize = MainTC.Size.Width;
            double newDoublePaddingSize;
            int newSize;

            // Calculating the new padding size.
            newDoublePaddingSize = mainSize / 4 / 2.34;

            // Converting the new padding size to a int to round it off.
            newSize = (int)newDoublePaddingSize;

            // Setting the padding of the tabcontrol to the new padding size.
            MainTC.Padding = new Point(newSize, 3);
        }


        #region GeneralMethods


        /// <summary>
        /// Tries to make a connection to the database files.
        /// </summary>
        /// <returns>True : If there is a database connection. False : If there is no connection.</returns>
        private bool MakeConnection()
        {
            const int timesToSkipQuestion = 5;
            int timesFailedToConnect = 0;

            // Loop That only ends if its true or false
            while (true)
            {
                try
                {
                    // Testing the connection with the databases
                    SqlGeneral sql = new SqlGeneral();
                    sql.ConnectionTest(_sqlSoftware.ConnectionString);
                    sql.ConnectionTest(_sqlGames.ConnectionString);
                    sql.ConnectionTest(_sqlMovies.ConnectionString);
                    sql.ConnectionTest(_sqlSeries.ConnectionString);

                    // Retun a true there is a connection with the Databases
                    return true;
                }
                catch (Exception ex)
                {
                    // Incoment the faild amout of times it connects
                    timesFailedToConnect++;

                    // If if failed so many times more than the Times To skip question than ask the question
                    if (timesFailedToConnect >= timesToSkipQuestion)
                    {
                        Program.LOG("There was a error with connecting to the Database" + ex.Message, LogColor.Error);

                        // Asking the question of you want to try and connect again
                        DialogResult messageBoxResult = MessageBox.Show(
                            ex.Message + "/n Do you want to try again",
                            "Error",
                            MessageBoxButtons.RetryCancel,
                            MessageBoxIcon.Error
                        );

                        // If he wants to retry else Retrun false
                        if (messageBoxResult != DialogResult.Retry)
                        {
                            return false;
                        }
                    }
                }
            }
        }


        /// <summary>
        ///  GGets the selected Table By the Selected tab index
        /// </summary>
        /// <param name="selectedTabIndex">The Selected Tab Index</param>
        /// <returns>The Selected table name in use</returns>
        private string GetOverallCurrentTable(int selectedTabIndex)
        {
            string selectedTable;

            // Check wich Tab is acctive in the TabControl
            switch (_selectedTabIndex)
            {
                case 0: // Software
                    selectedTable = _selectedSoftwareTable;
                    break;
                case 1: // Games
                    selectedTable = _selectedGamesTable;
                    break;
                case 2: // Movies
                    selectedTable = _selectedMoviesTable;
                    break;
                case 3: // Series
                    selectedTable = _selectedSeriesTable;
                    break;
                default: // Default
                    throw new InvalidOperationException();                 
            }
            return selectedTable;
        }


        /// <summary>
        /// Acctive Search searches true the data grid view and of something does not match then hide it.
        /// </summary>
        /// <param name="dataGridView">The Data Grid View To search True</param>
        /// <param name="search">The Search Textbox</param>
        private void SearchDataGridView(DataGridView dataGridView, TextBox search)
        {
            if (dataGridView.Rows.Count != 0)
            {
                CurrencyManager currencyManager = (CurrencyManager)BindingContext[dataGridView.DataSource];

                // If the textbox is empty then show everything.
                if (!String.IsNullOrEmpty(search.Text.ToString()))
                {
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        foreach (DataGridViewCell cell in row.Cells)
                        {
                            // Makeing sure that the value of the cell is not null
                            if (cell.Value != null)
                            {
                                // If the cell contains a letter that is the same then show it the rest hide
                                if (cell.Value.ToString().ToLower().Contains(search.Text.ToLower()))
                                {
                                    currencyManager.SuspendBinding();
                                    row.Visible = true;
                                    currencyManager.ResumeBinding();
                                    break;
                                }
                                else
                                {
                                    currencyManager.SuspendBinding();
                                    row.Visible = false;
                                    currencyManager.ResumeBinding();
                                }
                            }
                        }
                    }
                }
                else
                {
                    // Show everything when the textbox is empty
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        currencyManager.SuspendBinding();
                        row.Visible = true;
                        currencyManager.ResumeBinding();
                    }
                }
            }
        }


        /// <summary>
        /// Auto Resizes the collums of all the datagridviews
        /// </summary>
        private void AutoResizeCollums()
        {
            SoftwareDataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            GamesDataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            MoviesDataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            SeriesDataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }


        /// <summary>
        /// Load's all the Table name's to the Coombobox's
        /// </summary>
        private void LoadTableNames()
        {
            // First clear then load again
            ScbSelectTable.DataSource = null;
            GcbSelectedTable.DataSource = null;
            McbSelectedTable.DataSource = null;
            SEcbSelectedTable.DataSource = null;

            // Loading all the Names in the combobox for software
            ScbSelectTable.DataSource = _sqlSoftware.AllTablesNames();
            GcbSelectedTable.DataSource = _sqlGames.AllTablesNames();
            McbSelectedTable.DataSource = _sqlMovies.AllTablesNames();
            SEcbSelectedTable.DataSource = _sqlSeries.AllTablesNames();
        }


        /// <summary>
        /// Load's the data grid view with data from the selected table.
        /// </summary>
        private void LoadTableDataGridView()
        {
            // Loading the Datagrid views with data from its selected table
            if (!String.IsNullOrEmpty(_selectedSoftwareTable))
            {
                _sqlSoftware.LoadTable(_selectedSoftwareTable, SoftwareDataGridView);
            }

            if (!String.IsNullOrEmpty(_selectedGamesTable))
            {
                _sqlGames.LoadTable(_selectedGamesTable, GamesDataGridView);
            }

            if (!String.IsNullOrEmpty(_selectedMoviesTable))
            {
                _sqlMovies.LoadTable(_selectedMoviesTable, MoviesDataGridView);
            }

            if (!String.IsNullOrEmpty(_selectedSeriesTable))
            {
                _sqlSeries.LoadTable(_selectedSeriesTable, SeriesDataGridView);
            }

            Program.LOG("Reloaded the DataGridViews data");

            // Resizeing the DataGridView collums to a new size that data can fit
            AutoResizeCollums();
        }


        /// <summary>
        /// Setting the internet Connectivty status.
        /// </summary>
        /// <param name="internetConnectivity">If there is internet</param>
        private void SetConnectivityStatus(bool internetConnectivity)
        {
            _connectivity = internetConnectivity;


            // If there is internet
            if (internetConnectivity == true)
            {
                // Setting the label text
                InternetConectivery.Text = "Connected";
                // Setting the label color
                InternetConectivery.ForeColor = Color.Green;

            }
            else
            {
                // Setting the label text
                InternetConectivery.Text = "Disconnected";
                // Setting the label Color
                InternetConectivery.ForeColor = Color.Red;

                // Showing a message box telling the user that there is no interent and some fetures will be disabeld
                MessageBox.Show("There is no interent some fetures will be disabled");

                // Disable the futres that cannot be used without internet
                testToolStripMenuItem.Enabled = false;
                pictureToolStripMenuItem.Enabled = false;
                ChangePictureToolStripitem.Enabled = false;
            }
        }


        /// <summary>
        /// Making it nice by Up or down calculating the size So we can have a simple value and metric
        /// </summary>
        /// <param name="size">The size</param>
        /// <returns>The size in the new metric, string </returns>
        private string MakingSizeEasyerToRead(float size)
        {
            string metric = "MB";

            // If size is more than 1024 MB than recalcutate it to GB
            if (size > 1024.0f)
            {
                size = size / 1024.0f;
                metric = "GB";
            }
            // If size is less than 1 MB than recalulate it to KB
            else if (size < 1.0f)
            {
                size = size * 1024.0f;
                metric = "KB";
            }

            // Setting the value so it only has 2 decimal points
            size = (float)Convert.ToDouble(Math.Round(size, 2));

            // Making the string
            string fullSize = size.ToString() + " " + metric;

            return fullSize;
        }

        #endregion


        #region ButtonFunctions


        // Add -> Item in the Database Menu strip Button - Add a item to a table
        private void addItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Crating a form
            Add addItemForm = new Add(_selectedTabIndex, GetOverallCurrentTable(_selectedTabIndex));

            // Showing the from
            addItemForm.ShowDialog();

            // If the from was closed propoly with a OK
            if (addItemForm.DialogResult == DialogResult.OK)
            {
                // Loades all the Data to The Data Grid View
                LoadTableDataGridView();
            }
        }


        // Add -> Table - Adds a new table to the selected Tab Database
        private void tableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string tableName;

            // Creating a new form
            NewTable newTable = new NewTable();

            // Showing that form
            newTable.ShowDialog();

            // If the from was Runed ok And Ok was pressed
            if (newTable.DialogResult == DialogResult.OK)
            {
                List<string> tableNames = new List<string>();

                // Setting the new name for the table
                tableName = newTable.TableName;

                switch (_selectedTabIndex)
                {
                    case 0: // Software                      
                        tableNames = _sqlSoftware.AllTablesNames();
                        break;
                    case 1: // Games
                        tableNames = _sqlGames.AllTablesNames(); ;
                        break;
                    case 2: // Movies
                        tableNames = _sqlMovies.AllTablesNames();
                        break;
                    case 3: // Series
                        tableNames = _sqlMovies.AllTablesNames();
                        break;
                }

                try
                {
                    // Chekcing if the table already exist
                    foreach (string name in tableNames)
                    {
                        if (name.ToLower() == tableName)
                        {
                            throw new DuplicateWaitObjectException("Tables List", "There is already a table with the same name");
                        }
                    }

                    // Checking witch Tab in TabControl Was Selected
                    // Creating the new table and setting the combo box to the new table
                    switch (_selectedTabIndex)
                    {
                        case 0: // Software                      
                            _sqlSoftware.CreateTable(tableName);
                            _selectedSoftwareTable = tableName;
                            break;
                        case 1: // Games
                            _sqlGames.CreateTable(tableName);
                            _selectedGamesTable = tableName;
                            break;
                        case 2: // Movies
                            _sqlMovies.CreateTable(tableName);
                            _selectedMoviesTable = tableName;
                            break;
                        case 3: // Series
                            _sqlSeries.CreateTable(tableName);
                            _selectedSeriesTable = tableName;
                            break;
                    }

                    // Reloading the Data Grid View
                    LoadTableDataGridView();

                    // Reloading the Names in the Combobox
                    LoadTableNames();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }


        // Edit -> Item in the database menu strip button - Edit's a item from a table
        private void editItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowId = 0;
            string tableName = "";
            Edit editItemForm;

            switch (_selectedTabIndex)
            {
                case 0: // Software
                    rowId = _selectedSoftareDataGridRowIndex;
                    tableName = _selectedSoftwareTable;
                    editItemForm = new Edit(_selectedTabIndex, tableName, rowId, _softwareDataStruct);
                    break;
                case 1: // Games
                    rowId = _selectedGamesDataGridRowIndex;
                    tableName = _selectedGamesTable;
                    editItemForm = new Edit(_selectedTabIndex, tableName, rowId, _gameDataStruct);
                    break;
                case 2: // Movies
                    rowId = _selectedMoviesDataGridRowIndex;
                    tableName = _selectedMoviesTable;
                    editItemForm = new Edit(_selectedTabIndex, tableName, rowId, _movieDataStruct);
                    break;
                case 3: // Series
                    rowId = _selectedSeriesDataGridRowIndex;
                    tableName = _selectedSeriesTable;
                    editItemForm = new Edit(_selectedTabIndex, tableName, rowId, _serieDataStruct);
                    break;
                default:
                    throw new ApplicationException("There was a big internal error");
            }

            // Show the dialog
            editItemForm.ShowDialog();

            // Reload the DataGridViews
            LoadTableDataGridView();
        }


        // Program - ChangePicture - Opens the change picutre dialog
        private void changePictureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string pictureLocation = null;
            PictureBox selectedPictureBox = null;

            // Getting the picture location of the selected row
            switch (_selectedTabIndex)
            {
                case 0: // Software
                    pictureLocation = _softwarePictureLocation;
                    selectedPictureBox = SpbPicture;
                    break;
                case 1: // Games
                    pictureLocation = _gamesPictureLocation;
                    selectedPictureBox = GpbPicture;
                    break;
                case 2: // Movies
                    pictureLocation = _moviesPictureLocation;
                    selectedPictureBox = MpbPicture;
                    break;
                case 3: // Series
                    pictureLocation = _seriesPictureLocation;
                    selectedPictureBox = SEpbPicture;
                    break;
            }


            // Checking if the string has a picture
            if (!String.IsNullOrEmpty(pictureLocation))
            {
                // Unloading the picture
                selectedPictureBox.Image.Dispose();
                selectedPictureBox.Image = null;

                // Opeing the changePicture Form
                ChangePicture changepicture = new ChangePicture(pictureLocation);

                changepicture.ShowDialog();

                // Reload the images
                using (FileStream stream = new FileStream(pictureLocation, FileMode.Open, FileAccess.Read))
                {
                    // Resetig the correct picture box
                    switch (_selectedTabIndex)
                    {
                        case 0: // Software
                            SpbPicture.Image = Image.FromStream(stream);
                            stream.Dispose();
                            break;
                        case 1: // Games
                            GpbPicture.Image = Image.FromStream(stream);
                            stream.Dispose();
                            break;
                        case 2: // Movies
                            MpbPicture.Image = Image.FromStream(stream);
                            stream.Dispose();
                            break;
                        case 3: // Series
                            SEpbPicture.Image = Image.FromStream(stream);
                            stream.Dispose();
                            break;
                        default:
                            stream.Dispose();
                            break;
                    }
                }
            }
            else
            {
                MessageBox.Show("There was no picture found");
            }
        }


        // Remove -> Item in the database Menu strip button - Removes a row from a table
        private void removeItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Asking the user if he is sure he wants to delete this from the table
            if (MessageBox.Show("Are you sure you want to delete this row ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                // Delete the row from the table
                _sqlSoftware.RemoveFromTable(_selectedSoftwareTable, _selectedSoftareDataGridRowIndex);

                // Reloades the Data Grid View to update the deletion
                LoadTableDataGridView();
            }
        }


        // Remove -> Table - Removes a table from one of the 4 databases
        private void tableToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            // Asking if your sure you want to delete it
            if (MessageBox.Show("Are You sure you want to delete this table?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                // If the answer was yes then creat a new form with a textbox
                SecuryCheck securityCheck = new SecuryCheck();

                // Opening a new form
                securityCheck.ShowDialog();

                // Asking the person if he can retype to be 100% sure he want to delete a table
                if (securityCheck.DialogResult == DialogResult.OK)
                {
                    // Check wich Tab is acctive in the TabControl
                    // Delteing a table from the database
                    switch (_selectedTabIndex)
                    {
                        case 0: // Software
                            _sqlSoftware.RemoveTable(_selectedSoftwareTable);
                            break;
                        case 1: // Games
                            _sqlGames.RemoveTable(_selectedGamesTable);
                            break;
                        case 2: // Movies
                            _sqlMovies.RemoveTable(_selectedMoviesTable);
                            break;
                        case 3: // Series
                            _sqlSeries.RemoveTable(_selectedSeriesTable);
                            break;
                    }

                    // Reloading the names in the combobox
                    LoadTableNames();
                }
            }
        }


        // About - Menue Clicked - Opens the about form
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();

            about.Show();
        }


        // Add -> Picture - Adds A picture to a row
        private void pictureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddPicture picture = null;
            string pictureLocation = null;

            // Setting the picutre location and the picture update settings by tab
            switch (_selectedTabIndex)
            {
                case 0:
                    picture = new AddPicture(_selectedSoftareDataGridRowIndex, GetOverallCurrentTable(_selectedTabIndex), _selectedTabIndex);
                    pictureLocation = _softwarePictureLocation;
                    break;
                case 1:
                    picture = new AddPicture(_selectedGamesDataGridRowIndex, GetOverallCurrentTable(_selectedTabIndex), _selectedTabIndex);
                    pictureLocation = _gamesPictureLocation;
                    break;
                case 2:
                    picture = new AddPicture(_selectedMoviesDataGridRowIndex, GetOverallCurrentTable(_selectedTabIndex), _selectedTabIndex);
                    pictureLocation = _moviesPictureLocation;
                    break;
                case 3:
                    picture = new AddPicture(_selectedSeriesDataGridRowIndex, GetOverallCurrentTable(_selectedTabIndex), _selectedTabIndex);
                    pictureLocation = _seriesPictureLocation;
                    break;
            }

            // Checking if there is a picture there sould not be
            if (String.IsNullOrEmpty(pictureLocation))
            {
                // Show the dialog and reload the tables
                if (picture.ShowDialog() == DialogResult.OK)
                {
                    LoadTableDataGridView();
                }
            }
            else
            {
                MessageBox.Show("There is already a picture please use the change picture in programs", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        // Program -> Get Data From Internet - Getting data from the internet
        private void GetDataFromInterentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InternetData internetData;
            DialogResult result;
            int rowId = 0;


            // Checking witch row index sould be used
            switch (_selectedTabIndex)
            {
                case 0: // Software
                    rowId = _selectedSoftareDataGridRowIndex;
                    break;
                case 1: // Games
                    rowId = _selectedGamesDataGridRowIndex;
                    break;
                case 2: // Movies
                    rowId = _selectedMoviesDataGridRowIndex;
                    break;
                case 3: // Series
                    rowId = _selectedSeriesDataGridRowIndex;
                    break;
            }

            // Creatign the instance
            internetData = new InternetData(_selectedMoviesTable, _selectedSeriesTable, _selectedGamesTable, rowId);

            // Opeing the form 
            result = internetData.ShowDialog();

            // If it was sucessful then reload the datagridview
            if (result == DialogResult.OK)
            {
                LoadTableDataGridView();
            }
        }


        // Program -> Recheck internet - Rechecks if there is internet
        private void recheckInternetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool thereIsInternet;
            Console.WriteLine("Checking interent");

            thereIsInternet = new Connectivity().CheckForInternetConnection();

            // Setting the Labels and all the oters
            SetConnectivityStatus(thereIsInternet);

            // Geving info to the user
            if (thereIsInternet == true)
            {
                MessageBox.Show("There is interent");
            }
            else if (thereIsInternet == false)
            {
                MessageBox.Show("There is no interent");
            }
        }


        // Tools -> Change Folder Name - Change folder name dialog shows up custom tools
        private void renameFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                RenameFolder renameFolder;

                // Selecting witch database to use
                if (_selectedTabIndex == 0 && !String.IsNullOrEmpty(_softwareDataStruct.Name) && !String.IsNullOrEmpty(_softwareDataStruct.Location))
                {
                    renameFolder = new RenameFolder(_softwareDataStruct);
                }
                else if (_selectedTabIndex == 1 && !String.IsNullOrEmpty(_gameDataStruct.Name) && !String.IsNullOrEmpty(_gameDataStruct.Location))
                {
                    renameFolder = new RenameFolder(_gameDataStruct);
                }
                else if (_selectedTabIndex == 2 && !String.IsNullOrEmpty(_movieDataStruct.Name) && !String.IsNullOrEmpty(_movieDataStruct.Location))
                {
                    renameFolder = new RenameFolder(_movieDataStruct);
                }
                else
                {
                    throw new InvalidOperationException("You can only change a name of a folder series dont have folders. You need to have a locaiton with that row. Or there is a other problem");
                }

                // Opeing the form
                renameFolder.ShowDialog();

                // Makeing sure that the dialog result was entered
                if (renameFolder.DialogResult == DialogResult.OK)
                {
                    // Selecting the database and adding the form
                    if (_selectedTabIndex == 0) // Software
                    {
                        // Updateing the database
                        _sqlSoftware.UpdateFolderLocation(_selectedSoftwareTable, _selectedSoftareDataGridRowIndex, renameFolder.NewFolderLocationName);
                    }
                    else if (_selectedTabIndex == 1) // Games
                    {
                        // Updateing the database
                        _sqlGames.UpdateFolderLocation(_selectedGamesTable, _selectedGamesDataGridRowIndex, renameFolder.NewFolderLocationName);
                    }
                    else if (_selectedTabIndex == 2) // Movies
                    {
                        // Updateing the database
                        _sqlMovies.UpdateFolderLocation(_selectedMoviesTable, _selectedMoviesDataGridRowIndex, renameFolder.NewFolderLocationName);
                    }
                }

                // Refresh the datagrids
                LoadTableDataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        #endregion


        #region DataGridViewEvents

        // SoftWare - When a cell has been clicked - Retuns the Id of the selected row And Load the picture
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string pictureLocation = null;

            if (e.RowIndex != -1)
            {
                // Get the Id number of the row
                string softwareIndex = SoftwareDataGridView.Rows[e.RowIndex].Cells[0].Value.ToString();

                // Returning that Id as a int
                _selectedSoftareDataGridRowIndex = Convert.ToInt32(softwareIndex);

                try
                {
                    // Filling the datastruct
                    _softwareDataStruct.Name = SoftwareDataGridView.Rows[e.RowIndex].Cells["Name"].Value.ToString();
                    _softwareDataStruct.Developer = SoftwareDataGridView.Rows[e.RowIndex].Cells["Developer"].Value.ToString();
                    _softwareDataStruct.Publisher = SoftwareDataGridView.Rows[e.RowIndex].Cells["Publisher"].Value.ToString();
                    _softwareDataStruct.Version = SoftwareDataGridView.Rows[e.RowIndex].Cells["Version"].Value.ToString();
                    _softwareDataStruct.SubCategory = SoftwareDataGridView.Rows[e.RowIndex].Cells["SubCategory"].Value.ToString();
                    _softwareDataStruct.CrackType = SoftwareDataGridView.Rows[e.RowIndex].Cells["CrackType"].Value.ToString();
                    _softwareDataStruct.TorrentWebLink = SoftwareDataGridView.Rows[e.RowIndex].Cells["TorrentWebSiteLink"].Value.ToString();
                    _softwareDataStruct.Custom = SoftwareDataGridView.Rows[e.RowIndex].Cells["Custom"].Value.ToString();
                    _softwareDataStruct.Custom1 = SoftwareDataGridView.Rows[e.RowIndex].Cells["Custom1"].Value.ToString();
                    _softwareDataStruct.Size = Convert.ToSingle(SoftwareDataGridView.Rows[e.RowIndex].Cells["Size"].Value.ToString());
                    _softwareDataStruct.Location = SoftwareDataGridView.Rows[e.RowIndex].Cells["Location"].Value.ToString();
                    _softwareDataStruct.PictureLocation = SoftwareDataGridView.Rows[e.RowIndex].Cells["PictureLocation"].Value.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                try
                {
                    // Setting the lables to the correct values
                    SlbName.Text = _softwareDataStruct.Name;
                    SlbDeveloper.Text = _softwareDataStruct.Developer;
                    SlbVersion.Text = _softwareDataStruct.Version;
                    SlbPublisher.Text = _softwareDataStruct.Publisher;
                    SlbSubCategory.Text = _softwareDataStruct.SubCategory;
                    SlbSize.Text = MakingSizeEasyerToRead(_softwareDataStruct.Size);
                    SlbCustom.Text = _softwareDataStruct.Custom;
                    pictureLocation = _softwareDataStruct.PictureLocation;
                }
                catch (Exception ex)
                {
                    // If it could not load the labels
                    if (_fillLabelsErrorShowAgain == true)
                    {
                        if (MessageBox.Show(ex.Message + "\n \n Do you want to show this again", "error", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
                        {
                            _fillLabelsErrorShowAgain = false;
                        }
                    }
                }

                // Sending the picture location back
                _softwarePictureLocation = pictureLocation;

                // Loading picture - Checking if there is a picture
                if (!String.IsNullOrEmpty(pictureLocation) && pictureLocation != " ")
                {
                    try
                    {
                        // Loading the picture
                        using (FileStream stream = new FileStream(pictureLocation, FileMode.Open, FileAccess.Read))
                        {
                            SpbPicture.Image = Image.FromStream(stream);
                            stream.Dispose();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                else
                {
                    // Checking that the picture box is empty
                    if (SpbPicture.Image != null)
                    {
                        // Makeing sure if there is no picture that the picturebox is empty
                        SpbPicture.Image.Dispose();
                        SpbPicture.Image = null;
                    }
                }
            }
        }


        // Games - When a cell has been clicked - Retuns the Id of the selected row And Load the picture
        private void GamesDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string pictureLocation = null;


            if (e.RowIndex != -1)
            {
                // Getting the Id of that selected row
                string gamesIndex = GamesDataGridView.Rows[e.RowIndex].Cells[0].Value.ToString();

                // Retuning that id as a int
                _selectedGamesDataGridRowIndex = Convert.ToInt32(gamesIndex);

                try
                {
                    _gameDataStruct.Name = GamesDataGridView.Rows[e.RowIndex].Cells["Name"].Value.ToString();
                    _gameDataStruct.Developer = GamesDataGridView.Rows[e.RowIndex].Cells["Developer"].Value.ToString();
                    _gameDataStruct.Publisher = GamesDataGridView.Rows[e.RowIndex].Cells["Publisher"].Value.ToString();
                    _gameDataStruct.Version = GamesDataGridView.Rows[e.RowIndex].Cells["Version"].Value.ToString();
                    _gameDataStruct.Size = Convert.ToSingle(GamesDataGridView.Rows[e.RowIndex].Cells["Size"].Value);
                    _gameDataStruct.Genre = GamesDataGridView.Rows[e.RowIndex].Cells["Genre"].Value.ToString();
                    _gameDataStruct.CrackType = GamesDataGridView.Rows[e.RowIndex].Cells["CrackType"].Value.ToString();
                    _gameDataStruct.TorrentWebLink = GamesDataGridView.Rows[e.RowIndex].Cells["TorrentWebSiteLink"].Value.ToString();
                    _gameDataStruct.Custom = GamesDataGridView.Rows[e.RowIndex].Cells["Custom"].Value.ToString();
                    _gameDataStruct.Custom1 = GamesDataGridView.Rows[e.RowIndex].Cells["Custom1"].Value.ToString();
                    _gameDataStruct.Location = GamesDataGridView.Rows[e.RowIndex].Cells["Location"].Value.ToString();
                    _gameDataStruct.PictureLocation = GamesDataGridView.Rows[e.RowIndex].Cells["PictureLocation"].Value.ToString();

                }
                catch (Exception ex)
                {
                    if (_fillLabelsErrorShowAgain == true)
                    {
                        // If it was not able to load the labels text
                        if (MessageBox.Show(ex.Message + " Do you want to show this again", "error", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
                        {
                            _fillLabelsErrorShowAgain = false;
                        }
                    }
                }

                // Setting the lables to the correct values
                GlbName.Text = _gameDataStruct.Name;
                GlbDeveloper.Text = _gameDataStruct.Developer;
                GlbPublisher.Text = _gameDataStruct.Publisher;
                GlbVersion.Text = _gameDataStruct.Version;
                GlbGenre.Text = _gameDataStruct.Genre;
                GlbSize.Text = MakingSizeEasyerToRead(_gameDataStruct.Size);
                GlbTorrentWebsiteSearch.Text = _gameDataStruct.TorrentWebLink;
                GlbCustom.Text = _gameDataStruct.Custom;
                pictureLocation = _gameDataStruct.PictureLocation;

                // Sending back the picture location
                _gamesPictureLocation = pictureLocation;

                // Loading picture - Cheacking if there is a picture
                if (!String.IsNullOrEmpty(pictureLocation) && pictureLocation != " ")
                {
                    try
                    {
                        // Laoding the picture
                        using (FileStream stream = new FileStream(pictureLocation, FileMode.Open, FileAccess.Read))
                        {
                            GpbPicture.Image = Image.FromStream(stream);
                            stream.Dispose();
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }
                else
                {
                    // Checking that the picture box is empty
                    if (GpbPicture.Image != null)
                    {
                        // if there is no picture make sure that the picturebox is empty
                        GpbPicture.Image.Dispose();
                        GpbPicture.Image = null;
                    }

                }
            }

        }


        // Movies When a cell has been clicked - Retuns the Id of the selected row And Load the picture
        private void MoviesDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string pictureLocation = null;

            if (e.RowIndex != -1)
            {

                // Getting the id of that selected row
                string movieIndex = MoviesDataGridView.Rows[e.RowIndex].Cells[0].Value.ToString();

                // Retuing that id as a int
                _selectedMoviesDataGridRowIndex = Convert.ToInt32(movieIndex);

                try
                {
                    _movieDataStruct.Name = MoviesDataGridView.Rows[e.RowIndex].Cells["Name"].Value.ToString();
                    _movieDataStruct.Studio = MoviesDataGridView.Rows[e.RowIndex].Cells["Studio"].Value.ToString();
                    _movieDataStruct.Lenght = Convert.ToInt32(MoviesDataGridView.Rows[e.RowIndex].Cells["Lenght"].Value);
                    _movieDataStruct.Subtitles = MoviesDataGridView.Rows[e.RowIndex].Cells["subtitles"].Value.ToString();
                    _movieDataStruct.Subtitles1 = MoviesDataGridView.Rows[e.RowIndex].Cells["subtitles1"].Value.ToString();
                    _movieDataStruct.Size = Convert.ToSingle(MoviesDataGridView.Rows[e.RowIndex].Cells["Size"].Value);
                    _movieDataStruct.Genre = MoviesDataGridView.Rows[e.RowIndex].Cells["Genre"].Value.ToString();
                    _movieDataStruct.Genre1 = MoviesDataGridView.Rows[e.RowIndex].Cells["Genre1"].Value.ToString();
                    _movieDataStruct.Genre2 = MoviesDataGridView.Rows[e.RowIndex].Cells["Genre2"].Value.ToString();
                    _movieDataStruct.Language = MoviesDataGridView.Rows[e.RowIndex].Cells["Language"].Value.ToString();
                    _movieDataStruct.Language1 = MoviesDataGridView.Rows[e.RowIndex].Cells["Language1"].Value.ToString();
                    _movieDataStruct.Custom = MoviesDataGridView.Rows[e.RowIndex].Cells["Custom"].Value.ToString();
                    _movieDataStruct.Custom1 = MoviesDataGridView.Rows[e.RowIndex].Cells["Custom1"].Value.ToString();
                    _movieDataStruct.Location = MoviesDataGridView.Rows[e.RowIndex].Cells["Location"].Value.ToString();
                    _movieDataStruct.PictureLocation = MoviesDataGridView.Rows[e.RowIndex].Cells["PictureLocation"].Value.ToString();
                    //_movieDataStruct.ReleaseDate = MoviesDataGridView.Rows[e.RowIndex].Cells[n].Value.ToString();

                }
                catch (Exception ex)
                {
                    if (_fillLabelsErrorShowAgain == true)
                    {
                        if (MessageBox.Show(ex.Message + " Do you want to show this again", "error", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
                        {
                            _fillLabelsErrorShowAgain = false;
                        }
                    }
                }

                // Setting the labels tothe corret Values
                MlbName.Text = _movieDataStruct.Name;
                MlbStudio.Text = _movieDataStruct.Studio;
                MlbLenght.Text = _movieDataStruct.Lenght.ToString();
                MlbGenre.Text = _movieDataStruct.Genre;
                MlbLanguage.Text = _movieDataStruct.Language;
                MlbSubtitles.Text = _movieDataStruct.Subtitles;
                MlbCustom.Text = _movieDataStruct.Custom;
                MlbSize.Text = MakingSizeEasyerToRead(_movieDataStruct.Size);
                pictureLocation = _movieDataStruct.PictureLocation;

                // sending back the location of the pictutre
                _moviesPictureLocation = pictureLocation;

                // Loading the picture - checking if there is a picture
                if (!String.IsNullOrEmpty(pictureLocation) && pictureLocation != " ")
                {
                    try
                    {
                        // Loading the picture
                        using (FileStream stream = new FileStream(pictureLocation, FileMode.Open, FileAccess.Read))
                        {
                            MpbPicture.Image = Image.FromStream(stream);
                            stream.Dispose();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    // Checking that the picture box is empty
                    if (MpbPicture.Image != null)
                    {
                        // Makeing sure that the picture box is emtpy if there is nothing there
                        MpbPicture.Image.Dispose();
                        MpbPicture.Image = null;
                    }
                }
            }
        }


        // Series - When a cell has been clicked - Retuns the Id of the selected row And Load the picture
        private void SeriesDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string pictureLocation = null;

            if (e.RowIndex != -1)
            {
                // Getting the id of that selected row
                string seriesIndex = SeriesDataGridView.Rows[e.RowIndex].Cells[0].Value.ToString();

                // Returing that id as a int
                _selectedSeriesDataGridRowIndex = Convert.ToInt32(seriesIndex);

                try
                {
                    _serieDataStruct.Name = SeriesDataGridView.Rows[e.RowIndex].Cells["Name"].Value.ToString();
                    _serieDataStruct.Studio = SeriesDataGridView.Rows[e.RowIndex].Cells["Studio"].Value.ToString();
                    _serieDataStruct.Episode = SeriesDataGridView.Rows[e.RowIndex].Cells["Episode"].Value.ToString();
                    _serieDataStruct.Season = SeriesDataGridView.Rows[e.RowIndex].Cells["Season"].Value.ToString();
                    _serieDataStruct.Size = Convert.ToInt32(SeriesDataGridView.Rows[e.RowIndex].Cells["Size"].Value);
                    _serieDataStruct.Quality = SeriesDataGridView.Rows[e.RowIndex].Cells["Quality"].Value.ToString();
                    _serieDataStruct.Lenght = SeriesDataGridView.Rows[e.RowIndex].Cells["Lenght"].Value.ToString();
                    _serieDataStruct.Genre = SeriesDataGridView.Rows[e.RowIndex].Cells["Genre"].Value.ToString();
                    _serieDataStruct.Genre1 = SeriesDataGridView.Rows[e.RowIndex].Cells["Genre1"].Value.ToString();
                    _serieDataStruct.Language = SeriesDataGridView.Rows[e.RowIndex].Cells["Language"].Value.ToString();
                    _serieDataStruct.CheckNewLink = SeriesDataGridView.Rows[e.RowIndex].Cells["CheakNewlink"].Value.ToString();
                    _serieDataStruct.NextSeasonData = SeriesDataGridView.Rows[e.RowIndex].Cells["Nextseasondate"].Value.ToString();
                    _serieDataStruct.Custom = SeriesDataGridView.Rows[e.RowIndex].Cells["Custom"].Value.ToString();
                    _serieDataStruct.Custom1 = SeriesDataGridView.Rows[e.RowIndex].Cells["Custom1"].Value.ToString();
                    _serieDataStruct.Location = SeriesDataGridView.Rows[e.RowIndex].Cells["Location"].Value.ToString();
                    _serieDataStruct.PictureLocation = SeriesDataGridView.Rows[e.RowIndex].Cells["PictureLocation"].Value.ToString();



                }
                catch (Exception ex)
                {
                    if (_fillLabelsErrorShowAgain == true)
                    {
                        if (MessageBox.Show(ex.Message + " Do you want to show this again", "error", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
                        {
                            _fillLabelsErrorShowAgain = false;
                        }
                    }
                }

                // Setting the lables to the correct values
                SElbName.Text = _serieDataStruct.Name;
                SElbStudio.Text = _serieDataStruct.Studio;
                SElbEpisode.Text = _serieDataStruct.Episode;
                SElbSeason.Text = _serieDataStruct.Season;
                SElbQuality.Text = _serieDataStruct.Quality;
                SElbLanguage.Text = _serieDataStruct.Language;
                SElbNextSeasonDate.Text = _serieDataStruct.NextSeasonData;
                SElbCustom.Text = _serieDataStruct.Custom;
                SElbSeason.Text = _serieDataStruct.Custom1;
                pictureLocation = _serieDataStruct.Location;

                // Setting the picture location
                _seriesPictureLocation = pictureLocation;

                // Loading picture - Cheacking if there is a picture
                if (!String.IsNullOrEmpty(pictureLocation) && pictureLocation != " ")
                {
                    try
                    {
                        // Loading the picture
                        using (FileStream stream = new FileStream(pictureLocation, FileMode.Open, FileAccess.Read))
                        {
                            SEpbPicture.Image = Image.FromStream(stream);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    //Checking that the picture box is empty
                    if (SElbEpisode.Image != null)
                    {
                        //makeing sure the picture is empty when there is not picture
                        SEpbPicture.Image.Dispose();
                        SEpbPicture.Image = null;
                    }
                }

            }
        }



        // Software - When the Text of the Search Textbox has Changed - Only show the value's that are Containded in the Cell's Hide The Rest
        private void StbSearch_TextChanged(object sender, EventArgs e)
        {
            SearchDataGridView(SoftwareDataGridView, StbSearch);
        }


        // Games - When the Text of the Search Textbox has Changed - Only show the value's that are Containded in the Cell's Hide The Rest
        private void GtbSearch_TextChanged(object sender, EventArgs e)
        {
            SearchDataGridView(GamesDataGridView, GtbSearch);
        }


        // Movies - When the Text of the Search Textbox has Changed - Only show the value's that are Containded in the Cell's Hide The Rest
        private void MtbSearch_TextChanged(object sender, EventArgs e)
        {
            SearchDataGridView(MoviesDataGridView, MtbSearch);
        }


        // Series - When the Text of the Search Textbox has Changed - Only show the value's that are Containded in the Cell's Hide The Rest
        private void SEtbSearch_TextChanged(object sender, EventArgs e)
        {
            SearchDataGridView(SeriesDataGridView, SEtbSearch);
        }



        // Software - When Combobox text has changed - Show the new table that has been selected
        private void Mcb1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Sets the varible when table text has changed
            _selectedSoftwareTable = ScbSelectTable.Text;

            // Reloads the Data Grid View to the new Table
            LoadTableDataGridView();
        }


        // Games - When Combobox text has changed - Show the new table that has been selected
        private void GcbSelectedTable_SelectedIndexChanged(object sender, EventArgs e)
        {


            // Sets the varible when table text has changed
            _selectedGamesTable = GcbSelectedTable.Text;

            // Reloads the Data Grid View to the new Table
            LoadTableDataGridView();
        }


        // Movies - When Combobox text has changed - Show the new table that has been selected
        private void McbSelectedTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Sets the varible when table text has changed
            _selectedMoviesTable = McbSelectedTable.Text;

            // Reloads the Data Grid View to the new Table
            LoadTableDataGridView();
        }


        // Series - When Combobox text has changed - Show the new table that has been selected
        private void SEcbSelectedTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Sets the varible when table text has changed
            _selectedSeriesTable = SEcbSelectedTable.Text;

            // Reloads the Data Grid View to the new Table
            LoadTableDataGridView();
        }



        // Software - Cell Double Click - Opens the folder of the item
        private void SoftwareDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            // Checking if it is not the sort funcotion
            if (e.RowIndex != -1)
            {
                string folderLocation;

                try
                {
                    // Getting the folder location
                    folderLocation = SoftwareDataGridView.Rows[e.RowIndex].Cells[11].Value.ToString();

                    // Cecking if there is something in the sting and if the folder exists
                    if (!String.IsNullOrEmpty(folderLocation) && Directory.Exists(folderLocation))
                    {
                        // Open the folder
                        System.Diagnostics.Process.Start(SoftwareDataGridView.Rows[e.RowIndex].Cells[11].Value.ToString());
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }


        // Games - Cell Double Click - Opens the folder of the item
        private void GamesDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                string folderLocation;

                try
                {
                    // Getting the folder location
                    folderLocation = GamesDataGridView.Rows[e.RowIndex].Cells[11].Value.ToString();

                    // Checking if the folder exists
                    if (!String.IsNullOrEmpty(folderLocation) && Directory.Exists(folderLocation))
                    {
                        // Opening the folder
                        System.Diagnostics.Process.Start(GamesDataGridView.Rows[e.RowIndex].Cells[11].Value.ToString());
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }


        // Movies - Cell Double Click - Opens the folder of the item
        private void MoviesDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                string folderLocation;

                try
                {
                    // Getting the folder form the source
                    folderLocation = MoviesDataGridView.Rows[e.RowIndex].Cells[14].Value.ToString();

                    // Checkign if there is a folder
                    if (!String.IsNullOrEmpty(folderLocation) && Directory.Exists(folderLocation))
                    {
                        // Opening the folder
                        System.Diagnostics.Process.Start(MoviesDataGridView.Rows[e.RowIndex].Cells[14].Value.ToString());
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }


        // Series - Cell Double Click - Opens the folder of the item
        private void SeriesDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                string folderLocation;

                try
                {
                    // Getting the folder
                    folderLocation = SeriesDataGridView.Rows[e.RowIndex].Cells[15].Value.ToString();

                    // Cecking if the folder is there
                    if (!String.IsNullOrEmpty(folderLocation) && Directory.Exists(folderLocation))
                    {
                        // Opening the folder
                        System.Diagnostics.Process.Start(SeriesDataGridView.Rows[e.RowIndex].Cells[15].Value.ToString());
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }


        // Adds from the selected row
        private void addFromSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Add add;

            // Checking witch constuctor it sould use
            if (_selectedTabIndex == 0) // Software
            {
                add = new Add(_selectedTabIndex, _selectedSoftwareTable, _softwareDataStruct);
            }
            else if (_selectedTabIndex == 1) // Games
            {
                add = new Add(_selectedTabIndex, _selectedGamesTable, _gameDataStruct);
            }
            else if (_selectedTabIndex == 2) // Movies
            {
                add = new Add(_selectedTabIndex, _selectedMoviesTable, _movieDataStruct);
            }
            else if (_selectedTabIndex == 3) // Series
            {
                add = new Add(_selectedTabIndex, _selectedSeriesTable, _serieDataStruct);
            }
            else
            {
                throw new InvalidOperationException();
            }
        
            // Loading and showing the dialog
            add.ShowDialog();

            // Reseting the tables
            LoadTableDataGridView();
        }


        #endregion

        private void combineToSeasonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Opens the combine to season form
            CombineToSeason combineToSeason = new CombineToSeason(_selectedSeriesTable);

            // Shows the dialog
            if(combineToSeason.ShowDialog() == DialogResult.OK)
            {
                // IF the dialog result was okay, Reset the Datagridview
                LoadTableDataGridView();
            }
        }

    }
}

