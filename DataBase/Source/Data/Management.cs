﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataBase.Source.Data
{
    class Management
    {

        public Management()
        {

        }

        /// <summary>
        /// Converts KB - GB - TB - To MB
        /// </summary>
        /// <param name="metric">The metric used in UpperCase</param>
        /// <param name="size">The Size of that metric</param>
        /// <returns>a MB size value</returns>
        public float ConvertToMB(string metric, string size)
        {
            float finalSize = 0.0f;
            
            // Makeing sure that there are not letters in the string
            foreach (char ch in size.ToArray())
            {
                // Checking if it is digit
                if (!char.IsDigit(ch) && ch != '.' && ch != ',')
                {
                    // Sending a error becouse that is not a digit
                    throw new FormatException("There cant be a letter in a float value");
                }
            }

            // replacing the dots with comma's
            size = size.Replace('.', ',');   

            // Checking witch Metric has been used and recalculating to MB
            switch (metric)
            {
                case "KB":
                    finalSize = Convert.ToSingle(size) / 1024.0f;
                    break;
                case "MB":
                    finalSize = Convert.ToSingle(size);
                    break;
                case "GB":
                    finalSize = Convert.ToSingle(size) * 1024.0f;
                    break;
                case "TB":
                    finalSize = Convert.ToSingle(size) * 1024.0f * 1024.0f;
                    break;
            }

            finalSize = (float)Math.Round(finalSize, 2);


            return finalSize;
        }

    }




    // TODO : Change everything to structs

    public struct SoftwareData
    {
        public int RowId;
        public string TableName;
        public string Name;
        public string Developer;
        public string Publisher;
        public string Version;
        public float Size;
        public string SubCategory;
        public string CrackType;
        public string TorrentWebLink;
        public String Custom;
        public string Custom1;
        public string Location;
        public string PictureLocation;           
    }

    public struct GamesData
    {
        public int RowId;
        public string TableName;
        public string Name;
        public string Developer;
        public string Publisher;
        public string Version;
        public float Size;
        public string Genre;
        public string CrackType;
        public string TorrentWebLink;
        public String Custom;
        public string Custom1;
        public string Location;
        public string PictureLocation;
    }

    public struct MoviesData
    {
        public int RowId;
        public string TableName;
        public string Name;
        public string Studio;
        public string Lenght;
        public string Subtitles;
        public float Size;
        public string Subtitles1;
        public string Genre;
        public string Gnere1;
        public string Genre2;
        public string Language;
        public string Language1;
        public string Custom;
        public string Custom1;
        public string Location;
        public string PictureLocation;
    }

    public struct SeriesData
    {
        public int RowId;
        public string TableName;
        public string Name;
        public string Studio;
        public string Episode;
        public string Season;
        public float Size;
        public string Quality;
        public string Lenght;
        public string Gnere;
        public string Genre1;
        public string Language;
        public string CheckNewLink;
        public string Custom;
        public string Custom1;
        public string Location;
        public string PictureLocation;
    }
}
