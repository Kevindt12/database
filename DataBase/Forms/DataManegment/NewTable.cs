﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataBase.Forms.DataManegment
{
    public partial class NewTable : Form
    {
        private string _TableName;

        // Geting setting the propotie of table name
        public string TableName
        {
            get
            {
                return _TableName;
            }
            set
            {
                _TableName = value;
            }
        }

        // Constuctor
        public NewTable()
        {
            InitializeComponent();
        }



        // When the button ok is clicked
        private void btnOk_Click(object sender, EventArgs e)
        {
            // Checking if there was a entry and rety
            if (tbMain.Text == "" || tbMain.Text == null)
            {
                MessageBox.Show("There was no entry", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            // If there are white spaces is okay but put under scorese there
            else if (!String.IsNullOrWhiteSpace(tbMain.Text))
            {
                _TableName = tbMain.Text.Replace(' ', '_');
            }
            // if there was a entry then contenue
            else
            {
                // Setting the varible to the textbox
                _TableName = tbMain.Text;
            }
        }

        // When the form loades
        private void NewTable_Load(object sender, EventArgs e)
        {
            // Setting the dialog Result Settings
            btnCancel.DialogResult = DialogResult.Cancel;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
        }

        // When the text has changed
        private void tbMain_TextChanged(object sender, EventArgs e)
        {
            // if there is no text in the textbox then make sure that dialog resut is not okay
            if (tbMain.Text == "" || tbMain.Text == null)
            {
                btnOk.DialogResult = DialogResult.None;
            }
            // else make sure it is ok
            else
            {
                btnOk.DialogResult = DialogResult.OK;
            }
        }
    }
}
