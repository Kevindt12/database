﻿using System;
using System.Windows.Forms;

using DataBase.Source;
using DataBase.Source.Data;

namespace DataBase.Forms.DataManegment
{
    public partial class Edit : Add
    {
        private bool _softwareTable = false;
        private bool _gamesTable = false;
        private bool _moviesTable = false;
        private bool _seriesTable = false;
        private int _selectedTabIndex;
        private string _tableName;
        private int _rowId;

        private SoftwareData _softwareData;
        private MovieData _movieData;
        private SerieData _serieData;
        private GameData _gameData;


        public Edit(int SelectedTabIndex, string TableName, int RowId, SoftwareData softwareData) : base(SelectedTabIndex, TableName)
        {
            InitializeComponent();

            // LOG
            Console.WriteLine("Editing a Row");

            _selectedTabIndex = SelectedTabIndex;
            _tableName = TableName;
            _rowId = RowId;

            // Setting the datastruct and filling the textboxes
            _softwareData = softwareData;
        }

        public Edit(int SelectedTabIndex, string TableName, int RowId, MovieData movieData) : base(SelectedTabIndex, TableName)
        {
            InitializeComponent();

            _selectedTabIndex = SelectedTabIndex;
            _tableName = TableName;
            _rowId = RowId;

            // Setting the datastruct and filling the textboxes
            _movieData = movieData;

        }

        public Edit(int SelectedTabIndex, string TableName, int RowId, SerieData serieData) : base(SelectedTabIndex, TableName)
        {
            InitializeComponent();

            _selectedTabIndex = SelectedTabIndex;
            _tableName = TableName;
            _rowId = RowId;

            // Setting the datastruct and filling the textboxes
            _serieData = serieData;

        }

        public Edit(int SelectedTabIndex, string TableName, int RowId, GameData gameData) : base(SelectedTabIndex, TableName)
        {
            InitializeComponent();

            _selectedTabIndex = SelectedTabIndex;
            _tableName = TableName;
            _rowId = RowId;

            // Setting the datastruct and filling the textboxes
            _gameData = gameData;

        }


        // When Form loads
        private void Edit_Load(object sender, EventArgs e)
        {
            // Selecting and opening the appropiate tabcontrol Tab.
            tabControl1.SelectedIndex = _selectedTabIndex;

            // Checking with tab was selected and setting the labels to the correct values
            switch (_selectedTabIndex)
            {
                case 0:
                    label56.Text = _tableName;
                    label58.Text = _rowId.ToString();

                    // Disableing the other done buttons
                    GbtnDone.Enabled = false;
                    MbtnDone.Enabled = false;
                    SEbtnDone.Enabled = false;
                    _softwareTable = true;
                    break;
                case 1:
                    label14.Text = _tableName;
                    label12.Text = _rowId.ToString();

                    // Disableing the other done buttons
                    SbtnDone.Enabled = false;
                    MbtnDone.Enabled = false;
                    SEbtnDone.Enabled = false;
                    _gamesTable = true;
                    break;
                case 2:
                    label60.Text = _tableName;
                    label25.Text = _rowId.ToString();

                    // Disableing the other done buttons
                    SbtnDone.Enabled = false;
                    GbtnDone.Enabled = false;
                    SEbtnDone.Enabled = false;
                    _moviesTable = true;
                    break;
                case 3:
                    label62.Text = _tableName;
                    label37.Text = _rowId.ToString();

                    // Disableing the other done buttons
                    SbtnDone.Enabled = false;
                    GbtnDone.Enabled = false;
                    MbtnDone.Enabled = false;
                    _seriesTable = true;
                    break;
            }


            // Setting the textboxes to there row value so that there is no null entry
            // Software
            if (_softwareTable == true)
            {
                base.FillTextBoxesWithDataFromStruct(_softwareData);
            }

            // Games
            else if (_gamesTable == true)
            {
                base.FillTextBoxesWithDataFromStruct(_gameData);
            }

            // Movies
            else if (_moviesTable == true)
            {
                base.FillTextBoxesWithDataFromStruct(_movieData);
            }

            // Series
            else if (_seriesTable == true)
            {
                base.FillTextBoxesWithDataFromStruct(_serieData);
            }

            // Setting the size metric for all the tabs
            ScbSizeMetric.Text = "MB";
            GcbSizeMetric.Text = "MB";
            McbSizeMetric.Text = "MB";

            // Fixing the size To make this a fixed size form
            this.MinimumSize = this.Size;
            this.MaximumSize = this.Size;
            this.MaximizeBox = false;

            // Setting the cancel buttons
            SbtnCancel.DialogResult = DialogResult.Cancel;
            GbtnCancel.DialogResult = DialogResult.Cancel;
            MbtnCancel.DialogResult = DialogResult.Cancel;
            SEbtnCancel.DialogResult = DialogResult.Cancel;
        }



        // When Text Has Canged in the name Textbox If the name textbox is null then make sure you cant save

        // Software
        private void StbName_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(StbName.Text))
            {
                SbtnDone.DialogResult = DialogResult.OK;
            }
            else
            {
                SbtnDone.DialogResult = DialogResult.None;
            }
        }

        // Games
        private void GtbName_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(GtbName.Text))
            {
                GbtnDone.DialogResult = DialogResult.OK;
            }
            else
            {
                GbtnDone.DialogResult = DialogResult.None;
            }
        }

        // Movies
        private void MtbName_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(MtbName.Text))
            {
                MbtnDone.DialogResult = DialogResult.OK;
            }
            else
            {
                MbtnDone.DialogResult = DialogResult.None;
            }
        }

        // Series
        private void SEtbName_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(SEtbName.Text))
            {
                SEbtnDone.DialogResult = DialogResult.OK;
            }
            else
            {
                SEbtnDone.DialogResult = DialogResult.None;
            }
        }



        // Software - Done Clicked
        private void SbtnDone_Click(object sender, EventArgs e)
        {
            float size = 0;

            SqlGeneral.Software SQLSoftware = new SqlGeneral.Software();

            // Checking if there is a size in the textbox
            if (!String.IsNullOrEmpty(StbSize.Text))
            {
                // If there is then change the size to mb
                size = base.ConvertToMB(ScbSizeMetric.Text, StbSize.Text);
            }

            // Adding all the data to a struct
            _softwareData.Name = StbName.Text;
            _softwareData.Developer = StbDeveloper.Text;
            _softwareData.Publisher = StbPublisher.Text;
            _softwareData.Version = StbVersion.Text;
            _softwareData.SubCategory = StbSubCategory.Text;
            _softwareData.CrackType = ScbCrackType.Text;
            _softwareData.TorrentWebLink = StbTorrentLinkSearch.Text;
            _softwareData.Custom = StbCustom.Text;
            _softwareData.Custom1 = StbCustom1.Text;
            _softwareData.Location = StbLocation.Text;
            _softwareData.Size = size;

            // Update the database table
            SQLSoftware.UpdateRowInTable(_tableName, _rowId, _softwareData);
        }

        // Games - Done Clicked
        private void GbtnDone_Click(object sender, EventArgs e)
        {
            float size = 0;

            SqlGeneral.Games SQLGames = new SqlGeneral.Games();

            // Checking if there is a size in the textbox
            if (!String.IsNullOrEmpty(GtbSize.Text))
            {
                // If there is then change the size to mb
                size = base.ConvertToMB(GcbSizeMetric.Text, GtbSize.Text);
            }

            // Filling the datastuct
            _gameData.Name = GtbName.Text;
            _gameData.Developer = GtbDeveloper.Text;
            _gameData.Publisher = GtbPublisher.Text;
            _gameData.Version = GtbVersion.Text;
            _gameData.Genre = GtbGenre.Text;
            _gameData.CrackType = GcbCrackType.Text;
            _gameData.TorrentWebLink = GtbTorrentWebLink.Text;
            _gameData.Custom = GtbCustom.Text;
            _gameData.Custom1 = GtbCustom1.Text;
            _gameData.Size = size;

            // Update the database table
            SQLGames.UpdateRowInTable(_tableName, _rowId, _gameData);
        }

        // Movies - Done Clicked
        private void MbtnDone_Click(object sender, EventArgs e)
        {
            float size = 0;

            SqlGeneral.Movies SQLMovies = new SqlGeneral.Movies();

            // Checking if there is a size in the textbox
            if (!String.IsNullOrEmpty(MtbSize.Text))
            {
                // If there is then change the size to mb
                size = base.ConvertToMB(McbSizeMetric.Text, MtbSize.Text);
            }

            // Filling the datastuct
            _movieData.Name = MtbName.Text;
            _movieData.Studio = MtbStudio.Text;
            _movieData.Lenght = Convert.ToInt32(MtbLenght.Text);
            _movieData.Subtitles = MtbSubtitles.Text;
            _movieData.Size = size;
            _movieData.Subtitles1 = MtbSubtitles1.Text;
            _movieData.Genre = MtbGenre.Text;
            _movieData.Genre1 = MtbGenre1.Text;
            _movieData.Genre2 = MtbGenre2.Text;
            _movieData.Language = MtbLanuage.Text;
            _movieData.Language1 = MtbLanguage1.Text;
            _movieData.Custom = MtbCustom.Text;
            _movieData.Custom1 = MtbCustom1.Text;
            _movieData.Location = MtbLocation.Text;

            // Update the database table
            SQLMovies.UpdateRowInTable(_tableName, _rowId, _movieData);
        }

        // Series - Done Clicked
        private void SEbtnDone_Click(object sender, EventArgs e)
        {          
            SqlGeneral.Series SQLSeries = new SqlGeneral.Series();


            // Filling the datastruct
            _serieData.Name = SEtbName.Text;
            _serieData.Studio = SEtbSutdio.Text;
            _serieData.Lenght = SEtbLenght.Text;
            _serieData.Size = base.ConvertToMB(SEcbSizeMetric.Text, SEtbSize.Text);
            _serieData.Genre = SEtbGenre.Text;
            _serieData.Genre1 = SetbGenre1.Text;
            _serieData.Language = SEtbLanguage.Text;
            _serieData.CheckNewLink = SEtbCheckNewLink.Text;
            _serieData.NextSeasonData = SEdtpNextSeasonDate.Value.ToString(); ;
            _serieData.Quality = SEcbQuality.Text;
            _serieData.Season = SEtbSeason.Text;
            _serieData.Episode = SEtbEpisode.Text;


            SQLSeries.UpdateRowInTable(_tableName, _rowId, _serieData);
        }



        // Software - Getting the folderlocation
        private void SbtnFolderLocation_Click(object sender, EventArgs e)
        {
            StbLocation.Text = base.GetFolerLocation();
        }

        // Games - Getting the folderlocation
        private void GbtnFolderLocation_Click(object sender, EventArgs e)
        {
            GtbLocation.Text = base.GetFolerLocation();
        }

        // Movies - Getting the folderlocation
        private void MbtnFolderLocation_Click(object sender, EventArgs e)
        {
            MtbLocation.Text = base.GetFolerLocation();
        }

    }
}
