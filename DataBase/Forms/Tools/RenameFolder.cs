﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Microsoft.VisualBasic.FileIO;
using System.IO;

using DataBase.Source;
using DataBase.Source.Data;

namespace DataBase.Forms.Tools
{
    public partial class RenameFolder : Form
    {
        private string _oldFolderName;
        private string _newFolderLocationName;

        private SoftwareData _softwareData;
        private GameData _gameData;
        private MovieData _movieData;

        private ComboBox[] _comboboxes = new ComboBox[6];

        public string NewFolderLocationName
        {
            get
            {
                return _newFolderLocationName;
            }
            set
            {
                _newFolderLocationName = value;
            }
        }

        // The orinial folder location if required
        public string OriginFolderLocation
        {
            get
            {
                return _oldFolderName;
            }
            set
            {
                _oldFolderName = value;
            }
        }




        /// <summary>
        /// Renames a folder to a other name via a dailog
        /// </summary>
        /// <param name="softwareData">Software data</param>
        public RenameFolder(SoftwareData softwareData)
        {
            InitializeComponent();

            // Setting everyting so only the software will work
            FillComboBoxesWithSoftwareData();

            // Setting the varibles
            _softwareData = softwareData;
            _oldFolderName = softwareData.Location;

            // Setting the radio button
            rbSoftware.Checked = true;

            // Disableing the checkboxes
            rbGames.Enabled = false;
            rbMovies.Enabled = false;
            rbSoftware.Enabled = false;
        }

        /// <summary>
        /// Renames a folder to a other name via a dailog
        /// </summary>
        /// <param name="gameData">Game Data</param>
        public RenameFolder(GameData gameData)
        {
            InitializeComponent();

            // Setting everyting so only the software will work
            FillComboBoxesWithGamesData();

            // Setting the varibles
            _gameData = gameData;
            _oldFolderName = gameData.Location;

            // Setting the radio button
            rbGames.Checked = true;

            // Disableing the checkboxes
            rbGames.Enabled = false;
            rbMovies.Enabled = false;
            rbSoftware.Enabled = false;
        }

        /// <summary>
        /// Renames a folder to a other name via a dailog
        /// </summary>
        /// <param name="movieData">Movie Data</param>
        public RenameFolder(MovieData movieData)
        {
            InitializeComponent();

            // Setting everyting so only the software will work
            FillComboBoxesWithMovieData();

            // Setting the varibles
            _movieData = movieData;
            _oldFolderName = movieData.Location;

            // Setting the radio button
            rbMovies.Checked = true;

            // Disableing the checkboxes
            rbGames.Enabled = false;
            rbMovies.Enabled = false;
            rbSoftware.Enabled = false;
        }

        private void RenameFolder_Load(object sender, EventArgs e)
        {
            // Fill the combobox array
            ComboBox[] _comboboxes = { comboBox1, comboBox2, comboBox3, comboBox4, comboBox5, comboBox6 };
        }


        // Methods

        /// <summary>
        /// Fills the comboboxs wit the software options
        /// </summary>
        private void FillComboBoxesWithSoftwareData()
        {
            foreach (ComboBox comboBox in this.Controls.OfType<ComboBox>())
            {
                comboBox.Items.Add("Nothing");
                comboBox.Items.Add("Name");
                comboBox.Items.Add("Developer");
                comboBox.Items.Add("Publisher");
                comboBox.Items.Add("Version");
                comboBox.Items.Add("Size");
                comboBox.Items.Add("SubCatergory");
                comboBox.Items.Add("CrackType");
                comboBox.Items.Add("Custom");
                comboBox.Items.Add("Custom1");


                // Set to nothing 
                comboBox.Text = comboBox.Items[0].ToString();
            }
        }

        /// <summary>
        /// Fills the comboboxs wit the Games options
        /// </summary>
        private void FillComboBoxesWithGamesData()
        {
            foreach (ComboBox comboBox in this.Controls.OfType<ComboBox>())
            {
                comboBox.Items.Add("Nothing");
                comboBox.Items.Add("Name");
                comboBox.Items.Add("Developer");
                comboBox.Items.Add("Publisher");
                comboBox.Items.Add("Version");
                comboBox.Items.Add("Size");
                comboBox.Items.Add("Genre");
                comboBox.Items.Add("CrackType");
                comboBox.Items.Add("Custom");
                comboBox.Items.Add("Custom1");

                comboBox.Text = comboBox.Items[0].ToString();
            }
        }

        /// <summary>
        /// Fills the comboboxs wit the Movies options
        /// </summary>
        private void FillComboBoxesWithMovieData()
        {
            foreach (ComboBox comboBox in this.Controls.OfType<ComboBox>())
            {
                comboBox.Items.Add("Nothing");
                comboBox.Items.Add("Name");
                comboBox.Items.Add("Studio");
                comboBox.Items.Add("Lenght");
                comboBox.Items.Add("SubTitles");
                comboBox.Items.Add("Size");
                comboBox.Items.Add("Genre");
                comboBox.Items.Add("Custom");
                comboBox.Items.Add("Custom1");

                comboBox.Text = comboBox.Items[0].ToString();
            }
        }




        /// <summary>
        /// This selectes the data from the stuct by Combobox
        /// </summary>
        /// <param name="comboboxData">What was in the combobox The text</param>
        /// <param name="softwareData">The struct</param>
        /// <returns>What was in the struct by that combobox text</returns>
        private string CheckingWhatIsSelectedSoftware(string comboboxData, SoftwareData softwareData)
        {
            switch (comboboxData)
            {
                case "Nothing":
                    return "";
                case "Name":
                    return softwareData.Name;
                case "Developer":
                    return softwareData.Developer;
                case "Publisher":
                    return softwareData.Publisher;
                case "Version":
                    return softwareData.Version;
                case "Size":
                    return softwareData.Size.ToString();
                case "SubCatergory":
                    return softwareData.SubCategory;
                case "CrackType":
                    return softwareData.CrackType;
                case "Custom":
                    return softwareData.Custom;
                case "Custom1":
                    return softwareData.Custom1;
                default:
                    throw new ArgumentOutOfRangeException("ComboBox", "There was nothhing in the combobox there has to be something");
            }
        }

        /// <summary>
        /// This selectes the data from the stuct by Combobox
        /// </summary>
        /// <param name="comboboxData">What was in the combobox The text</param>
        /// <param name="softwareData">The struct</param>
        /// <returns>What was in the struct by that combobox text</returns>
        private string CheckingWhatIsSelectedGame(string comboboxData, GameData gameData)
        {
            switch (comboboxData)
            {
                case "Nothing":
                    return "";
                case "Name":
                    return gameData.Name;
                case "Developer":
                    return gameData.Developer;
                case "Publisher":
                    return gameData.Publisher;
                case "Version":
                    return gameData.Version;
                case "Size":
                    return gameData.Size.ToString();
                case "Genre":
                    return gameData.Genre;
                case "CrackType":
                    return gameData.CrackType;
                case "Custom":
                    return gameData.Custom;
                case "Custom1":
                    return gameData.Custom1;
                default:
                    throw new ArgumentOutOfRangeException("ComboBox", "There was nothhing in the combobox there has to be something");
            }
        }

        /// <summary>
        /// This selectes the data from the stuct by Combobox
        /// </summary>
        /// <param name="comboboxData">What was in the combobox The text</param>
        /// <param name="softwareData">The struct</param>
        /// <returns>What was in the struct by that combobox text</returns>
        private string CheckingWhatIsSelectedMovie(string comboboxData, MovieData movieData)
        {
            switch (comboboxData)
            {
                case "Nothing":
                    return "";
                case "Name":
                    return movieData.Name;
                case "Studio":
                    return movieData.Studio;
                case "Lenght":
                    return movieData.Lenght.ToString();
                case "SubTitles":
                    return movieData.Subtitles;
                case "Size":
                    return movieData.Size.ToString();
                case "Genre":
                    return movieData.Genre;
                case "Custom":
                    return movieData.Custom;
                case "Custom1":
                    return movieData.Custom1;
                default:
                    throw new ArgumentOutOfRangeException("ComboBox", "There was nothhing in the combobox there has to be something");
            }
        }



        /// <summary>
        /// Builds the string whith the content of the comboboxes
        /// </summary>
        /// <param name="list">The list going to the builder</param>
        /// <returns></returns>
        private string NewNameOfFolder()
        {
            string finalString = "";

            // Reload the array
            ComboBox[] _combobox = { comboBox1, comboBox2, comboBox3, comboBox4, comboBox5, comboBox6 };

            // Selecting each combobox from the from
            foreach (ComboBox combobox in _combobox)
            {
                if (rbSoftware.Checked == true)
                {
                    // Checking if there is already something in the string If ther isn't than just add the string dont need the add on itself and the white space
                    if (string.IsNullOrEmpty(finalString))
                    {
                        // Build the string for the software
                        finalString = CheckingWhatIsSelectedSoftware(combobox.Text, _softwareData);
                    }
                    else
                    {
                        // Build the string for the software
                        finalString = finalString + " " + CheckingWhatIsSelectedSoftware(combobox.Text, _softwareData);
                    }

                }
                else if (rbGames.Checked == true)
                {
                    // Checking if there is already something in the string If ther isn't than just add the string dont need the add on itself and the white space
                    if (string.IsNullOrEmpty(finalString))
                    {
                        // Build the string for the game
                        finalString = CheckingWhatIsSelectedGame(combobox.Text, _gameData);
                    }
                    else
                    {
                        // Build the string for the game
                        finalString = finalString + " " + CheckingWhatIsSelectedGame(combobox.Text, _gameData);
                    }

                }
                else if (rbMovies.Checked == true)
                {
                    // Checking if there is already something in the string If ther isn't than just add the string dont need the add on itself and the white space
                    if (string.IsNullOrEmpty(finalString))
                    {
                        // Build the string for the movie
                        finalString = CheckingWhatIsSelectedMovie(combobox.Text, _movieData);
                    }
                    else
                    {
                        // Build the string for the movie
                        finalString = finalString + " " + CheckingWhatIsSelectedMovie(combobox.Text, _movieData);
                    }

                }
                else  // Throw a error because that means this program will be faital
                {
                    throw new ApplicationException("There was a problem with makeing the string /n Please Contact the developer");
                }
            }

            return finalString;
        }




        // remane the forlder
        private void btnDone_Click(object sender, EventArgs e)
        {
            string newFolderName = NewNameOfFolder();

            // Checking that the old name is not the smae as the new one
            if (_oldFolderName != newFolderName && !String.IsNullOrEmpty(newFolderName))
            {
                try
                {
                    // Rename the folder
                    FileSystem.RenameDirectory(_oldFolderName, newFolderName);                    

                    // Set the proboty so the new folder name can be retreaved
                    NewFolderLocationName = Directory.GetParent(_oldFolderName).ToString() + newFolderName;

                }
                catch (Exception ex)
                {
                    throw ex;
                }

                // closeing the from
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else if (_oldFolderName == newFolderName)
            {
                throw new InvalidExpressionException("You cant rename a folder with the same name");
            }
            else if (String.IsNullOrEmpty(newFolderName))
            {
                throw new ArgumentNullException("Comboboxes", "The comboboxes have to contain something cannot be 'nothing' for all the comboboxes");
            }


        }

        // Closees the from without doing anything
        private void btnCancel_Click(object sender, EventArgs e)
        {
            // Closeing the form
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

    }
























}
