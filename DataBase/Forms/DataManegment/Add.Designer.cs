﻿namespace DataBase.Forms.DataManegment
{
    partial class Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpSoftware = new System.Windows.Forms.TabPage();
            this.srbInternet = new System.Windows.Forms.RadioButton();
            this.srbLocal = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.label56 = new System.Windows.Forms.Label();
            this.StbLocation = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.StbPictureLocation = new System.Windows.Forms.TextBox();
            this.SbtnGetPicture = new System.Windows.Forms.Button();
            this.SbtnCancel = new System.Windows.Forms.Button();
            this.SbtnDone = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.ScbCrackType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.StbCustom1 = new System.Windows.Forms.TextBox();
            this.StbCustom = new System.Windows.Forms.TextBox();
            this.StbTorrentLinkSearch = new System.Windows.Forms.TextBox();
            this.StbSubCategory = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ScbSizeMetric = new System.Windows.Forms.ComboBox();
            this.ScbTable = new System.Windows.Forms.ComboBox();
            this.StbSize = new System.Windows.Forms.TextBox();
            this.StbVersion = new System.Windows.Forms.TextBox();
            this.StbPublisher = new System.Windows.Forms.TextBox();
            this.StbDeveloper = new System.Windows.Forms.TextBox();
            this.StbName = new System.Windows.Forms.TextBox();
            this.tpGames = new System.Windows.Forms.TabPage();
            this.GrbInternet = new System.Windows.Forms.RadioButton();
            this.GrbLocal = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.label57 = new System.Windows.Forms.Label();
            this.GtbLocation = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.GtbPictureLocation = new System.Windows.Forms.TextBox();
            this.GbtnGetMovePicture = new System.Windows.Forms.Button();
            this.GbtnCancel = new System.Windows.Forms.Button();
            this.GbtnDone = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.GcbCrackType = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.GtbCustom1 = new System.Windows.Forms.TextBox();
            this.GtbCustom = new System.Windows.Forms.TextBox();
            this.GtbTorrentWebLink = new System.Windows.Forms.TextBox();
            this.GtbGenre = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.GcbSizeMetric = new System.Windows.Forms.ComboBox();
            this.GcbTable = new System.Windows.Forms.ComboBox();
            this.GtbSize = new System.Windows.Forms.TextBox();
            this.GtbVersion = new System.Windows.Forms.TextBox();
            this.GtbPublisher = new System.Windows.Forms.TextBox();
            this.GtbDeveloper = new System.Windows.Forms.TextBox();
            this.GtbName = new System.Windows.Forms.TextBox();
            this.tpMovies = new System.Windows.Forms.TabPage();
            this.MrbInternet = new System.Windows.Forms.RadioButton();
            this.MrbLocal = new System.Windows.Forms.RadioButton();
            this.button3 = new System.Windows.Forms.Button();
            this.label58 = new System.Windows.Forms.Label();
            this.MtbLocation = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.MtbCustom1 = new System.Windows.Forms.TextBox();
            this.MtbGenre1 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.MtbCustom = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.MtbSubtitles1 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.MtbPictureLocation = new System.Windows.Forms.TextBox();
            this.MbtnGetPicture = new System.Windows.Forms.Button();
            this.MbtnCancel = new System.Windows.Forms.Button();
            this.MbtnDone = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.MtbLanguage1 = new System.Windows.Forms.TextBox();
            this.MtbLanuage = new System.Windows.Forms.TextBox();
            this.MtbGenre2 = new System.Windows.Forms.TextBox();
            this.MtbGenre = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.McbSizeMetric = new System.Windows.Forms.ComboBox();
            this.McbTable = new System.Windows.Forms.ComboBox();
            this.MtbSize = new System.Windows.Forms.TextBox();
            this.MtbSubtitles = new System.Windows.Forms.TextBox();
            this.MtbLenght = new System.Windows.Forms.TextBox();
            this.MtbStudio = new System.Windows.Forms.TextBox();
            this.MtbName = new System.Windows.Forms.TextBox();
            this.tpSeries = new System.Windows.Forms.TabPage();
            this.SEdtpNextSeasonDate = new System.Windows.Forms.DateTimePicker();
            this.SErbInternet = new System.Windows.Forms.RadioButton();
            this.SErbLocal = new System.Windows.Forms.RadioButton();
            this.button4 = new System.Windows.Forms.Button();
            this.label59 = new System.Windows.Forms.Label();
            this.SEtbLocation = new System.Windows.Forms.TextBox();
            this.SetbGenre1 = new System.Windows.Forms.TextBox();
            this.SEcbQuality = new System.Windows.Forms.ComboBox();
            this.SEtbSeason = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.SEtbCustom1 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.SEtbLenght = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.SEtbCustom = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.SEtbPictureLocation = new System.Windows.Forms.TextBox();
            this.SEbtnGetPicture = new System.Windows.Forms.Button();
            this.SEbtnCancel = new System.Windows.Forms.Button();
            this.SEbtnDone = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.SEtbCheckNewLink = new System.Windows.Forms.TextBox();
            this.SEtbLanguage = new System.Windows.Forms.TextBox();
            this.SEtbGenre = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.SEcbSizeMetric = new System.Windows.Forms.ComboBox();
            this.SEcbTable = new System.Windows.Forms.ComboBox();
            this.SEtbSize = new System.Windows.Forms.TextBox();
            this.SEtbEpisode = new System.Windows.Forms.TextBox();
            this.SEtbSutdio = new System.Windows.Forms.TextBox();
            this.SEtbName = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tpSoftware.SuspendLayout();
            this.tpGames.SuspendLayout();
            this.tpMovies.SuspendLayout();
            this.tpSeries.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpSoftware);
            this.tabControl1.Controls.Add(this.tpGames);
            this.tabControl1.Controls.Add(this.tpMovies);
            this.tabControl1.Controls.Add(this.tpSeries);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(70, 16);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(717, 369);
            this.tabControl1.TabIndex = 0;
            // 
            // tpSoftware
            // 
            this.tpSoftware.BackColor = System.Drawing.SystemColors.Control;
            this.tpSoftware.Controls.Add(this.srbInternet);
            this.tpSoftware.Controls.Add(this.srbLocal);
            this.tpSoftware.Controls.Add(this.button1);
            this.tpSoftware.Controls.Add(this.label56);
            this.tpSoftware.Controls.Add(this.StbLocation);
            this.tpSoftware.Controls.Add(this.label12);
            this.tpSoftware.Controls.Add(this.StbPictureLocation);
            this.tpSoftware.Controls.Add(this.SbtnGetPicture);
            this.tpSoftware.Controls.Add(this.SbtnCancel);
            this.tpSoftware.Controls.Add(this.SbtnDone);
            this.tpSoftware.Controls.Add(this.label11);
            this.tpSoftware.Controls.Add(this.ScbCrackType);
            this.tpSoftware.Controls.Add(this.label6);
            this.tpSoftware.Controls.Add(this.label7);
            this.tpSoftware.Controls.Add(this.label8);
            this.tpSoftware.Controls.Add(this.label9);
            this.tpSoftware.Controls.Add(this.label10);
            this.tpSoftware.Controls.Add(this.StbCustom1);
            this.tpSoftware.Controls.Add(this.StbCustom);
            this.tpSoftware.Controls.Add(this.StbTorrentLinkSearch);
            this.tpSoftware.Controls.Add(this.StbSubCategory);
            this.tpSoftware.Controls.Add(this.label5);
            this.tpSoftware.Controls.Add(this.label4);
            this.tpSoftware.Controls.Add(this.label3);
            this.tpSoftware.Controls.Add(this.label2);
            this.tpSoftware.Controls.Add(this.label1);
            this.tpSoftware.Controls.Add(this.ScbSizeMetric);
            this.tpSoftware.Controls.Add(this.ScbTable);
            this.tpSoftware.Controls.Add(this.StbSize);
            this.tpSoftware.Controls.Add(this.StbVersion);
            this.tpSoftware.Controls.Add(this.StbPublisher);
            this.tpSoftware.Controls.Add(this.StbDeveloper);
            this.tpSoftware.Controls.Add(this.StbName);
            this.tpSoftware.Location = new System.Drawing.Point(4, 48);
            this.tpSoftware.Name = "tpSoftware";
            this.tpSoftware.Padding = new System.Windows.Forms.Padding(7);
            this.tpSoftware.Size = new System.Drawing.Size(709, 317);
            this.tpSoftware.TabIndex = 0;
            this.tpSoftware.Text = "Software";
            // 
            // srbInternet
            // 
            this.srbInternet.AutoSize = true;
            this.srbInternet.Location = new System.Drawing.Point(545, 36);
            this.srbInternet.Name = "srbInternet";
            this.srbInternet.Size = new System.Drawing.Size(61, 17);
            this.srbInternet.TabIndex = 33;
            this.srbInternet.TabStop = true;
            this.srbInternet.Text = "Internet";
            this.srbInternet.UseVisualStyleBackColor = true;
            // 
            // srbLocal
            // 
            this.srbLocal.AutoSize = true;
            this.srbLocal.Checked = true;
            this.srbLocal.Location = new System.Drawing.Point(434, 36);
            this.srbLocal.Name = "srbLocal";
            this.srbLocal.Size = new System.Drawing.Size(51, 17);
            this.srbLocal.TabIndex = 32;
            this.srbLocal.TabStop = true;
            this.srbLocal.Text = "Local";
            this.srbLocal.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(564, 246);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(108, 23);
            this.button1.TabIndex = 31;
            this.button1.Text = "Folder Location";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.SbtnGetFolderLocation_Click);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(348, 251);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(48, 13);
            this.label56.TabIndex = 30;
            this.label56.Text = "Location";
            // 
            // StbLocation
            // 
            this.StbLocation.Location = new System.Drawing.Point(434, 248);
            this.StbLocation.Name = "StbLocation";
            this.StbLocation.Size = new System.Drawing.Size(124, 20);
            this.StbLocation.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(290, 13);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(129, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "PictureDownloadLoaction";
            // 
            // StbPictureLocation
            // 
            this.StbPictureLocation.Location = new System.Drawing.Point(425, 10);
            this.StbPictureLocation.Name = "StbPictureLocation";
            this.StbPictureLocation.Size = new System.Drawing.Size(235, 20);
            this.StbPictureLocation.TabIndex = 27;
            this.StbPictureLocation.MouseEnter += new System.EventHandler(this.tbPictureLocation_MouseEnter);
            // 
            // SbtnGetPicture
            // 
            this.SbtnGetPicture.Location = new System.Drawing.Point(189, 265);
            this.SbtnGetPicture.Name = "SbtnGetPicture";
            this.SbtnGetPicture.Size = new System.Drawing.Size(108, 23);
            this.SbtnGetPicture.TabIndex = 26;
            this.SbtnGetPicture.Text = "Select Picture";
            this.SbtnGetPicture.UseVisualStyleBackColor = true;
            this.SbtnGetPicture.Click += new System.EventHandler(this.SbtnGetPicture_Click);
            // 
            // SbtnCancel
            // 
            this.SbtnCancel.Location = new System.Drawing.Point(108, 265);
            this.SbtnCancel.Name = "SbtnCancel";
            this.SbtnCancel.Size = new System.Drawing.Size(75, 23);
            this.SbtnCancel.TabIndex = 25;
            this.SbtnCancel.Text = "Cancel";
            this.SbtnCancel.UseVisualStyleBackColor = true;
            this.SbtnCancel.Click += new System.EventHandler(this.Cancel_Button_Clicked);
            // 
            // SbtnDone
            // 
            this.SbtnDone.Location = new System.Drawing.Point(27, 265);
            this.SbtnDone.Name = "SbtnDone";
            this.SbtnDone.Size = new System.Drawing.Size(75, 23);
            this.SbtnDone.TabIndex = 24;
            this.SbtnDone.Text = "Done";
            this.SbtnDone.UseVisualStyleBackColor = true;
            this.SbtnDone.Click += new System.EventHandler(this.SbtnDone_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(34, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Table";
            // 
            // ScbCrackType
            // 
            this.ScbCrackType.AutoCompleteCustomSource.AddRange(new string[] {
            "Paid",
            "Serial",
            "Crack Copy",
            "Crack Patch",
            "Free"});
            this.ScbCrackType.FormattingEnabled = true;
            this.ScbCrackType.Location = new System.Drawing.Point(434, 107);
            this.ScbCrackType.Name = "ScbCrackType";
            this.ScbCrackType.Size = new System.Drawing.Size(235, 21);
            this.ScbCrackType.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(348, 216);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Custom1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(348, 181);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Custom";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(348, 145);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Torrent web link";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(348, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Crack Type";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(348, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Sub Category";
            // 
            // StbCustom1
            // 
            this.StbCustom1.Location = new System.Drawing.Point(434, 213);
            this.StbCustom1.Name = "StbCustom1";
            this.StbCustom1.Size = new System.Drawing.Size(235, 20);
            this.StbCustom1.TabIndex = 16;
            // 
            // StbCustom
            // 
            this.StbCustom.Location = new System.Drawing.Point(434, 178);
            this.StbCustom.Name = "StbCustom";
            this.StbCustom.Size = new System.Drawing.Size(235, 20);
            this.StbCustom.TabIndex = 15;
            // 
            // StbTorrentLinkSearch
            // 
            this.StbTorrentLinkSearch.Location = new System.Drawing.Point(434, 142);
            this.StbTorrentLinkSearch.Name = "StbTorrentLinkSearch";
            this.StbTorrentLinkSearch.Size = new System.Drawing.Size(235, 20);
            this.StbTorrentLinkSearch.TabIndex = 14;
            // 
            // StbSubCategory
            // 
            this.StbSubCategory.Location = new System.Drawing.Point(434, 72);
            this.StbSubCategory.Name = "StbSubCategory";
            this.StbSubCategory.Size = new System.Drawing.Size(235, 20);
            this.StbSubCategory.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 216);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Size";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Version";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Publisher";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Developer";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Name";
            // 
            // ScbSizeMetric
            // 
            this.ScbSizeMetric.AutoCompleteCustomSource.AddRange(new string[] {
            "KB",
            "MB",
            "GB",
            "TB"});
            this.ScbSizeMetric.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ScbSizeMetric.FormattingEnabled = true;
            this.ScbSizeMetric.Items.AddRange(new object[] {
            "KB",
            "MB",
            "GB",
            "TB"});
            this.ScbSizeMetric.Location = new System.Drawing.Point(252, 213);
            this.ScbSizeMetric.Name = "ScbSizeMetric";
            this.ScbSizeMetric.Size = new System.Drawing.Size(65, 21);
            this.ScbSizeMetric.TabIndex = 6;
            // 
            // ScbTable
            // 
            this.ScbTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ScbTable.FormattingEnabled = true;
            this.ScbTable.Location = new System.Drawing.Point(92, 10);
            this.ScbTable.Name = "ScbTable";
            this.ScbTable.Size = new System.Drawing.Size(164, 21);
            this.ScbTable.TabIndex = 5;
            // 
            // StbSize
            // 
            this.StbSize.Location = new System.Drawing.Point(82, 213);
            this.StbSize.Name = "StbSize";
            this.StbSize.Size = new System.Drawing.Size(164, 20);
            this.StbSize.TabIndex = 4;
            // 
            // StbVersion
            // 
            this.StbVersion.Location = new System.Drawing.Point(82, 178);
            this.StbVersion.Name = "StbVersion";
            this.StbVersion.Size = new System.Drawing.Size(235, 20);
            this.StbVersion.TabIndex = 3;
            // 
            // StbPublisher
            // 
            this.StbPublisher.Location = new System.Drawing.Point(82, 142);
            this.StbPublisher.Name = "StbPublisher";
            this.StbPublisher.Size = new System.Drawing.Size(235, 20);
            this.StbPublisher.TabIndex = 2;
            // 
            // StbDeveloper
            // 
            this.StbDeveloper.Location = new System.Drawing.Point(82, 107);
            this.StbDeveloper.Name = "StbDeveloper";
            this.StbDeveloper.Size = new System.Drawing.Size(235, 20);
            this.StbDeveloper.TabIndex = 1;
            // 
            // StbName
            // 
            this.StbName.Location = new System.Drawing.Point(82, 72);
            this.StbName.Name = "StbName";
            this.StbName.Size = new System.Drawing.Size(235, 20);
            this.StbName.TabIndex = 0;
            this.StbName.TextChanged += new System.EventHandler(this.StbName_TextChanged);
            // 
            // tpGames
            // 
            this.tpGames.BackColor = System.Drawing.SystemColors.Control;
            this.tpGames.Controls.Add(this.GrbInternet);
            this.tpGames.Controls.Add(this.GrbLocal);
            this.tpGames.Controls.Add(this.button2);
            this.tpGames.Controls.Add(this.label57);
            this.tpGames.Controls.Add(this.GtbLocation);
            this.tpGames.Controls.Add(this.label13);
            this.tpGames.Controls.Add(this.GtbPictureLocation);
            this.tpGames.Controls.Add(this.GbtnGetMovePicture);
            this.tpGames.Controls.Add(this.GbtnCancel);
            this.tpGames.Controls.Add(this.GbtnDone);
            this.tpGames.Controls.Add(this.label14);
            this.tpGames.Controls.Add(this.GcbCrackType);
            this.tpGames.Controls.Add(this.label15);
            this.tpGames.Controls.Add(this.label16);
            this.tpGames.Controls.Add(this.label17);
            this.tpGames.Controls.Add(this.label18);
            this.tpGames.Controls.Add(this.label19);
            this.tpGames.Controls.Add(this.GtbCustom1);
            this.tpGames.Controls.Add(this.GtbCustom);
            this.tpGames.Controls.Add(this.GtbTorrentWebLink);
            this.tpGames.Controls.Add(this.GtbGenre);
            this.tpGames.Controls.Add(this.label20);
            this.tpGames.Controls.Add(this.label21);
            this.tpGames.Controls.Add(this.label22);
            this.tpGames.Controls.Add(this.label23);
            this.tpGames.Controls.Add(this.label24);
            this.tpGames.Controls.Add(this.GcbSizeMetric);
            this.tpGames.Controls.Add(this.GcbTable);
            this.tpGames.Controls.Add(this.GtbSize);
            this.tpGames.Controls.Add(this.GtbVersion);
            this.tpGames.Controls.Add(this.GtbPublisher);
            this.tpGames.Controls.Add(this.GtbDeveloper);
            this.tpGames.Controls.Add(this.GtbName);
            this.tpGames.Location = new System.Drawing.Point(4, 48);
            this.tpGames.Name = "tpGames";
            this.tpGames.Padding = new System.Windows.Forms.Padding(3);
            this.tpGames.Size = new System.Drawing.Size(709, 317);
            this.tpGames.TabIndex = 1;
            this.tpGames.Text = "Games";
            // 
            // GrbInternet
            // 
            this.GrbInternet.AutoSize = true;
            this.GrbInternet.Location = new System.Drawing.Point(571, 49);
            this.GrbInternet.Name = "GrbInternet";
            this.GrbInternet.Size = new System.Drawing.Size(61, 17);
            this.GrbInternet.TabIndex = 61;
            this.GrbInternet.TabStop = true;
            this.GrbInternet.Text = "Internet";
            this.GrbInternet.UseVisualStyleBackColor = true;
            // 
            // GrbLocal
            // 
            this.GrbLocal.AutoSize = true;
            this.GrbLocal.Checked = true;
            this.GrbLocal.Location = new System.Drawing.Point(460, 49);
            this.GrbLocal.Name = "GrbLocal";
            this.GrbLocal.Size = new System.Drawing.Size(51, 17);
            this.GrbLocal.TabIndex = 60;
            this.GrbLocal.TabStop = true;
            this.GrbLocal.Text = "Local";
            this.GrbLocal.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(571, 261);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 23);
            this.button2.TabIndex = 59;
            this.button2.Text = "Folder Location";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.GbtnGetFolderLocation_Click);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(356, 264);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(48, 13);
            this.label57.TabIndex = 58;
            this.label57.Text = "Location";
            // 
            // GtbLocation
            // 
            this.GtbLocation.Location = new System.Drawing.Point(442, 261);
            this.GtbLocation.Name = "GtbLocation";
            this.GtbLocation.Size = new System.Drawing.Size(123, 20);
            this.GtbLocation.TabIndex = 57;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(298, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(129, 13);
            this.label13.TabIndex = 56;
            this.label13.Text = "PictureDownloadLoaction";
            // 
            // GtbPictureLocation
            // 
            this.GtbPictureLocation.Location = new System.Drawing.Point(433, 23);
            this.GtbPictureLocation.Name = "GtbPictureLocation";
            this.GtbPictureLocation.Size = new System.Drawing.Size(235, 20);
            this.GtbPictureLocation.TabIndex = 55;
            this.GtbPictureLocation.MouseEnter += new System.EventHandler(this.tbPictureLocation_MouseEnter);
            // 
            // GbtnGetMovePicture
            // 
            this.GbtnGetMovePicture.Location = new System.Drawing.Point(197, 278);
            this.GbtnGetMovePicture.Name = "GbtnGetMovePicture";
            this.GbtnGetMovePicture.Size = new System.Drawing.Size(108, 23);
            this.GbtnGetMovePicture.TabIndex = 54;
            this.GbtnGetMovePicture.Text = "Select Picture";
            this.GbtnGetMovePicture.UseVisualStyleBackColor = true;
            this.GbtnGetMovePicture.Click += new System.EventHandler(this.GbtnGetMovePicture_Click);
            // 
            // GbtnCancel
            // 
            this.GbtnCancel.Location = new System.Drawing.Point(116, 278);
            this.GbtnCancel.Name = "GbtnCancel";
            this.GbtnCancel.Size = new System.Drawing.Size(75, 23);
            this.GbtnCancel.TabIndex = 53;
            this.GbtnCancel.Text = "Cancel";
            this.GbtnCancel.UseVisualStyleBackColor = true;
            this.GbtnCancel.Click += new System.EventHandler(this.Cancel_Button_Clicked);
            // 
            // GbtnDone
            // 
            this.GbtnDone.Location = new System.Drawing.Point(35, 278);
            this.GbtnDone.Name = "GbtnDone";
            this.GbtnDone.Size = new System.Drawing.Size(75, 23);
            this.GbtnDone.TabIndex = 52;
            this.GbtnDone.Text = "Done";
            this.GbtnDone.UseVisualStyleBackColor = true;
            this.GbtnDone.Click += new System.EventHandler(this.GbttnDone_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(42, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 51;
            this.label14.Text = "Table";
            // 
            // GcbCrackType
            // 
            this.GcbCrackType.AutoCompleteCustomSource.AddRange(new string[] {
            "Paid",
            "Serial",
            "Crack Copy",
            "Crack Patch",
            "Free"});
            this.GcbCrackType.FormattingEnabled = true;
            this.GcbCrackType.Location = new System.Drawing.Point(442, 120);
            this.GcbCrackType.Name = "GcbCrackType";
            this.GcbCrackType.Size = new System.Drawing.Size(235, 21);
            this.GcbCrackType.TabIndex = 50;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(356, 229);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 13);
            this.label15.TabIndex = 49;
            this.label15.Text = "Custom1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(356, 194);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 13);
            this.label16.TabIndex = 48;
            this.label16.Text = "Custom";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(356, 158);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 13);
            this.label17.TabIndex = 47;
            this.label17.Text = "Torrent web link";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(356, 123);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 13);
            this.label18.TabIndex = 46;
            this.label18.Text = "Crack Type";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(356, 88);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 13);
            this.label19.TabIndex = 45;
            this.label19.Text = "Genre";
            // 
            // GtbCustom1
            // 
            this.GtbCustom1.Location = new System.Drawing.Point(442, 226);
            this.GtbCustom1.Name = "GtbCustom1";
            this.GtbCustom1.Size = new System.Drawing.Size(235, 20);
            this.GtbCustom1.TabIndex = 44;
            // 
            // GtbCustom
            // 
            this.GtbCustom.Location = new System.Drawing.Point(442, 191);
            this.GtbCustom.Name = "GtbCustom";
            this.GtbCustom.Size = new System.Drawing.Size(235, 20);
            this.GtbCustom.TabIndex = 43;
            // 
            // GtbTorrentWebLink
            // 
            this.GtbTorrentWebLink.Location = new System.Drawing.Point(442, 155);
            this.GtbTorrentWebLink.Name = "GtbTorrentWebLink";
            this.GtbTorrentWebLink.Size = new System.Drawing.Size(235, 20);
            this.GtbTorrentWebLink.TabIndex = 42;
            // 
            // GtbGenre
            // 
            this.GtbGenre.Location = new System.Drawing.Point(442, 85);
            this.GtbGenre.Name = "GtbGenre";
            this.GtbGenre.Size = new System.Drawing.Size(235, 20);
            this.GtbGenre.TabIndex = 41;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(32, 229);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(27, 13);
            this.label20.TabIndex = 40;
            this.label20.Text = "Size";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(32, 194);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(42, 13);
            this.label21.TabIndex = 39;
            this.label21.Text = "Version";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(32, 158);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(50, 13);
            this.label22.TabIndex = 38;
            this.label22.Text = "Publisher";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(32, 123);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 13);
            this.label23.TabIndex = 37;
            this.label23.Text = "Developer";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(32, 88);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 13);
            this.label24.TabIndex = 36;
            this.label24.Text = "Name";
            // 
            // GcbSizeMetric
            // 
            this.GcbSizeMetric.AutoCompleteCustomSource.AddRange(new string[] {
            "KB",
            "MB",
            "GB",
            "TB"});
            this.GcbSizeMetric.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GcbSizeMetric.FormattingEnabled = true;
            this.GcbSizeMetric.Items.AddRange(new object[] {
            "KB",
            "MB",
            "GB",
            "TB"});
            this.GcbSizeMetric.Location = new System.Drawing.Point(260, 226);
            this.GcbSizeMetric.Name = "GcbSizeMetric";
            this.GcbSizeMetric.Size = new System.Drawing.Size(65, 21);
            this.GcbSizeMetric.TabIndex = 35;
            // 
            // GcbTable
            // 
            this.GcbTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GcbTable.FormattingEnabled = true;
            this.GcbTable.Location = new System.Drawing.Point(100, 23);
            this.GcbTable.Name = "GcbTable";
            this.GcbTable.Size = new System.Drawing.Size(164, 21);
            this.GcbTable.TabIndex = 34;
            // 
            // GtbSize
            // 
            this.GtbSize.Location = new System.Drawing.Point(90, 226);
            this.GtbSize.Name = "GtbSize";
            this.GtbSize.Size = new System.Drawing.Size(164, 20);
            this.GtbSize.TabIndex = 33;
            // 
            // GtbVersion
            // 
            this.GtbVersion.Location = new System.Drawing.Point(90, 191);
            this.GtbVersion.Name = "GtbVersion";
            this.GtbVersion.Size = new System.Drawing.Size(235, 20);
            this.GtbVersion.TabIndex = 32;
            // 
            // GtbPublisher
            // 
            this.GtbPublisher.Location = new System.Drawing.Point(90, 155);
            this.GtbPublisher.Name = "GtbPublisher";
            this.GtbPublisher.Size = new System.Drawing.Size(235, 20);
            this.GtbPublisher.TabIndex = 31;
            // 
            // GtbDeveloper
            // 
            this.GtbDeveloper.Location = new System.Drawing.Point(90, 120);
            this.GtbDeveloper.Name = "GtbDeveloper";
            this.GtbDeveloper.Size = new System.Drawing.Size(235, 20);
            this.GtbDeveloper.TabIndex = 30;
            // 
            // GtbName
            // 
            this.GtbName.Location = new System.Drawing.Point(90, 85);
            this.GtbName.Name = "GtbName";
            this.GtbName.Size = new System.Drawing.Size(235, 20);
            this.GtbName.TabIndex = 29;
            this.GtbName.TextChanged += new System.EventHandler(this.GtbName_TextChanged);
            // 
            // tpMovies
            // 
            this.tpMovies.BackColor = System.Drawing.SystemColors.Control;
            this.tpMovies.Controls.Add(this.MrbInternet);
            this.tpMovies.Controls.Add(this.MrbLocal);
            this.tpMovies.Controls.Add(this.button3);
            this.tpMovies.Controls.Add(this.label58);
            this.tpMovies.Controls.Add(this.MtbLocation);
            this.tpMovies.Controls.Add(this.label51);
            this.tpMovies.Controls.Add(this.MtbCustom1);
            this.tpMovies.Controls.Add(this.MtbGenre1);
            this.tpMovies.Controls.Add(this.label49);
            this.tpMovies.Controls.Add(this.MtbCustom);
            this.tpMovies.Controls.Add(this.label50);
            this.tpMovies.Controls.Add(this.MtbSubtitles1);
            this.tpMovies.Controls.Add(this.label25);
            this.tpMovies.Controls.Add(this.MtbPictureLocation);
            this.tpMovies.Controls.Add(this.MbtnGetPicture);
            this.tpMovies.Controls.Add(this.MbtnCancel);
            this.tpMovies.Controls.Add(this.MbtnDone);
            this.tpMovies.Controls.Add(this.label26);
            this.tpMovies.Controls.Add(this.label27);
            this.tpMovies.Controls.Add(this.label28);
            this.tpMovies.Controls.Add(this.label29);
            this.tpMovies.Controls.Add(this.label30);
            this.tpMovies.Controls.Add(this.label31);
            this.tpMovies.Controls.Add(this.MtbLanguage1);
            this.tpMovies.Controls.Add(this.MtbLanuage);
            this.tpMovies.Controls.Add(this.MtbGenre2);
            this.tpMovies.Controls.Add(this.MtbGenre);
            this.tpMovies.Controls.Add(this.label32);
            this.tpMovies.Controls.Add(this.label33);
            this.tpMovies.Controls.Add(this.label34);
            this.tpMovies.Controls.Add(this.label35);
            this.tpMovies.Controls.Add(this.label36);
            this.tpMovies.Controls.Add(this.McbSizeMetric);
            this.tpMovies.Controls.Add(this.McbTable);
            this.tpMovies.Controls.Add(this.MtbSize);
            this.tpMovies.Controls.Add(this.MtbSubtitles);
            this.tpMovies.Controls.Add(this.MtbLenght);
            this.tpMovies.Controls.Add(this.MtbStudio);
            this.tpMovies.Controls.Add(this.MtbName);
            this.tpMovies.Location = new System.Drawing.Point(4, 48);
            this.tpMovies.Name = "tpMovies";
            this.tpMovies.Size = new System.Drawing.Size(709, 317);
            this.tpMovies.TabIndex = 2;
            this.tpMovies.Text = "Movies";
            // 
            // MrbInternet
            // 
            this.MrbInternet.AutoSize = true;
            this.MrbInternet.Location = new System.Drawing.Point(571, 49);
            this.MrbInternet.Name = "MrbInternet";
            this.MrbInternet.Size = new System.Drawing.Size(61, 17);
            this.MrbInternet.TabIndex = 70;
            this.MrbInternet.TabStop = true;
            this.MrbInternet.Text = "Internet";
            this.MrbInternet.UseVisualStyleBackColor = true;
            // 
            // MrbLocal
            // 
            this.MrbLocal.AutoSize = true;
            this.MrbLocal.Checked = true;
            this.MrbLocal.Location = new System.Drawing.Point(460, 49);
            this.MrbLocal.Name = "MrbLocal";
            this.MrbLocal.Size = new System.Drawing.Size(51, 17);
            this.MrbLocal.TabIndex = 69;
            this.MrbLocal.TabStop = true;
            this.MrbLocal.Text = "Local";
            this.MrbLocal.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(560, 256);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(108, 23);
            this.button3.TabIndex = 68;
            this.button3.Text = "Folder Location";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.MbtnGetFolderLocation_Click);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(347, 261);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(48, 13);
            this.label58.TabIndex = 67;
            this.label58.Text = "Location";
            // 
            // MtbLocation
            // 
            this.MtbLocation.Location = new System.Drawing.Point(433, 258);
            this.MtbLocation.Name = "MtbLocation";
            this.MtbLocation.Size = new System.Drawing.Size(121, 20);
            this.MtbLocation.TabIndex = 66;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(347, 235);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(48, 13);
            this.label51.TabIndex = 65;
            this.label51.Text = "Custom1";
            // 
            // MtbCustom1
            // 
            this.MtbCustom1.Location = new System.Drawing.Point(433, 232);
            this.MtbCustom1.Name = "MtbCustom1";
            this.MtbCustom1.Size = new System.Drawing.Size(235, 20);
            this.MtbCustom1.TabIndex = 64;
            // 
            // MtbGenre1
            // 
            this.MtbGenre1.Location = new System.Drawing.Point(432, 102);
            this.MtbGenre1.Name = "MtbGenre1";
            this.MtbGenre1.Size = new System.Drawing.Size(235, 20);
            this.MtbGenre1.TabIndex = 61;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(346, 209);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(42, 13);
            this.label49.TabIndex = 60;
            this.label49.Text = "Custom";
            // 
            // MtbCustom
            // 
            this.MtbCustom.Location = new System.Drawing.Point(432, 206);
            this.MtbCustom.Name = "MtbCustom";
            this.MtbCustom.Size = new System.Drawing.Size(235, 20);
            this.MtbCustom.TabIndex = 59;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(22, 209);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(51, 13);
            this.label50.TabIndex = 58;
            this.label50.Text = "subtitles1";
            // 
            // MtbSubtitles1
            // 
            this.MtbSubtitles1.Location = new System.Drawing.Point(80, 206);
            this.MtbSubtitles1.Name = "MtbSubtitles1";
            this.MtbSubtitles1.Size = new System.Drawing.Size(235, 20);
            this.MtbSubtitles1.TabIndex = 57;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(298, 26);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(129, 13);
            this.label25.TabIndex = 56;
            this.label25.Text = "PictureDownloadLoaction";
            // 
            // MtbPictureLocation
            // 
            this.MtbPictureLocation.Location = new System.Drawing.Point(433, 23);
            this.MtbPictureLocation.Name = "MtbPictureLocation";
            this.MtbPictureLocation.Size = new System.Drawing.Size(235, 20);
            this.MtbPictureLocation.TabIndex = 55;
            this.MtbPictureLocation.MouseEnter += new System.EventHandler(this.tbPictureLocation_MouseEnter);
            // 
            // MbtnGetPicture
            // 
            this.MbtnGetPicture.Location = new System.Drawing.Point(187, 255);
            this.MbtnGetPicture.Name = "MbtnGetPicture";
            this.MbtnGetPicture.Size = new System.Drawing.Size(108, 23);
            this.MbtnGetPicture.TabIndex = 54;
            this.MbtnGetPicture.Text = "Select Picture";
            this.MbtnGetPicture.UseVisualStyleBackColor = true;
            this.MbtnGetPicture.Click += new System.EventHandler(this.MbtnGetPicture_Click);
            // 
            // MbtnCancel
            // 
            this.MbtnCancel.Location = new System.Drawing.Point(106, 255);
            this.MbtnCancel.Name = "MbtnCancel";
            this.MbtnCancel.Size = new System.Drawing.Size(75, 23);
            this.MbtnCancel.TabIndex = 53;
            this.MbtnCancel.Text = "Cancel";
            this.MbtnCancel.UseVisualStyleBackColor = true;
            this.MbtnCancel.Click += new System.EventHandler(this.Cancel_Button_Clicked);
            // 
            // MbtnDone
            // 
            this.MbtnDone.Location = new System.Drawing.Point(25, 255);
            this.MbtnDone.Name = "MbtnDone";
            this.MbtnDone.Size = new System.Drawing.Size(75, 23);
            this.MbtnDone.TabIndex = 52;
            this.MbtnDone.Text = "Done";
            this.MbtnDone.UseVisualStyleBackColor = true;
            this.MbtnDone.Click += new System.EventHandler(this.MbtnDone_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(42, 26);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(34, 13);
            this.label26.TabIndex = 51;
            this.label26.Text = "Table";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(347, 183);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(61, 13);
            this.label27.TabIndex = 49;
            this.label27.Text = "Language1";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(347, 157);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(55, 13);
            this.label28.TabIndex = 48;
            this.label28.Text = "Language";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(346, 131);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(42, 13);
            this.label29.TabIndex = 47;
            this.label29.Text = "Genre2";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(347, 105);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(42, 13);
            this.label30.TabIndex = 46;
            this.label30.Text = "Genre1";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(347, 79);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(36, 13);
            this.label31.TabIndex = 45;
            this.label31.Text = "Genre";
            // 
            // MtbLanguage1
            // 
            this.MtbLanguage1.Location = new System.Drawing.Point(432, 180);
            this.MtbLanguage1.Name = "MtbLanguage1";
            this.MtbLanguage1.Size = new System.Drawing.Size(235, 20);
            this.MtbLanguage1.TabIndex = 44;
            // 
            // MtbLanuage
            // 
            this.MtbLanuage.Location = new System.Drawing.Point(433, 154);
            this.MtbLanuage.Name = "MtbLanuage";
            this.MtbLanuage.Size = new System.Drawing.Size(235, 20);
            this.MtbLanuage.TabIndex = 43;
            // 
            // MtbGenre2
            // 
            this.MtbGenre2.Location = new System.Drawing.Point(432, 128);
            this.MtbGenre2.Name = "MtbGenre2";
            this.MtbGenre2.Size = new System.Drawing.Size(235, 20);
            this.MtbGenre2.TabIndex = 42;
            // 
            // MtbGenre
            // 
            this.MtbGenre.Location = new System.Drawing.Point(433, 76);
            this.MtbGenre.Name = "MtbGenre";
            this.MtbGenre.Size = new System.Drawing.Size(235, 20);
            this.MtbGenre.TabIndex = 41;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(31, 183);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(27, 13);
            this.label32.TabIndex = 40;
            this.label32.Text = "Size";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(23, 157);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(45, 13);
            this.label33.TabIndex = 39;
            this.label33.Text = "subtitles";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(22, 131);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(40, 13);
            this.label34.TabIndex = 38;
            this.label34.Text = "Lenght";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(23, 105);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(37, 13);
            this.label35.TabIndex = 37;
            this.label35.Text = "Studio";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(23, 79);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(35, 13);
            this.label36.TabIndex = 36;
            this.label36.Text = "Name";
            // 
            // McbSizeMetric
            // 
            this.McbSizeMetric.AutoCompleteCustomSource.AddRange(new string[] {
            "KB",
            "MB",
            "GB",
            "TB"});
            this.McbSizeMetric.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.McbSizeMetric.FormattingEnabled = true;
            this.McbSizeMetric.Items.AddRange(new object[] {
            "KB",
            "MB",
            "GB",
            "TB"});
            this.McbSizeMetric.Location = new System.Drawing.Point(250, 180);
            this.McbSizeMetric.Name = "McbSizeMetric";
            this.McbSizeMetric.Size = new System.Drawing.Size(65, 21);
            this.McbSizeMetric.TabIndex = 35;
            // 
            // McbTable
            // 
            this.McbTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.McbTable.FormattingEnabled = true;
            this.McbTable.Location = new System.Drawing.Point(100, 23);
            this.McbTable.Name = "McbTable";
            this.McbTable.Size = new System.Drawing.Size(164, 21);
            this.McbTable.TabIndex = 34;
            // 
            // MtbSize
            // 
            this.MtbSize.Location = new System.Drawing.Point(80, 180);
            this.MtbSize.Name = "MtbSize";
            this.MtbSize.Size = new System.Drawing.Size(164, 20);
            this.MtbSize.TabIndex = 33;
            // 
            // MtbSubtitles
            // 
            this.MtbSubtitles.Location = new System.Drawing.Point(81, 154);
            this.MtbSubtitles.Name = "MtbSubtitles";
            this.MtbSubtitles.Size = new System.Drawing.Size(235, 20);
            this.MtbSubtitles.TabIndex = 32;
            // 
            // MtbLenght
            // 
            this.MtbLenght.Location = new System.Drawing.Point(80, 128);
            this.MtbLenght.Name = "MtbLenght";
            this.MtbLenght.Size = new System.Drawing.Size(235, 20);
            this.MtbLenght.TabIndex = 31;
            // 
            // MtbStudio
            // 
            this.MtbStudio.Location = new System.Drawing.Point(81, 102);
            this.MtbStudio.Name = "MtbStudio";
            this.MtbStudio.Size = new System.Drawing.Size(235, 20);
            this.MtbStudio.TabIndex = 30;
            // 
            // MtbName
            // 
            this.MtbName.Location = new System.Drawing.Point(81, 76);
            this.MtbName.Name = "MtbName";
            this.MtbName.Size = new System.Drawing.Size(235, 20);
            this.MtbName.TabIndex = 29;
            this.MtbName.TextChanged += new System.EventHandler(this.MtbName_TextChanged);
            // 
            // tpSeries
            // 
            this.tpSeries.BackColor = System.Drawing.SystemColors.Control;
            this.tpSeries.Controls.Add(this.SEdtpNextSeasonDate);
            this.tpSeries.Controls.Add(this.SErbInternet);
            this.tpSeries.Controls.Add(this.SErbLocal);
            this.tpSeries.Controls.Add(this.button4);
            this.tpSeries.Controls.Add(this.label59);
            this.tpSeries.Controls.Add(this.SEtbLocation);
            this.tpSeries.Controls.Add(this.SetbGenre1);
            this.tpSeries.Controls.Add(this.SEcbQuality);
            this.tpSeries.Controls.Add(this.SEtbSeason);
            this.tpSeries.Controls.Add(this.label54);
            this.tpSeries.Controls.Add(this.SEtbCustom1);
            this.tpSeries.Controls.Add(this.label55);
            this.tpSeries.Controls.Add(this.SEtbLenght);
            this.tpSeries.Controls.Add(this.label52);
            this.tpSeries.Controls.Add(this.SEtbCustom);
            this.tpSeries.Controls.Add(this.label53);
            this.tpSeries.Controls.Add(this.label37);
            this.tpSeries.Controls.Add(this.SEtbPictureLocation);
            this.tpSeries.Controls.Add(this.SEbtnGetPicture);
            this.tpSeries.Controls.Add(this.SEbtnCancel);
            this.tpSeries.Controls.Add(this.SEbtnDone);
            this.tpSeries.Controls.Add(this.label38);
            this.tpSeries.Controls.Add(this.label39);
            this.tpSeries.Controls.Add(this.label40);
            this.tpSeries.Controls.Add(this.label41);
            this.tpSeries.Controls.Add(this.label42);
            this.tpSeries.Controls.Add(this.label43);
            this.tpSeries.Controls.Add(this.SEtbCheckNewLink);
            this.tpSeries.Controls.Add(this.SEtbLanguage);
            this.tpSeries.Controls.Add(this.SEtbGenre);
            this.tpSeries.Controls.Add(this.label44);
            this.tpSeries.Controls.Add(this.label45);
            this.tpSeries.Controls.Add(this.label46);
            this.tpSeries.Controls.Add(this.label47);
            this.tpSeries.Controls.Add(this.label48);
            this.tpSeries.Controls.Add(this.SEcbSizeMetric);
            this.tpSeries.Controls.Add(this.SEcbTable);
            this.tpSeries.Controls.Add(this.SEtbSize);
            this.tpSeries.Controls.Add(this.SEtbEpisode);
            this.tpSeries.Controls.Add(this.SEtbSutdio);
            this.tpSeries.Controls.Add(this.SEtbName);
            this.tpSeries.Location = new System.Drawing.Point(4, 48);
            this.tpSeries.Name = "tpSeries";
            this.tpSeries.Size = new System.Drawing.Size(709, 317);
            this.tpSeries.TabIndex = 3;
            this.tpSeries.Text = "Series";
            // 
            // SEdtpNextSeasonDate
            // 
            this.SEdtpNextSeasonDate.Location = new System.Drawing.Point(446, 175);
            this.SEdtpNextSeasonDate.Name = "SEdtpNextSeasonDate";
            this.SEdtpNextSeasonDate.Size = new System.Drawing.Size(200, 20);
            this.SEdtpNextSeasonDate.TabIndex = 77;
            // 
            // SErbInternet
            // 
            this.SErbInternet.AutoSize = true;
            this.SErbInternet.Location = new System.Drawing.Point(578, 47);
            this.SErbInternet.Name = "SErbInternet";
            this.SErbInternet.Size = new System.Drawing.Size(61, 17);
            this.SErbInternet.TabIndex = 76;
            this.SErbInternet.TabStop = true;
            this.SErbInternet.Text = "Internet";
            this.SErbInternet.UseVisualStyleBackColor = true;
            // 
            // SErbLocal
            // 
            this.SErbLocal.AutoSize = true;
            this.SErbLocal.Checked = true;
            this.SErbLocal.Location = new System.Drawing.Point(467, 47);
            this.SErbLocal.Name = "SErbLocal";
            this.SErbLocal.Size = new System.Drawing.Size(51, 17);
            this.SErbLocal.TabIndex = 75;
            this.SErbLocal.TabStop = true;
            this.SErbLocal.Text = "Local";
            this.SErbLocal.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(569, 252);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(108, 23);
            this.button4.TabIndex = 74;
            this.button4.Text = "Folder Location";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.SEbtnGetFolderLocation_Click);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(356, 257);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(48, 13);
            this.label59.TabIndex = 73;
            this.label59.Text = "Location";
            // 
            // SEtbLocation
            // 
            this.SEtbLocation.Location = new System.Drawing.Point(442, 254);
            this.SEtbLocation.Name = "SEtbLocation";
            this.SEtbLocation.Size = new System.Drawing.Size(121, 20);
            this.SEtbLocation.TabIndex = 72;
            // 
            // SetbGenre1
            // 
            this.SetbGenre1.Location = new System.Drawing.Point(442, 97);
            this.SetbGenre1.Name = "SetbGenre1";
            this.SetbGenre1.Size = new System.Drawing.Size(235, 20);
            this.SetbGenre1.TabIndex = 71;
            // 
            // SEcbQuality
            // 
            this.SEcbQuality.AutoCompleteCustomSource.AddRange(new string[] {
            "Paid",
            "Serial",
            "Crack Copy",
            "Crack Patch",
            "Free"});
            this.SEcbQuality.FormattingEnabled = true;
            this.SEcbQuality.Items.AddRange(new object[] {
            "240P",
            "480P",
            "720P",
            "1080P",
            "1440P",
            "2140P"});
            this.SEcbQuality.Location = new System.Drawing.Point(90, 178);
            this.SEcbQuality.Name = "SEcbQuality";
            this.SEcbQuality.Size = new System.Drawing.Size(235, 21);
            this.SEcbQuality.TabIndex = 70;
            this.SEcbQuality.Text = "1080P";
            // 
            // SEtbSeason
            // 
            this.SEtbSeason.Location = new System.Drawing.Point(245, 122);
            this.SEtbSeason.Name = "SEtbSeason";
            this.SEtbSeason.Size = new System.Drawing.Size(80, 20);
            this.SEtbSeason.TabIndex = 69;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(356, 231);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(48, 13);
            this.label54.TabIndex = 64;
            this.label54.Text = "Custom1";
            // 
            // SEtbCustom1
            // 
            this.SEtbCustom1.Location = new System.Drawing.Point(442, 228);
            this.SEtbCustom1.Name = "SEtbCustom1";
            this.SEtbCustom1.Size = new System.Drawing.Size(235, 20);
            this.SEtbCustom1.TabIndex = 63;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(25, 208);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(40, 13);
            this.label55.TabIndex = 62;
            this.label55.Text = "Lenght";
            // 
            // SEtbLenght
            // 
            this.SEtbLenght.Location = new System.Drawing.Point(90, 205);
            this.SEtbLenght.Name = "SEtbLenght";
            this.SEtbLenght.Size = new System.Drawing.Size(235, 20);
            this.SEtbLenght.TabIndex = 61;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(356, 205);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(42, 13);
            this.label52.TabIndex = 60;
            this.label52.Text = "Custom";
            // 
            // SEtbCustom
            // 
            this.SEtbCustom.Location = new System.Drawing.Point(442, 202);
            this.SEtbCustom.Name = "SEtbCustom";
            this.SEtbCustom.Size = new System.Drawing.Size(235, 20);
            this.SEtbCustom.TabIndex = 59;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(25, 182);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(39, 13);
            this.label53.TabIndex = 58;
            this.label53.Text = "Quality";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(298, 26);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(129, 13);
            this.label37.TabIndex = 56;
            this.label37.Text = "PictureDownloadLoaction";
            // 
            // SEtbPictureLocation
            // 
            this.SEtbPictureLocation.Location = new System.Drawing.Point(433, 23);
            this.SEtbPictureLocation.Name = "SEtbPictureLocation";
            this.SEtbPictureLocation.Size = new System.Drawing.Size(235, 20);
            this.SEtbPictureLocation.TabIndex = 55;
            this.SEtbPictureLocation.MouseEnter += new System.EventHandler(this.tbPictureLocation_MouseEnter);
            // 
            // SEbtnGetPicture
            // 
            this.SEbtnGetPicture.Location = new System.Drawing.Point(200, 260);
            this.SEbtnGetPicture.Name = "SEbtnGetPicture";
            this.SEbtnGetPicture.Size = new System.Drawing.Size(108, 23);
            this.SEbtnGetPicture.TabIndex = 54;
            this.SEbtnGetPicture.Text = "Select Picture";
            this.SEbtnGetPicture.UseVisualStyleBackColor = true;
            this.SEbtnGetPicture.Click += new System.EventHandler(this.SEbtnGetPicture_Click);
            // 
            // SEbtnCancel
            // 
            this.SEbtnCancel.Location = new System.Drawing.Point(119, 260);
            this.SEbtnCancel.Name = "SEbtnCancel";
            this.SEbtnCancel.Size = new System.Drawing.Size(75, 23);
            this.SEbtnCancel.TabIndex = 53;
            this.SEbtnCancel.Text = "Cancel";
            this.SEbtnCancel.UseVisualStyleBackColor = true;
            this.SEbtnCancel.Click += new System.EventHandler(this.Cancel_Button_Clicked);
            // 
            // SEbtnDone
            // 
            this.SEbtnDone.Location = new System.Drawing.Point(38, 260);
            this.SEbtnDone.Name = "SEbtnDone";
            this.SEbtnDone.Size = new System.Drawing.Size(75, 23);
            this.SEbtnDone.TabIndex = 52;
            this.SEbtnDone.Text = "Done";
            this.SEbtnDone.UseVisualStyleBackColor = true;
            this.SEbtnDone.Click += new System.EventHandler(this.SEbtnDone_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(42, 26);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(34, 13);
            this.label38.TabIndex = 51;
            this.label38.Text = "Table";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(356, 178);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(84, 13);
            this.label39.TabIndex = 49;
            this.label39.Text = "Nextseasondate";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(356, 152);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(76, 13);
            this.label40.TabIndex = 48;
            this.label40.Text = "CheakNewlink";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(356, 126);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(55, 13);
            this.label41.TabIndex = 47;
            this.label41.Text = "Language";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(356, 99);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(42, 13);
            this.label42.TabIndex = 46;
            this.label42.Text = "Genre1";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(356, 73);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(36, 13);
            this.label43.TabIndex = 45;
            this.label43.Text = "Genre";
            // 
            // SEtbCheckNewLink
            // 
            this.SEtbCheckNewLink.Location = new System.Drawing.Point(442, 149);
            this.SEtbCheckNewLink.Name = "SEtbCheckNewLink";
            this.SEtbCheckNewLink.Size = new System.Drawing.Size(235, 20);
            this.SEtbCheckNewLink.TabIndex = 43;
            // 
            // SEtbLanguage
            // 
            this.SEtbLanguage.Location = new System.Drawing.Point(442, 123);
            this.SEtbLanguage.Name = "SEtbLanguage";
            this.SEtbLanguage.Size = new System.Drawing.Size(235, 20);
            this.SEtbLanguage.TabIndex = 42;
            // 
            // SEtbGenre
            // 
            this.SEtbGenre.Location = new System.Drawing.Point(442, 70);
            this.SEtbGenre.Name = "SEtbGenre";
            this.SEtbGenre.Size = new System.Drawing.Size(235, 20);
            this.SEtbGenre.TabIndex = 41;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(32, 155);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(27, 13);
            this.label44.TabIndex = 40;
            this.label44.Text = "Size";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(185, 125);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(43, 13);
            this.label45.TabIndex = 39;
            this.label45.Text = "Season";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(32, 126);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(45, 13);
            this.label46.TabIndex = 38;
            this.label46.Text = "Episode";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(32, 99);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(37, 13);
            this.label47.TabIndex = 37;
            this.label47.Text = "Studio";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(32, 73);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(35, 13);
            this.label48.TabIndex = 36;
            this.label48.Text = "Name";
            // 
            // SEcbSizeMetric
            // 
            this.SEcbSizeMetric.AutoCompleteCustomSource.AddRange(new string[] {
            "KB",
            "MB",
            "GB",
            "TB"});
            this.SEcbSizeMetric.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SEcbSizeMetric.Enabled = false;
            this.SEcbSizeMetric.FormattingEnabled = true;
            this.SEcbSizeMetric.Items.AddRange(new object[] {
            "KB",
            "MB",
            "GB",
            "TB"});
            this.SEcbSizeMetric.Location = new System.Drawing.Point(260, 152);
            this.SEcbSizeMetric.Name = "SEcbSizeMetric";
            this.SEcbSizeMetric.Size = new System.Drawing.Size(65, 21);
            this.SEcbSizeMetric.TabIndex = 35;
            // 
            // SEcbTable
            // 
            this.SEcbTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SEcbTable.FormattingEnabled = true;
            this.SEcbTable.Location = new System.Drawing.Point(100, 23);
            this.SEcbTable.Name = "SEcbTable";
            this.SEcbTable.Size = new System.Drawing.Size(164, 21);
            this.SEcbTable.TabIndex = 34;
            // 
            // SEtbSize
            // 
            this.SEtbSize.Enabled = false;
            this.SEtbSize.Location = new System.Drawing.Point(90, 152);
            this.SEtbSize.Name = "SEtbSize";
            this.SEtbSize.Size = new System.Drawing.Size(164, 20);
            this.SEtbSize.TabIndex = 33;
            // 
            // SEtbEpisode
            // 
            this.SEtbEpisode.Location = new System.Drawing.Point(90, 123);
            this.SEtbEpisode.Name = "SEtbEpisode";
            this.SEtbEpisode.Size = new System.Drawing.Size(80, 20);
            this.SEtbEpisode.TabIndex = 31;
            // 
            // SEtbSutdio
            // 
            this.SEtbSutdio.Location = new System.Drawing.Point(90, 96);
            this.SEtbSutdio.Name = "SEtbSutdio";
            this.SEtbSutdio.Size = new System.Drawing.Size(235, 20);
            this.SEtbSutdio.TabIndex = 30;
            // 
            // SEtbName
            // 
            this.SEtbName.Location = new System.Drawing.Point(90, 70);
            this.SEtbName.Name = "SEtbName";
            this.SEtbName.Size = new System.Drawing.Size(235, 20);
            this.SEtbName.TabIndex = 29;
            this.SEtbName.TextChanged += new System.EventHandler(this.SEtbName_TextChanged);
            // 
            // Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 369);
            this.Controls.Add(this.tabControl1);
            this.Name = "Add";
            this.Text = "Add";
            this.Load += new System.EventHandler(this.Add_Load);
            this.tabControl1.ResumeLayout(false);
            this.tpSoftware.ResumeLayout(false);
            this.tpSoftware.PerformLayout();
            this.tpGames.ResumeLayout(false);
            this.tpGames.PerformLayout();
            this.tpMovies.ResumeLayout(false);
            this.tpMovies.PerformLayout();
            this.tpSeries.ResumeLayout(false);
            this.tpSeries.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpSoftware;
        private System.Windows.Forms.TabPage tpGames;
        private System.Windows.Forms.TabPage tpMovies;
        private System.Windows.Forms.TabPage tpSeries;
        private System.Windows.Forms.TextBox StbSize;
        private System.Windows.Forms.TextBox StbVersion;
        private System.Windows.Forms.TextBox StbPublisher;
        private System.Windows.Forms.TextBox StbDeveloper;
        private System.Windows.Forms.TextBox StbName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ScbSizeMetric;
        private System.Windows.Forms.ComboBox ScbTable;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox StbCustom1;
        private System.Windows.Forms.TextBox StbCustom;
        private System.Windows.Forms.TextBox StbTorrentLinkSearch;
        private System.Windows.Forms.TextBox StbSubCategory;
        private System.Windows.Forms.ComboBox ScbCrackType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox StbPictureLocation;
        private System.Windows.Forms.Button SbtnGetPicture;
        private System.Windows.Forms.Button SbtnCancel;
        private System.Windows.Forms.Button SbtnDone;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox GtbPictureLocation;
        private System.Windows.Forms.Button GbtnGetMovePicture;
        private System.Windows.Forms.Button GbtnCancel;
        private System.Windows.Forms.Button GbtnDone;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox GcbCrackType;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox GtbCustom1;
        private System.Windows.Forms.TextBox GtbCustom;
        private System.Windows.Forms.TextBox GtbTorrentWebLink;
        private System.Windows.Forms.TextBox GtbGenre;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox GcbSizeMetric;
        private System.Windows.Forms.ComboBox GcbTable;
        private System.Windows.Forms.TextBox GtbSize;
        private System.Windows.Forms.TextBox GtbVersion;
        private System.Windows.Forms.TextBox GtbPublisher;
        private System.Windows.Forms.TextBox GtbDeveloper;
        private System.Windows.Forms.TextBox GtbName;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox MtbPictureLocation;
        private System.Windows.Forms.Button MbtnGetPicture;
        private System.Windows.Forms.Button MbtnCancel;
        private System.Windows.Forms.Button MbtnDone;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox MtbLanguage1;
        private System.Windows.Forms.TextBox MtbLanuage;
        private System.Windows.Forms.TextBox MtbGenre2;
        private System.Windows.Forms.TextBox MtbGenre;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox McbSizeMetric;
        private System.Windows.Forms.ComboBox McbTable;
        private System.Windows.Forms.TextBox MtbSize;
        private System.Windows.Forms.TextBox MtbSubtitles;
        private System.Windows.Forms.TextBox MtbLenght;
        private System.Windows.Forms.TextBox MtbStudio;
        private System.Windows.Forms.TextBox MtbName;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox SEtbPictureLocation;
        private System.Windows.Forms.Button SEbtnGetPicture;
        private System.Windows.Forms.Button SEbtnCancel;
        private System.Windows.Forms.Button SEbtnDone;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox SEtbCheckNewLink;
        private System.Windows.Forms.TextBox SEtbLanguage;
        private System.Windows.Forms.TextBox SEtbGenre;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.ComboBox SEcbSizeMetric;
        private System.Windows.Forms.ComboBox SEcbTable;
        private System.Windows.Forms.TextBox SEtbSize;
        private System.Windows.Forms.TextBox SEtbEpisode;
        private System.Windows.Forms.TextBox SEtbSutdio;
        private System.Windows.Forms.TextBox SEtbName;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox MtbCustom;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox MtbSubtitles1;
        private System.Windows.Forms.TextBox MtbGenre1;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox MtbCustom1;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox SEtbCustom1;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox SEtbLenght;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox SEtbCustom;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox SEtbSeason;
        private System.Windows.Forms.ComboBox SEcbQuality;
        private System.Windows.Forms.TextBox SetbGenre1;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox StbLocation;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox GtbLocation;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox MtbLocation;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox SEtbLocation;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.RadioButton srbInternet;
        private System.Windows.Forms.RadioButton srbLocal;
        private System.Windows.Forms.RadioButton GrbInternet;
        private System.Windows.Forms.RadioButton GrbLocal;
        private System.Windows.Forms.RadioButton MrbInternet;
        private System.Windows.Forms.RadioButton MrbLocal;
        private System.Windows.Forms.RadioButton SErbInternet;
        private System.Windows.Forms.RadioButton SErbLocal;
        private System.Windows.Forms.DateTimePicker SEdtpNextSeasonDate;
    }
}