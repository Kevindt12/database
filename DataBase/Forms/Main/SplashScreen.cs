﻿using System;
using System.Windows.Forms;
using System.Threading;

using DataBase.Source;
using DataBase.Source.Settings;

namespace DataBase.Forms.Main
{
    public partial class SplashScreen : Form
    {
        private static Thread _splashthread;
        private static SplashScreen _Splashcreen;

        private int _counter = 0;
        private int _reset = 5;


        // Constuctor
        public SplashScreen()
        {
            InitializeComponent();

            timer1.Start();
            timer1.Interval = 150;

            // Debug
            Program.LOG("Starting the splash Screen");
            
        }
        
        // Starting the splash screen
        public static void RunSplash()
        {
            if (_splashthread == null)
            {
                // show the form in a new thread
                _splashthread = new Thread(new ThreadStart(MakeFrom));
                _splashthread.IsBackground = true;
                _splashthread.Start();
            }
            
        }


        // This will make the splash screen
        public static void MakeFrom()
        {
            if (_Splashcreen == null)
            {
                _Splashcreen = new SplashScreen();
            }

            Application.Run(_Splashcreen);
        }


        // This closes the thered and the splash screen
        public static void CloseThread()
        {
            if (_Splashcreen.InvokeRequired)
                _Splashcreen.Invoke(new MethodInvoker(CloseThread));

            else
                Application.ExitThread();
        }


        // Anti Freez watcher
        private void timer1_Tick(object sender, EventArgs e)
        {
            string dot = ".";

            // If the counter is less then the reset value
            if (_counter <= _reset)
            {
                // Add a dot to the and of the string
                label2.Text = label2.Text + dot;
                _counter++;
            }
            else
            {
                // Reset the string and the counter and start over
                label2.Text = "Loading";
                _counter = 0;
            }

            

        }
    }
}
