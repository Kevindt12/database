﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Web;

using DataBase.Source.Settings;
using DataBase.Source.Data;
using DataBase.Source;
using System.Net;

namespace DataBase.Forms.DataManegment

{
    public partial class AddPicture : Form
    {
        private string _sourcePicture;
        private string _NewName;

        private int _RowIndex;
        private string _TableName;
        private int _TabIndex;

        private SqlGeneral.Games SQLGames = new SqlGeneral.Games();
        private SqlGeneral.Movies SQLMovies = new SqlGeneral.Movies();
        private SqlGeneral.Series SQLSeries = new SqlGeneral.Series();
        private SqlGeneral.Software SQLSoftware = new SqlGeneral.Software();

        // Constuctor
        public AddPicture(int RowIndex, string TableName, int TabIndex)
        {
            InitializeComponent();

            btnCancel.DialogResult = DialogResult.Cancel;

            rbImage.Checked = true;

            _RowIndex = RowIndex;
            _TableName = TableName;
            _TabIndex = TabIndex;
        }


        // Get picture - Gets the Picture From a Local source
        private void btnGetPicture_Click(object sender, EventArgs e)
        {
            OpenFileDialog openfile = new OpenFileDialog();


            if (openfile.ShowDialog() == DialogResult.OK)
            {
                // setting the location of the file
                _sourcePicture = openfile.FileName;

                // Loading the image
                using (FileStream stream = new FileStream(_sourcePicture, FileMode.Open, FileAccess.Read))
                {
                    pictureBox1.Image = Image.FromStream(stream);
                    stream.Dispose();
                }
                textBox1.Text = _sourcePicture;
            }
        }



        private void btnDone_Click(object sender, EventArgs e)
        {
            // Checking if there is text and if file exists
            if (!String.IsNullOrEmpty(_sourcePicture) && File.Exists(_sourcePicture))
            {
                // Getting the extention
                string Extention = Path.GetExtension(_sourcePicture);
                string FolderLocation;

                // getting the new name for the picture
                GeneralSettings.GetPictureName();

                // Setting the folder where pics got to go
                FolderLocation = Environment.CurrentDirectory + "\\Pictures\\";

                // Getting the picture name
                _NewName = FolderLocation + GeneralSettings.GetPictureName() + Extention;

                // Disposeing the picture
                pictureBox1.Image.Dispose();
                pictureBox1.Image = null;

                // Moveing the new picutre to the right place
                File.Move(_sourcePicture, _NewName);

                if (File.Exists(_NewName))
                {
                    switch (_TabIndex)
                    {
                        case 0:
                            SQLSoftware.UpdatePictureLocation(_TableName, _RowIndex, _NewName);
                            break;
                        case 1:
                            SQLGames.UpdatePictureLocation(_TableName, _RowIndex, _NewName);
                            break;
                        case 2:
                            SQLMovies.UpdatePictureLocation(_TableName, _RowIndex, _NewName);
                            break;
                        case 3:
                            SQLSeries.UpdatePictureLocation(_TableName, _RowIndex, _NewName);
                            break;
                    }
                }
            }
        }




        // If the text has changed then change the dialog result
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text != null || textBox1.Text != "")
            {
                btnDone.DialogResult = DialogResult.OK;
            }
        }


        // Download picture - Download the picutre form a online source
        private void button1_Click(object sender, EventArgs e)
        {
            string newPictureLocation;
            string downloadLocationForPicture = "";

            // Empty the picture in the picture box
            if (pictureBox1.Image != null)
                pictureBox1.Image.Dispose();

            pictureBox1.Image = null;


            // Check where it sould get the picture from
            if (rbImage.Checked == true)
            {
                // Setting the download location to de defualt of the textbox
                downloadLocationForPicture = textBox1.Text;
            }
            else if (rbImdb.Checked == true)
            {
                // Checking if i sould get for movies or series
                if (_TabIndex == 2)
                {
                    // Getting the picture location from the webiste
                    downloadLocationForPicture = new IMDB.GetMovieData(textBox1.Text).GetData().PictureLocation;
                }
                else if (_TabIndex == 3)
                {
                    // Getting the picture from the website
                    downloadLocationForPicture = new IMDB.GetSeriesData(textBox1.Text).GetData().PictureLocation;
                }
                else
                {
                    // That means your trying to get the picture while your in software mode what is illigle
                    MessageBox.Show("You cant get a picture form imdb ");
                }
            }
            else if (rbMetacritic.Checked == true)
            {
                // Gets the picture location
                downloadLocationForPicture = new Metacridic(textBox1.Text).GetData().PictureLocation;
            }
            else
            {
                // else there was no radio button chacked and that is a faital error.
                throw new InvalidOperationException("There was a error");
            }

            // Get and downloiad the picture and save it
            newPictureLocation = DownloadAndRenamePictureFromInternet(downloadLocationForPicture);

            // Loading the image in the picture box
            pictureBox1.Image = new Bitmap(newPictureLocation);

            // And setting the Source picture varible
            _sourcePicture = newPictureLocation;
        }



        private string DownloadAndRenamePictureFromInternet(string url)
        {

            WebClient client;
            Uri uri = new Uri(url);
            Random random = new Random();
            string folderDestenation;
            string pictureFinalPath;
            string extention;


            // Getting the extention
            extention = Path.GetExtension(url);

            // Getting the location of were the pictcture is gowing to be downloaded to
            folderDestenation = Environment.CurrentDirectory + "\\Temp\\";

            // Makeing the full Location string
            pictureFinalPath = folderDestenation + random.Next().ToString() + extention;

            // Downloadng and moveing
            using (client = new WebClient())
            {
                // Checking if the Directory exist if it doesn't than create one.
                if (Directory.Exists(folderDestenation))
                {
                    // Download the picture and save it
                    client.DownloadFile(uri, pictureFinalPath);
                }
                else
                {
                    // Create a directory
                    Directory.CreateDirectory(folderDestenation);

                    // Download the picture and save it
                    client.DownloadFile(uri, pictureFinalPath);
                }
            }
            return pictureFinalPath;
        }
    }
}
