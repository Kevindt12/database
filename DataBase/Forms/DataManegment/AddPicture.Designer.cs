﻿namespace DataBase.Forms.DataManegment
{
    partial class AddPicture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnDone = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnGetPicture = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.rbImage = new System.Windows.Forms.RadioButton();
            this.rbImdb = new System.Windows.Forms.RadioButton();
            this.rbMetacritic = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 62);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(263, 350);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnDone
            // 
            this.btnDone.Location = new System.Drawing.Point(11, 418);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(63, 23);
            this.btnDone.TabIndex = 1;
            this.btnDone.Text = "Done";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(80, 418);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(11, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(330, 20);
            this.textBox1.TabIndex = 3;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btnGetPicture
            // 
            this.btnGetPicture.Location = new System.Drawing.Point(151, 418);
            this.btnGetPicture.Name = "btnGetPicture";
            this.btnGetPicture.Size = new System.Drawing.Size(75, 23);
            this.btnGetPicture.TabIndex = 4;
            this.btnGetPicture.Text = "Get Picture";
            this.btnGetPicture.UseVisualStyleBackColor = true;
            this.btnGetPicture.Click += new System.EventHandler(this.btnGetPicture_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(232, 418);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(109, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Download Picture";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // rbImage
            // 
            this.rbImage.AutoSize = true;
            this.rbImage.Location = new System.Drawing.Point(32, 38);
            this.rbImage.Name = "rbImage";
            this.rbImage.Size = new System.Drawing.Size(54, 17);
            this.rbImage.TabIndex = 6;
            this.rbImage.TabStop = true;
            this.rbImage.Text = "Image";
            this.rbImage.UseVisualStyleBackColor = true;
            // 
            // rbImdb
            // 
            this.rbImdb.AutoSize = true;
            this.rbImdb.Location = new System.Drawing.Point(118, 39);
            this.rbImdb.Name = "rbImdb";
            this.rbImdb.Size = new System.Drawing.Size(48, 17);
            this.rbImdb.TabIndex = 7;
            this.rbImdb.TabStop = true;
            this.rbImdb.Text = "Imdb";
            this.rbImdb.UseVisualStyleBackColor = true;
            // 
            // rbMetacritic
            // 
            this.rbMetacritic.AutoSize = true;
            this.rbMetacritic.Location = new System.Drawing.Point(190, 38);
            this.rbMetacritic.Name = "rbMetacritic";
            this.rbMetacritic.Size = new System.Drawing.Size(71, 17);
            this.rbMetacritic.TabIndex = 8;
            this.rbMetacritic.TabStop = true;
            this.rbMetacritic.Text = "Metacritic";
            this.rbMetacritic.UseVisualStyleBackColor = true;
            // 
            // AddPicture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 450);
            this.Controls.Add(this.rbMetacritic);
            this.Controls.Add(this.rbImdb);
            this.Controls.Add(this.rbImage);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnGetPicture);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.pictureBox1);
            this.Name = "AddPicture";
            this.Text = "AddPicture";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnGetPicture;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton rbImage;
        private System.Windows.Forms.RadioButton rbImdb;
        private System.Windows.Forms.RadioButton rbMetacritic;
    }
}