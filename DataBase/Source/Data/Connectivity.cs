﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace DataBase.Source.Data
{
    public class Connectivity
    {
        private bool _connectionStatus;
        private bool _previousConnectionStatus;


        private Timer _timer;


        // Events
        /// <summary>
        /// When the Internet Connection has chaned
        /// </summary>
        public event EventHandler InternetConnectionChanged;
        
        // Invokers
        protected virtual void OnInternetConnectionChanged()
        {
            if (InternetConnectionChanged != null)
            {
                InternetConnectionChanged?.Invoke(null, EventArgs.Empty);
            }
        }




        /// <summary>
        /// Is there a internet Connetion | This will check if there is also
        /// </summary>
        public bool CurrentInternetConnection
        {
            get
            {
                _connectionStatus = CheckForInternetConnection();
                return _connectionStatus;
            }
            private set
            {
                _connectionStatus = value;
                OnInternetConnectionChanged();
            }
        }


        /// <summary>
        /// Tick in Miliseconcs
        /// </summary>
        public int TimerInterval
        {
            private get
            {
                return (int)_timer.Interval;
            }
            set
            {
                _timer.Interval = value;
            }
        }



        // Constuctors

        /// <summary>
        /// This class will check for internet or check for interent automacily
        /// </summary>
        public Connectivity()
        {
            // Makeing a new timer
            _timer = new Timer();

            // Setting the Timer Tick Interval
            _timer.Interval = 10000;
            _timer.Tick += CheckingInternetConnectionTick;
        }

        /// <summary>
        /// This class will check for internet or check for interent automacily
        /// </summary>
        /// <param name="timer">A timer class</param>
        public Connectivity(Timer timer)
        {
            // Setting the timer to the input timer
            _timer = timer;

            // If timer is under 5 seconds that means most proboly there was no interval so set a new interval of 10 sec
            if (timer.Interval <= 5)
            {
                _timer.Interval = 10000;
            }

            _timer.Tick += CheckingInternetConnectionTick;
        }



        // Timer Methods Copy

        /// <summary>
        /// Starts the tick for checking the internet
        /// </summary>
        public void TimerStart()
        {
            _timer.Start();
        }

        /// <summary>
        /// Stops the Tick for checking the internet
        /// </summary>
        public void TimerStop()
        {
            _timer.Stop();
        }


        private bool TestingWebsite(string uri)
        {
            bool testResult = false;

            try
            {
                // Opeing the webiste
                using (WebClient client = new WebClient())
                using (Stream stream = client.OpenRead(uri))
                {
                    // If it was able to get the website then retrun a true
                    stream.Dispose();
                    client.Dispose();
                    testResult = true;
                }
            }
            // If it could not get the website then return a false
            catch (WebException)
            {
                testResult = false;
            }
            // If there was a other error then rethrow that exeption
            catch (Exception ex)
            {
                throw ex;
            }


            return testResult;
        }


        /// <summary>
        /// Checks for a internet connection
        /// </summary>
        /// <returns>Connection Status</returns>
        public bool CheckForInternetConnection()
        {
            const string firstWebsite = "https://www.google.nl";
            const string secondWebsite = "https://www.FaceBook.nl";
            const string thirdWebsite = "https://www.imdb.nl";


            // First Test | Testing Google.com
            if (TestingWebsite(firstWebsite))
            {
                return true;
            }
            // Second Test | Tesing Facebook.com
            else if (TestingWebsite(secondWebsite))
            {
                return true;
            }
            // Third Test | Tesing imdb.com
            else if (TestingWebsite(thirdWebsite))
            {
                return true;
            }
            // If all those did not work then retun false. that means there is no internet.
            else
            {
                return false;
            }
        }


        // Every Time the timer ticks
        private void CheckingInternetConnectionTick(object sender, EventArgs e)
        {
            // LOG
            Console.WriteLine("Checking For internet connection \n");

            // If the previoes internet connection status does not match the current internet status then uodate the interent status
            if (CurrentInternetConnection != _previousConnectionStatus)
            {
                CurrentInternetConnection = CheckForInternetConnection();
            }

            // Setting so when we run this again this is the old varible
            _previousConnectionStatus = CheckForInternetConnection();

        }

    }
}
