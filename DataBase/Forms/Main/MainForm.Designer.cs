﻿using DataBase.DataBases.Extras;
using DataBase.Forms;

namespace DataBase.Forms.Main
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.programToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recheckInternetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataBaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addFromSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangePictureToolStripitem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tableToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.combineToSeasonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.InternetConectivery = new System.Windows.Forms.ToolStripStatusLabel();
            this.MainTC = new System.Windows.Forms.TabControl();
            this.TPSoftware = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SoftwareDataGridView = new System.Windows.Forms.DataGridView();
            this.ScbSelectTable = new System.Windows.Forms.ComboBox();
            this.StbSearch = new System.Windows.Forms.TextBox();
            this.SpbPicture = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SlbCustom = new System.Windows.Forms.Label();
            this.SlbTorrentWebSiteSearch = new System.Windows.Forms.Label();
            this.SlbSize = new System.Windows.Forms.Label();
            this.SlbSubCategory = new System.Windows.Forms.Label();
            this.SlbVersion = new System.Windows.Forms.Label();
            this.SlbPublisher = new System.Windows.Forms.Label();
            this.SlbDeveloper = new System.Windows.Forms.Label();
            this.SlbName = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TPGames = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.GamesDataGridView = new System.Windows.Forms.DataGridView();
            this.GcbSelectedTable = new System.Windows.Forms.ComboBox();
            this.GtbSearch = new System.Windows.Forms.TextBox();
            this.GpbPicture = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.GlbCustom = new System.Windows.Forms.Label();
            this.GlbTorrentWebsiteSearch = new System.Windows.Forms.Label();
            this.GlbSize = new System.Windows.Forms.Label();
            this.GlbGenre = new System.Windows.Forms.Label();
            this.GlbVersion = new System.Windows.Forms.Label();
            this.GlbPublisher = new System.Windows.Forms.Label();
            this.GlbDeveloper = new System.Windows.Forms.Label();
            this.GlbName = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.TPMovies = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.MoviesDataGridView = new System.Windows.Forms.DataGridView();
            this.McbSelectedTable = new System.Windows.Forms.ComboBox();
            this.MtbSearch = new System.Windows.Forms.TextBox();
            this.MpbPicture = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.MlbCustom = new System.Windows.Forms.Label();
            this.MlbSubtitles = new System.Windows.Forms.Label();
            this.MlbLanguage = new System.Windows.Forms.Label();
            this.MlbGenre = new System.Windows.Forms.Label();
            this.MlbLenght = new System.Windows.Forms.Label();
            this.MlbSize = new System.Windows.Forms.Label();
            this.MlbStudio = new System.Windows.Forms.Label();
            this.MlbName = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.TPSeries = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SeriesDataGridView = new System.Windows.Forms.DataGridView();
            this.SEcbSelectedTable = new System.Windows.Forms.ComboBox();
            this.SEtbSearch = new System.Windows.Forms.TextBox();
            this.SEpbPicture = new System.Windows.Forms.PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.SElbCustom = new System.Windows.Forms.Label();
            this.SElbLanguage = new System.Windows.Forms.Label();
            this.SElbNextSeasonDate = new System.Windows.Forms.Label();
            this.SElbQuality = new System.Windows.Forms.Label();
            this.SElbSeason = new System.Windows.Forms.Label();
            this.SElbEpisode = new System.Windows.Forms.Label();
            this.SElbStudio = new System.Windows.Forms.Label();
            this.SElbName = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.testingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.softwareDataSet = new DataBase.DataBases.Extras.SoftwareDataSet();
            this.testingTableAdapter = new DataBase.DataBases.Extras.SoftwareDataSetTableAdapters.TestingTableAdapter();
            this.timerTick = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.MainTC.SuspendLayout();
            this.TPSoftware.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SoftwareDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpbPicture)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.TPGames.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GamesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GpbPicture)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.TPMovies.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MoviesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MpbPicture)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.TPSeries.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SeriesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SEpbPicture)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.softwareDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.programToolStripMenuItem,
            this.dataBaseToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1005, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // programToolStripMenuItem
            // 
            this.programToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.testToolStripMenuItem,
            this.recheckInternetToolStripMenuItem});
            this.programToolStripMenuItem.Name = "programToolStripMenuItem";
            this.programToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.programToolStripMenuItem.Text = "Program";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.testToolStripMenuItem.Text = "Get Data From Interent";
            this.testToolStripMenuItem.Click += new System.EventHandler(this.GetDataFromInterentToolStripMenuItem_Click);
            // 
            // recheckInternetToolStripMenuItem
            // 
            this.recheckInternetToolStripMenuItem.Name = "recheckInternetToolStripMenuItem";
            this.recheckInternetToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.recheckInternetToolStripMenuItem.Text = "Recheck Internet";
            this.recheckInternetToolStripMenuItem.Click += new System.EventHandler(this.recheckInternetToolStripMenuItem_Click);
            // 
            // dataBaseToolStripMenuItem
            // 
            this.dataBaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addItemToolStripMenuItem,
            this.editItemToolStripMenuItem,
            this.removeItemToolStripMenuItem});
            this.dataBaseToolStripMenuItem.Name = "dataBaseToolStripMenuItem";
            this.dataBaseToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.dataBaseToolStripMenuItem.Text = "DataBase";
            // 
            // addItemToolStripMenuItem
            // 
            this.addItemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemToolStripMenuItem,
            this.tableToolStripMenuItem,
            this.pictureToolStripMenuItem,
            this.addFromSelectedToolStripMenuItem});
            this.addItemToolStripMenuItem.Name = "addItemToolStripMenuItem";
            this.addItemToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.addItemToolStripMenuItem.Text = "Add";
            // 
            // itemToolStripMenuItem
            // 
            this.itemToolStripMenuItem.Name = "itemToolStripMenuItem";
            this.itemToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.itemToolStripMenuItem.Text = "Item";
            this.itemToolStripMenuItem.Click += new System.EventHandler(this.addItemToolStripMenuItem_Click);
            // 
            // tableToolStripMenuItem
            // 
            this.tableToolStripMenuItem.Name = "tableToolStripMenuItem";
            this.tableToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.tableToolStripMenuItem.Text = "Table";
            this.tableToolStripMenuItem.Click += new System.EventHandler(this.tableToolStripMenuItem_Click);
            // 
            // pictureToolStripMenuItem
            // 
            this.pictureToolStripMenuItem.Name = "pictureToolStripMenuItem";
            this.pictureToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.pictureToolStripMenuItem.Text = "Picture";
            this.pictureToolStripMenuItem.Click += new System.EventHandler(this.pictureToolStripMenuItem_Click);
            // 
            // addFromSelectedToolStripMenuItem
            // 
            this.addFromSelectedToolStripMenuItem.Name = "addFromSelectedToolStripMenuItem";
            this.addFromSelectedToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.addFromSelectedToolStripMenuItem.Text = "Add From Selected";
            this.addFromSelectedToolStripMenuItem.Click += new System.EventHandler(this.addFromSelectedToolStripMenuItem_Click);
            // 
            // editItemToolStripMenuItem
            // 
            this.editItemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemToolStripMenuItem1,
            this.ChangePictureToolStripitem});
            this.editItemToolStripMenuItem.Name = "editItemToolStripMenuItem";
            this.editItemToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.editItemToolStripMenuItem.Text = "Edit";
            // 
            // itemToolStripMenuItem1
            // 
            this.itemToolStripMenuItem1.Name = "itemToolStripMenuItem1";
            this.itemToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.itemToolStripMenuItem1.Text = "Item";
            this.itemToolStripMenuItem1.Click += new System.EventHandler(this.editItemToolStripMenuItem_Click);
            // 
            // ChangePictureToolStripitem
            // 
            this.ChangePictureToolStripitem.Name = "ChangePictureToolStripitem";
            this.ChangePictureToolStripitem.Size = new System.Drawing.Size(152, 22);
            this.ChangePictureToolStripitem.Text = "ChangePicture";
            this.ChangePictureToolStripitem.Click += new System.EventHandler(this.changePictureToolStripMenuItem_Click);
            // 
            // removeItemToolStripMenuItem
            // 
            this.removeItemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemToolStripMenuItem2,
            this.tableToolStripMenuItem2});
            this.removeItemToolStripMenuItem.Name = "removeItemToolStripMenuItem";
            this.removeItemToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.removeItemToolStripMenuItem.Text = "Remove";
            // 
            // itemToolStripMenuItem2
            // 
            this.itemToolStripMenuItem2.Name = "itemToolStripMenuItem2";
            this.itemToolStripMenuItem2.Size = new System.Drawing.Size(102, 22);
            this.itemToolStripMenuItem2.Text = "Item";
            this.itemToolStripMenuItem2.Click += new System.EventHandler(this.removeItemToolStripMenuItem_Click);
            // 
            // tableToolStripMenuItem2
            // 
            this.tableToolStripMenuItem2.Name = "tableToolStripMenuItem2";
            this.tableToolStripMenuItem2.Size = new System.Drawing.Size(102, 22);
            this.tableToolStripMenuItem2.Text = "Table";
            this.tableToolStripMenuItem2.Click += new System.EventHandler(this.tableToolStripMenuItem2_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.renameFolderToolStripMenuItem,
            this.combineToSeasonToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // renameFolderToolStripMenuItem
            // 
            this.renameFolderToolStripMenuItem.Name = "renameFolderToolStripMenuItem";
            this.renameFolderToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.renameFolderToolStripMenuItem.Text = "Rename Folder";
            this.renameFolderToolStripMenuItem.Click += new System.EventHandler(this.renameFolderToolStripMenuItem_Click);
            // 
            // combineToSeasonToolStripMenuItem
            // 
            this.combineToSeasonToolStripMenuItem.Name = "combineToSeasonToolStripMenuItem";
            this.combineToSeasonToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.combineToSeasonToolStripMenuItem.Text = "Combine To Season";
            this.combineToSeasonToolStripMenuItem.Click += new System.EventHandler(this.combineToSeasonToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.InternetConectivery});
            this.statusStrip1.Location = new System.Drawing.Point(0, 655);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1005, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // InternetConectivery
            // 
            this.InternetConectivery.Name = "InternetConectivery";
            this.InternetConectivery.Size = new System.Drawing.Size(118, 17);
            this.InternetConectivery.Text = "toolStripStatusLabel1";
            // 
            // MainTC
            // 
            this.MainTC.Controls.Add(this.TPSoftware);
            this.MainTC.Controls.Add(this.TPGames);
            this.MainTC.Controls.Add(this.TPMovies);
            this.MainTC.Controls.Add(this.TPSeries);
            this.MainTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTC.ItemSize = new System.Drawing.Size(3333, 25);
            this.MainTC.Location = new System.Drawing.Point(0, 24);
            this.MainTC.Name = "MainTC";
            this.MainTC.Padding = new System.Drawing.Point(107, 3);
            this.MainTC.SelectedIndex = 0;
            this.MainTC.Size = new System.Drawing.Size(1005, 631);
            this.MainTC.TabIndex = 2;
            this.MainTC.SelectedIndexChanged += new System.EventHandler(this.MainTC_SelectedIndexChanged);
            // 
            // TPSoftware
            // 
            this.TPSoftware.BackColor = System.Drawing.SystemColors.Control;
            this.TPSoftware.Controls.Add(this.label2);
            this.TPSoftware.Controls.Add(this.label1);
            this.TPSoftware.Controls.Add(this.SoftwareDataGridView);
            this.TPSoftware.Controls.Add(this.ScbSelectTable);
            this.TPSoftware.Controls.Add(this.StbSearch);
            this.TPSoftware.Controls.Add(this.SpbPicture);
            this.TPSoftware.Controls.Add(this.groupBox1);
            this.TPSoftware.Location = new System.Drawing.Point(4, 29);
            this.TPSoftware.Name = "TPSoftware";
            this.TPSoftware.Padding = new System.Windows.Forms.Padding(3);
            this.TPSoftware.Size = new System.Drawing.Size(997, 598);
            this.TPSoftware.TabIndex = 0;
            this.TPSoftware.Text = "Software";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(485, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Search";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(232, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Table";
            // 
            // SoftwareDataGridView
            // 
            this.SoftwareDataGridView.AllowUserToAddRows = false;
            this.SoftwareDataGridView.AllowUserToDeleteRows = false;
            this.SoftwareDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SoftwareDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SoftwareDataGridView.Location = new System.Drawing.Point(237, 35);
            this.SoftwareDataGridView.MultiSelect = false;
            this.SoftwareDataGridView.Name = "SoftwareDataGridView";
            this.SoftwareDataGridView.ReadOnly = true;
            this.SoftwareDataGridView.Size = new System.Drawing.Size(757, 560);
            this.SoftwareDataGridView.TabIndex = 4;
            this.SoftwareDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.SoftwareDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SoftwareDataGridView_CellDoubleClick);
            // 
            // ScbSelectTable
            // 
            this.ScbSelectTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ScbSelectTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ScbSelectTable.FormattingEnabled = true;
            this.ScbSelectTable.Location = new System.Drawing.Point(272, 8);
            this.ScbSelectTable.Name = "ScbSelectTable";
            this.ScbSelectTable.Size = new System.Drawing.Size(207, 21);
            this.ScbSelectTable.TabIndex = 3;
            this.ScbSelectTable.SelectedIndexChanged += new System.EventHandler(this.Mcb1_SelectedIndexChanged);
            // 
            // StbSearch
            // 
            this.StbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StbSearch.Location = new System.Drawing.Point(532, 8);
            this.StbSearch.Name = "StbSearch";
            this.StbSearch.Size = new System.Drawing.Size(457, 20);
            this.StbSearch.TabIndex = 2;
            this.StbSearch.TextChanged += new System.EventHandler(this.StbSearch_TextChanged);
            // 
            // SpbPicture
            // 
            this.SpbPicture.Location = new System.Drawing.Point(8, 6);
            this.SpbPicture.Name = "SpbPicture";
            this.SpbPicture.Size = new System.Drawing.Size(218, 322);
            this.SpbPicture.TabIndex = 1;
            this.SpbPicture.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.SlbCustom);
            this.groupBox1.Controls.Add(this.SlbTorrentWebSiteSearch);
            this.groupBox1.Controls.Add(this.SlbSize);
            this.groupBox1.Controls.Add(this.SlbSubCategory);
            this.groupBox1.Controls.Add(this.SlbVersion);
            this.groupBox1.Controls.Add(this.SlbPublisher);
            this.groupBox1.Controls.Add(this.SlbDeveloper);
            this.groupBox1.Controls.Add(this.SlbName);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(8, 334);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(218, 252);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Details";
            // 
            // SlbCustom
            // 
            this.SlbCustom.AutoSize = true;
            this.SlbCustom.Location = new System.Drawing.Point(138, 213);
            this.SlbCustom.Name = "SlbCustom";
            this.SlbCustom.Size = new System.Drawing.Size(41, 13);
            this.SlbCustom.TabIndex = 23;
            this.SlbCustom.Text = "label17";
            // 
            // SlbTorrentWebSiteSearch
            // 
            this.SlbTorrentWebSiteSearch.AutoSize = true;
            this.SlbTorrentWebSiteSearch.Location = new System.Drawing.Point(138, 187);
            this.SlbTorrentWebSiteSearch.Name = "SlbTorrentWebSiteSearch";
            this.SlbTorrentWebSiteSearch.Size = new System.Drawing.Size(41, 13);
            this.SlbTorrentWebSiteSearch.TabIndex = 22;
            this.SlbTorrentWebSiteSearch.Text = "label18";
            // 
            // SlbSize
            // 
            this.SlbSize.AutoSize = true;
            this.SlbSize.Location = new System.Drawing.Point(138, 161);
            this.SlbSize.Name = "SlbSize";
            this.SlbSize.Size = new System.Drawing.Size(41, 13);
            this.SlbSize.TabIndex = 21;
            this.SlbSize.Text = "label19";
            // 
            // SlbSubCategory
            // 
            this.SlbSubCategory.AutoSize = true;
            this.SlbSubCategory.Location = new System.Drawing.Point(138, 135);
            this.SlbSubCategory.Name = "SlbSubCategory";
            this.SlbSubCategory.Size = new System.Drawing.Size(41, 13);
            this.SlbSubCategory.TabIndex = 20;
            this.SlbSubCategory.Text = "label20";
            // 
            // SlbVersion
            // 
            this.SlbVersion.AutoSize = true;
            this.SlbVersion.Location = new System.Drawing.Point(138, 109);
            this.SlbVersion.Name = "SlbVersion";
            this.SlbVersion.Size = new System.Drawing.Size(41, 13);
            this.SlbVersion.TabIndex = 19;
            this.SlbVersion.Text = "label21";
            // 
            // SlbPublisher
            // 
            this.SlbPublisher.AutoSize = true;
            this.SlbPublisher.Location = new System.Drawing.Point(138, 83);
            this.SlbPublisher.Name = "SlbPublisher";
            this.SlbPublisher.Size = new System.Drawing.Size(41, 13);
            this.SlbPublisher.TabIndex = 18;
            this.SlbPublisher.Text = "label22";
            // 
            // SlbDeveloper
            // 
            this.SlbDeveloper.AutoSize = true;
            this.SlbDeveloper.Location = new System.Drawing.Point(138, 57);
            this.SlbDeveloper.Name = "SlbDeveloper";
            this.SlbDeveloper.Size = new System.Drawing.Size(41, 13);
            this.SlbDeveloper.TabIndex = 17;
            this.SlbDeveloper.Text = "label23";
            // 
            // SlbName
            // 
            this.SlbName.AutoSize = true;
            this.SlbName.Location = new System.Drawing.Point(138, 31);
            this.SlbName.Name = "SlbName";
            this.SlbName.Size = new System.Drawing.Size(41, 13);
            this.SlbName.TabIndex = 16;
            this.SlbName.Text = "label24";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 213);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 15;
            this.label15.Text = "Custom";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 187);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(120, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "Torrent Website Search";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 161);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "Size";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 135);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "SubCategory";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 109);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 13);
            this.label14.TabIndex = 11;
            this.label14.Text = "Version";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 83);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Publisher";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 57);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Developer";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Name";
            // 
            // TPGames
            // 
            this.TPGames.BackColor = System.Drawing.SystemColors.Control;
            this.TPGames.Controls.Add(this.label3);
            this.TPGames.Controls.Add(this.label4);
            this.TPGames.Controls.Add(this.GamesDataGridView);
            this.TPGames.Controls.Add(this.GcbSelectedTable);
            this.TPGames.Controls.Add(this.GtbSearch);
            this.TPGames.Controls.Add(this.GpbPicture);
            this.TPGames.Controls.Add(this.groupBox2);
            this.TPGames.Location = new System.Drawing.Point(4, 29);
            this.TPGames.Name = "TPGames";
            this.TPGames.Padding = new System.Windows.Forms.Padding(3);
            this.TPGames.Size = new System.Drawing.Size(997, 598);
            this.TPGames.TabIndex = 1;
            this.TPGames.Text = "Games";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(485, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Search";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(232, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Table";
            // 
            // GamesDataGridView
            // 
            this.GamesDataGridView.AllowUserToAddRows = false;
            this.GamesDataGridView.AllowUserToDeleteRows = false;
            this.GamesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GamesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GamesDataGridView.Location = new System.Drawing.Point(232, 33);
            this.GamesDataGridView.MultiSelect = false;
            this.GamesDataGridView.Name = "GamesDataGridView";
            this.GamesDataGridView.ReadOnly = true;
            this.GamesDataGridView.Size = new System.Drawing.Size(757, 553);
            this.GamesDataGridView.TabIndex = 11;
            this.GamesDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GamesDataGridView_CellContentClick);
            this.GamesDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GamesDataGridView_CellDoubleClick);
            // 
            // GcbSelectedTable
            // 
            this.GcbSelectedTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.GcbSelectedTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GcbSelectedTable.FormattingEnabled = true;
            this.GcbSelectedTable.Location = new System.Drawing.Point(272, 8);
            this.GcbSelectedTable.Name = "GcbSelectedTable";
            this.GcbSelectedTable.Size = new System.Drawing.Size(207, 21);
            this.GcbSelectedTable.TabIndex = 10;
            this.GcbSelectedTable.SelectedIndexChanged += new System.EventHandler(this.GcbSelectedTable_SelectedIndexChanged);
            // 
            // GtbSearch
            // 
            this.GtbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GtbSearch.Location = new System.Drawing.Point(532, 8);
            this.GtbSearch.Name = "GtbSearch";
            this.GtbSearch.Size = new System.Drawing.Size(457, 20);
            this.GtbSearch.TabIndex = 9;
            this.GtbSearch.TextChanged += new System.EventHandler(this.GtbSearch_TextChanged);
            // 
            // GpbPicture
            // 
            this.GpbPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GpbPicture.Location = new System.Drawing.Point(8, 6);
            this.GpbPicture.Name = "GpbPicture";
            this.GpbPicture.Size = new System.Drawing.Size(218, 322);
            this.GpbPicture.TabIndex = 8;
            this.GpbPicture.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.GlbCustom);
            this.groupBox2.Controls.Add(this.GlbTorrentWebsiteSearch);
            this.groupBox2.Controls.Add(this.GlbSize);
            this.groupBox2.Controls.Add(this.GlbGenre);
            this.groupBox2.Controls.Add(this.GlbVersion);
            this.groupBox2.Controls.Add(this.GlbPublisher);
            this.groupBox2.Controls.Add(this.GlbDeveloper);
            this.groupBox2.Controls.Add(this.GlbName);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Controls.Add(this.label37);
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Location = new System.Drawing.Point(8, 334);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(218, 252);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Details";
            // 
            // GlbCustom
            // 
            this.GlbCustom.AutoSize = true;
            this.GlbCustom.Location = new System.Drawing.Point(155, 211);
            this.GlbCustom.Name = "GlbCustom";
            this.GlbCustom.Size = new System.Drawing.Size(41, 13);
            this.GlbCustom.TabIndex = 39;
            this.GlbCustom.Text = "label25";
            // 
            // GlbTorrentWebsiteSearch
            // 
            this.GlbTorrentWebsiteSearch.AutoSize = true;
            this.GlbTorrentWebsiteSearch.Location = new System.Drawing.Point(155, 185);
            this.GlbTorrentWebsiteSearch.Name = "GlbTorrentWebsiteSearch";
            this.GlbTorrentWebsiteSearch.Size = new System.Drawing.Size(41, 13);
            this.GlbTorrentWebsiteSearch.TabIndex = 38;
            this.GlbTorrentWebsiteSearch.Text = "label26";
            // 
            // GlbSize
            // 
            this.GlbSize.AutoSize = true;
            this.GlbSize.Location = new System.Drawing.Point(155, 159);
            this.GlbSize.Name = "GlbSize";
            this.GlbSize.Size = new System.Drawing.Size(41, 13);
            this.GlbSize.TabIndex = 37;
            this.GlbSize.Text = "label27";
            // 
            // GlbGenre
            // 
            this.GlbGenre.AutoSize = true;
            this.GlbGenre.Location = new System.Drawing.Point(155, 133);
            this.GlbGenre.Name = "GlbGenre";
            this.GlbGenre.Size = new System.Drawing.Size(41, 13);
            this.GlbGenre.TabIndex = 36;
            this.GlbGenre.Text = "label28";
            // 
            // GlbVersion
            // 
            this.GlbVersion.AutoSize = true;
            this.GlbVersion.Location = new System.Drawing.Point(155, 107);
            this.GlbVersion.Name = "GlbVersion";
            this.GlbVersion.Size = new System.Drawing.Size(41, 13);
            this.GlbVersion.TabIndex = 35;
            this.GlbVersion.Text = "label29";
            // 
            // GlbPublisher
            // 
            this.GlbPublisher.AutoSize = true;
            this.GlbPublisher.Location = new System.Drawing.Point(155, 81);
            this.GlbPublisher.Name = "GlbPublisher";
            this.GlbPublisher.Size = new System.Drawing.Size(41, 13);
            this.GlbPublisher.TabIndex = 34;
            this.GlbPublisher.Text = "label30";
            // 
            // GlbDeveloper
            // 
            this.GlbDeveloper.AutoSize = true;
            this.GlbDeveloper.Location = new System.Drawing.Point(155, 55);
            this.GlbDeveloper.Name = "GlbDeveloper";
            this.GlbDeveloper.Size = new System.Drawing.Size(41, 13);
            this.GlbDeveloper.TabIndex = 33;
            this.GlbDeveloper.Text = "label31";
            // 
            // GlbName
            // 
            this.GlbName.AutoSize = true;
            this.GlbName.Location = new System.Drawing.Point(155, 29);
            this.GlbName.Name = "GlbName";
            this.GlbName.Size = new System.Drawing.Size(41, 13);
            this.GlbName.TabIndex = 32;
            this.GlbName.Text = "label32";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(23, 211);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(42, 13);
            this.label33.TabIndex = 31;
            this.label33.Text = "Custom";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(23, 185);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(120, 13);
            this.label34.TabIndex = 30;
            this.label34.Text = "Torrent Website Search";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(23, 159);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(27, 13);
            this.label35.TabIndex = 29;
            this.label35.Text = "Size";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(23, 133);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(36, 13);
            this.label36.TabIndex = 28;
            this.label36.Text = "Genre";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(23, 107);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(42, 13);
            this.label37.TabIndex = 27;
            this.label37.Text = "Version";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(23, 81);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(50, 13);
            this.label38.TabIndex = 26;
            this.label38.Text = "Publisher";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(23, 55);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(56, 13);
            this.label39.TabIndex = 25;
            this.label39.Text = "Developer";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(23, 29);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(35, 13);
            this.label40.TabIndex = 24;
            this.label40.Text = "Name";
            // 
            // TPMovies
            // 
            this.TPMovies.BackColor = System.Drawing.SystemColors.Control;
            this.TPMovies.Controls.Add(this.label5);
            this.TPMovies.Controls.Add(this.label6);
            this.TPMovies.Controls.Add(this.MoviesDataGridView);
            this.TPMovies.Controls.Add(this.McbSelectedTable);
            this.TPMovies.Controls.Add(this.MtbSearch);
            this.TPMovies.Controls.Add(this.MpbPicture);
            this.TPMovies.Controls.Add(this.groupBox3);
            this.TPMovies.Location = new System.Drawing.Point(4, 29);
            this.TPMovies.Name = "TPMovies";
            this.TPMovies.Size = new System.Drawing.Size(997, 598);
            this.TPMovies.TabIndex = 2;
            this.TPMovies.Text = "Movies";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(485, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Search";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(232, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Table";
            // 
            // MoviesDataGridView
            // 
            this.MoviesDataGridView.AllowUserToAddRows = false;
            this.MoviesDataGridView.AllowUserToDeleteRows = false;
            this.MoviesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MoviesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MoviesDataGridView.Location = new System.Drawing.Point(232, 33);
            this.MoviesDataGridView.MultiSelect = false;
            this.MoviesDataGridView.Name = "MoviesDataGridView";
            this.MoviesDataGridView.ReadOnly = true;
            this.MoviesDataGridView.Size = new System.Drawing.Size(757, 553);
            this.MoviesDataGridView.TabIndex = 11;
            this.MoviesDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MoviesDataGridView_CellContentClick);
            this.MoviesDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MoviesDataGridView_CellDoubleClick);
            // 
            // McbSelectedTable
            // 
            this.McbSelectedTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.McbSelectedTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.McbSelectedTable.FormattingEnabled = true;
            this.McbSelectedTable.Location = new System.Drawing.Point(272, 8);
            this.McbSelectedTable.Name = "McbSelectedTable";
            this.McbSelectedTable.Size = new System.Drawing.Size(207, 21);
            this.McbSelectedTable.TabIndex = 10;
            this.McbSelectedTable.SelectedIndexChanged += new System.EventHandler(this.McbSelectedTable_SelectedIndexChanged);
            // 
            // MtbSearch
            // 
            this.MtbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MtbSearch.Location = new System.Drawing.Point(532, 8);
            this.MtbSearch.Name = "MtbSearch";
            this.MtbSearch.Size = new System.Drawing.Size(457, 20);
            this.MtbSearch.TabIndex = 9;
            this.MtbSearch.TextChanged += new System.EventHandler(this.MtbSearch_TextChanged);
            // 
            // MpbPicture
            // 
            this.MpbPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MpbPicture.Location = new System.Drawing.Point(8, 6);
            this.MpbPicture.Name = "MpbPicture";
            this.MpbPicture.Size = new System.Drawing.Size(218, 322);
            this.MpbPicture.TabIndex = 8;
            this.MpbPicture.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.MlbCustom);
            this.groupBox3.Controls.Add(this.MlbSubtitles);
            this.groupBox3.Controls.Add(this.MlbLanguage);
            this.groupBox3.Controls.Add(this.MlbGenre);
            this.groupBox3.Controls.Add(this.MlbLenght);
            this.groupBox3.Controls.Add(this.MlbSize);
            this.groupBox3.Controls.Add(this.MlbStudio);
            this.groupBox3.Controls.Add(this.MlbName);
            this.groupBox3.Controls.Add(this.label49);
            this.groupBox3.Controls.Add(this.label50);
            this.groupBox3.Controls.Add(this.label51);
            this.groupBox3.Controls.Add(this.label52);
            this.groupBox3.Controls.Add(this.label53);
            this.groupBox3.Controls.Add(this.label54);
            this.groupBox3.Controls.Add(this.label55);
            this.groupBox3.Controls.Add(this.label56);
            this.groupBox3.Location = new System.Drawing.Point(8, 334);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(218, 252);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Details";
            // 
            // MlbCustom
            // 
            this.MlbCustom.AutoSize = true;
            this.MlbCustom.Location = new System.Drawing.Point(155, 211);
            this.MlbCustom.Name = "MlbCustom";
            this.MlbCustom.Size = new System.Drawing.Size(41, 13);
            this.MlbCustom.TabIndex = 39;
            this.MlbCustom.Text = "label41";
            // 
            // MlbSubtitles
            // 
            this.MlbSubtitles.AutoSize = true;
            this.MlbSubtitles.Location = new System.Drawing.Point(155, 185);
            this.MlbSubtitles.Name = "MlbSubtitles";
            this.MlbSubtitles.Size = new System.Drawing.Size(41, 13);
            this.MlbSubtitles.TabIndex = 38;
            this.MlbSubtitles.Text = "label42";
            // 
            // MlbLanguage
            // 
            this.MlbLanguage.AutoSize = true;
            this.MlbLanguage.Location = new System.Drawing.Point(155, 159);
            this.MlbLanguage.Name = "MlbLanguage";
            this.MlbLanguage.Size = new System.Drawing.Size(41, 13);
            this.MlbLanguage.TabIndex = 37;
            this.MlbLanguage.Text = "label43";
            // 
            // MlbGenre
            // 
            this.MlbGenre.AutoSize = true;
            this.MlbGenre.Location = new System.Drawing.Point(155, 133);
            this.MlbGenre.Name = "MlbGenre";
            this.MlbGenre.Size = new System.Drawing.Size(41, 13);
            this.MlbGenre.TabIndex = 36;
            this.MlbGenre.Text = "label44";
            // 
            // MlbLenght
            // 
            this.MlbLenght.AutoSize = true;
            this.MlbLenght.Location = new System.Drawing.Point(155, 107);
            this.MlbLenght.Name = "MlbLenght";
            this.MlbLenght.Size = new System.Drawing.Size(41, 13);
            this.MlbLenght.TabIndex = 35;
            this.MlbLenght.Text = "label45";
            // 
            // MlbSize
            // 
            this.MlbSize.AutoSize = true;
            this.MlbSize.Location = new System.Drawing.Point(155, 81);
            this.MlbSize.Name = "MlbSize";
            this.MlbSize.Size = new System.Drawing.Size(41, 13);
            this.MlbSize.TabIndex = 34;
            this.MlbSize.Text = "label46";
            // 
            // MlbStudio
            // 
            this.MlbStudio.AutoSize = true;
            this.MlbStudio.Location = new System.Drawing.Point(155, 55);
            this.MlbStudio.Name = "MlbStudio";
            this.MlbStudio.Size = new System.Drawing.Size(41, 13);
            this.MlbStudio.TabIndex = 33;
            this.MlbStudio.Text = "label47";
            // 
            // MlbName
            // 
            this.MlbName.AutoSize = true;
            this.MlbName.Location = new System.Drawing.Point(155, 29);
            this.MlbName.Name = "MlbName";
            this.MlbName.Size = new System.Drawing.Size(41, 13);
            this.MlbName.TabIndex = 32;
            this.MlbName.Text = "label48";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(23, 211);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(42, 13);
            this.label49.TabIndex = 31;
            this.label49.Text = "Custom";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(23, 185);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(47, 13);
            this.label50.TabIndex = 30;
            this.label50.Text = "Subtitles";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(23, 159);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(55, 13);
            this.label51.TabIndex = 29;
            this.label51.Text = "Language";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(23, 133);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(36, 13);
            this.label52.TabIndex = 28;
            this.label52.Text = "Genre";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(23, 107);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(40, 13);
            this.label53.TabIndex = 27;
            this.label53.Text = "Lenght";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(23, 81);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(27, 13);
            this.label54.TabIndex = 26;
            this.label54.Text = "Size";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(23, 55);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(37, 13);
            this.label55.TabIndex = 25;
            this.label55.Text = "Studio";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(23, 29);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(35, 13);
            this.label56.TabIndex = 24;
            this.label56.Text = "Name";
            // 
            // TPSeries
            // 
            this.TPSeries.BackColor = System.Drawing.SystemColors.Control;
            this.TPSeries.Controls.Add(this.label7);
            this.TPSeries.Controls.Add(this.label8);
            this.TPSeries.Controls.Add(this.SeriesDataGridView);
            this.TPSeries.Controls.Add(this.SEcbSelectedTable);
            this.TPSeries.Controls.Add(this.SEtbSearch);
            this.TPSeries.Controls.Add(this.SEpbPicture);
            this.TPSeries.Controls.Add(this.groupBox4);
            this.TPSeries.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TPSeries.Location = new System.Drawing.Point(4, 29);
            this.TPSeries.Margin = new System.Windows.Forms.Padding(10);
            this.TPSeries.Name = "TPSeries";
            this.TPSeries.Size = new System.Drawing.Size(997, 598);
            this.TPSeries.TabIndex = 3;
            this.TPSeries.Text = "Series";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(485, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Search";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(232, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Table";
            // 
            // SeriesDataGridView
            // 
            this.SeriesDataGridView.AllowUserToAddRows = false;
            this.SeriesDataGridView.AllowUserToDeleteRows = false;
            this.SeriesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SeriesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SeriesDataGridView.Location = new System.Drawing.Point(232, 33);
            this.SeriesDataGridView.MultiSelect = false;
            this.SeriesDataGridView.Name = "SeriesDataGridView";
            this.SeriesDataGridView.ReadOnly = true;
            this.SeriesDataGridView.Size = new System.Drawing.Size(757, 553);
            this.SeriesDataGridView.TabIndex = 11;
            this.SeriesDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SeriesDataGridView_CellContentClick);
            this.SeriesDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SeriesDataGridView_CellDoubleClick);
            // 
            // SEcbSelectedTable
            // 
            this.SEcbSelectedTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.SEcbSelectedTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SEcbSelectedTable.FormattingEnabled = true;
            this.SEcbSelectedTable.Location = new System.Drawing.Point(272, 8);
            this.SEcbSelectedTable.Name = "SEcbSelectedTable";
            this.SEcbSelectedTable.Size = new System.Drawing.Size(207, 21);
            this.SEcbSelectedTable.TabIndex = 10;
            this.SEcbSelectedTable.SelectedIndexChanged += new System.EventHandler(this.SEcbSelectedTable_SelectedIndexChanged);
            // 
            // SEtbSearch
            // 
            this.SEtbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SEtbSearch.Location = new System.Drawing.Point(532, 8);
            this.SEtbSearch.Name = "SEtbSearch";
            this.SEtbSearch.Size = new System.Drawing.Size(457, 20);
            this.SEtbSearch.TabIndex = 9;
            this.SEtbSearch.TextChanged += new System.EventHandler(this.SEtbSearch_TextChanged);
            // 
            // SEpbPicture
            // 
            this.SEpbPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SEpbPicture.Location = new System.Drawing.Point(8, 6);
            this.SEpbPicture.Name = "SEpbPicture";
            this.SEpbPicture.Size = new System.Drawing.Size(218, 322);
            this.SEpbPicture.TabIndex = 8;
            this.SEpbPicture.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.SElbCustom);
            this.groupBox4.Controls.Add(this.SElbLanguage);
            this.groupBox4.Controls.Add(this.SElbNextSeasonDate);
            this.groupBox4.Controls.Add(this.SElbQuality);
            this.groupBox4.Controls.Add(this.SElbSeason);
            this.groupBox4.Controls.Add(this.SElbEpisode);
            this.groupBox4.Controls.Add(this.SElbStudio);
            this.groupBox4.Controls.Add(this.SElbName);
            this.groupBox4.Controls.Add(this.label65);
            this.groupBox4.Controls.Add(this.label66);
            this.groupBox4.Controls.Add(this.label67);
            this.groupBox4.Controls.Add(this.label68);
            this.groupBox4.Controls.Add(this.label69);
            this.groupBox4.Controls.Add(this.label70);
            this.groupBox4.Controls.Add(this.label71);
            this.groupBox4.Controls.Add(this.label72);
            this.groupBox4.Location = new System.Drawing.Point(8, 334);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(218, 252);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Details";
            // 
            // SElbCustom
            // 
            this.SElbCustom.AutoSize = true;
            this.SElbCustom.Location = new System.Drawing.Point(151, 210);
            this.SElbCustom.Name = "SElbCustom";
            this.SElbCustom.Size = new System.Drawing.Size(41, 13);
            this.SElbCustom.TabIndex = 39;
            this.SElbCustom.Text = "label57";
            // 
            // SElbLanguage
            // 
            this.SElbLanguage.AutoSize = true;
            this.SElbLanguage.Location = new System.Drawing.Point(151, 184);
            this.SElbLanguage.Name = "SElbLanguage";
            this.SElbLanguage.Size = new System.Drawing.Size(41, 13);
            this.SElbLanguage.TabIndex = 38;
            this.SElbLanguage.Text = "label58";
            // 
            // SElbNextSeasonDate
            // 
            this.SElbNextSeasonDate.AutoSize = true;
            this.SElbNextSeasonDate.Location = new System.Drawing.Point(151, 158);
            this.SElbNextSeasonDate.Name = "SElbNextSeasonDate";
            this.SElbNextSeasonDate.Size = new System.Drawing.Size(41, 13);
            this.SElbNextSeasonDate.TabIndex = 37;
            this.SElbNextSeasonDate.Text = "label59";
            // 
            // SElbQuality
            // 
            this.SElbQuality.AutoSize = true;
            this.SElbQuality.Location = new System.Drawing.Point(151, 132);
            this.SElbQuality.Name = "SElbQuality";
            this.SElbQuality.Size = new System.Drawing.Size(41, 13);
            this.SElbQuality.TabIndex = 36;
            this.SElbQuality.Text = "label60";
            // 
            // SElbSeason
            // 
            this.SElbSeason.AutoSize = true;
            this.SElbSeason.Location = new System.Drawing.Point(151, 106);
            this.SElbSeason.Name = "SElbSeason";
            this.SElbSeason.Size = new System.Drawing.Size(41, 13);
            this.SElbSeason.TabIndex = 35;
            this.SElbSeason.Text = "label61";
            // 
            // SElbEpisode
            // 
            this.SElbEpisode.AutoSize = true;
            this.SElbEpisode.Location = new System.Drawing.Point(151, 80);
            this.SElbEpisode.Name = "SElbEpisode";
            this.SElbEpisode.Size = new System.Drawing.Size(41, 13);
            this.SElbEpisode.TabIndex = 34;
            this.SElbEpisode.Text = "label62";
            // 
            // SElbStudio
            // 
            this.SElbStudio.AutoSize = true;
            this.SElbStudio.Location = new System.Drawing.Point(151, 54);
            this.SElbStudio.Name = "SElbStudio";
            this.SElbStudio.Size = new System.Drawing.Size(41, 13);
            this.SElbStudio.TabIndex = 33;
            this.SElbStudio.Text = "label63";
            // 
            // SElbName
            // 
            this.SElbName.AutoSize = true;
            this.SElbName.Location = new System.Drawing.Point(151, 28);
            this.SElbName.Name = "SElbName";
            this.SElbName.Size = new System.Drawing.Size(41, 13);
            this.SElbName.TabIndex = 32;
            this.SElbName.Text = "label64";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(19, 210);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(42, 13);
            this.label65.TabIndex = 31;
            this.label65.Text = "Custom";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(19, 184);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(55, 13);
            this.label66.TabIndex = 30;
            this.label66.Text = "Language";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(19, 158);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(90, 13);
            this.label67.TabIndex = 29;
            this.label67.Text = "Next season date";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(19, 132);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(39, 13);
            this.label68.TabIndex = 28;
            this.label68.Text = "Quality";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(19, 106);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(43, 13);
            this.label69.TabIndex = 27;
            this.label69.Text = "Season";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(19, 80);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(45, 13);
            this.label70.TabIndex = 26;
            this.label70.Text = "Episode";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(19, 54);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(37, 13);
            this.label71.TabIndex = 25;
            this.label71.Text = "Studio";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(19, 28);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(35, 13);
            this.label72.TabIndex = 24;
            this.label72.Text = "Name";
            // 
            // testingBindingSource
            // 
            this.testingBindingSource.DataMember = "Testing";
            this.testingBindingSource.DataSource = this.softwareDataSet;
            // 
            // softwareDataSet
            // 
            this.softwareDataSet.DataSetName = "SoftwareDataSet";
            this.softwareDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // testingTableAdapter
            // 
            this.testingTableAdapter.ClearBeforeFill = true;
            // 
            // timerTick
            // 
            this.timerTick.Enabled = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 677);
            this.Controls.Add(this.MainTC);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "DataBase";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.SizeChanged += new System.EventHandler(this.MainForm_SizeChanged);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.MainTC.ResumeLayout(false);
            this.TPSoftware.ResumeLayout(false);
            this.TPSoftware.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SoftwareDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpbPicture)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.TPGames.ResumeLayout(false);
            this.TPGames.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GamesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GpbPicture)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.TPMovies.ResumeLayout(false);
            this.TPMovies.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MoviesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MpbPicture)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.TPSeries.ResumeLayout(false);
            this.TPSeries.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SeriesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SEpbPicture)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.softwareDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TabControl MainTC;
        private System.Windows.Forms.TabPage TPSoftware;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView SoftwareDataGridView;
        private System.Windows.Forms.ComboBox ScbSelectTable;
        private System.Windows.Forms.TextBox StbSearch;
        private System.Windows.Forms.PictureBox SpbPicture;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage TPGames;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView GamesDataGridView;
        private System.Windows.Forms.ComboBox GcbSelectedTable;
        private System.Windows.Forms.TextBox GtbSearch;
        private System.Windows.Forms.PictureBox GpbPicture;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabPage TPMovies;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView MoviesDataGridView;
        private System.Windows.Forms.ComboBox McbSelectedTable;
        private System.Windows.Forms.TextBox MtbSearch;
        private System.Windows.Forms.PictureBox MpbPicture;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TabPage TPSeries;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView SeriesDataGridView;
        private System.Windows.Forms.ComboBox SEcbSelectedTable;
        private System.Windows.Forms.TextBox SEtbSearch;
        private System.Windows.Forms.PictureBox SEpbPicture;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label SlbCustom;
        private System.Windows.Forms.Label SlbTorrentWebSiteSearch;
        private System.Windows.Forms.Label SlbSize;
        private System.Windows.Forms.Label SlbSubCategory;
        private System.Windows.Forms.Label SlbVersion;
        private System.Windows.Forms.Label SlbPublisher;
        private System.Windows.Forms.Label SlbDeveloper;
        private System.Windows.Forms.Label SlbName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private SoftwareDataSet softwareDataSet;
        private System.Windows.Forms.BindingSource testingBindingSource;
        private DataBases.Extras.SoftwareDataSetTableAdapters.TestingTableAdapter testingTableAdapter;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataBaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem itemToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem tableToolStripMenuItem2;
        private System.Windows.Forms.Label GlbCustom;
        private System.Windows.Forms.Label GlbTorrentWebsiteSearch;
        private System.Windows.Forms.Label GlbSize;
        private System.Windows.Forms.Label GlbGenre;
        private System.Windows.Forms.Label GlbVersion;
        private System.Windows.Forms.Label GlbPublisher;
        private System.Windows.Forms.Label GlbDeveloper;
        private System.Windows.Forms.Label GlbName;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label MlbCustom;
        private System.Windows.Forms.Label MlbSubtitles;
        private System.Windows.Forms.Label MlbLanguage;
        private System.Windows.Forms.Label MlbGenre;
        private System.Windows.Forms.Label MlbLenght;
        private System.Windows.Forms.Label MlbSize;
        private System.Windows.Forms.Label MlbStudio;
        private System.Windows.Forms.Label MlbName;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label SElbCustom;
        private System.Windows.Forms.Label SElbLanguage;
        private System.Windows.Forms.Label SElbNextSeasonDate;
        private System.Windows.Forms.Label SElbQuality;
        private System.Windows.Forms.Label SElbSeason;
        private System.Windows.Forms.Label SElbEpisode;
        private System.Windows.Forms.Label SElbStudio;
        private System.Windows.Forms.Label SElbName;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.ToolStripMenuItem programToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pictureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel InternetConectivery;
        private System.Windows.Forms.ToolStripMenuItem recheckInternetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ChangePictureToolStripitem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameFolderToolStripMenuItem;
        private System.Windows.Forms.Timer timerTick;
        private System.Windows.Forms.ToolStripMenuItem addFromSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem combineToSeasonToolStripMenuItem;
    }
}

