﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace DataBase.Forms.Tools.CustomSelectors
{
    // TODO: Refactor This

    public partial class RadioButtonSelector : Form
    {
        string _result;
        string[] _dataToselectFrom;
        int _buttonLocaion = 50;
        int id = 1;
        int _buttonAmount = 0;


        /// <summary>
        /// Sets the text of the from
        /// </summary>
        public override string Text
        {
            get;
            set;
        }

        /// <summary>
        ///  Gets the result of the From
        /// </summary>
        public string Result
        {
            get
            {
                return _result;
            }
            private set
            {
                _result = value;
            }
        }
        
        public int AmountOfRadioButtons
        {
            get
            {
                return _buttonAmount;
            }
        }

        /// <summary>
        /// Select if the cancel button sould be eneabeld
        /// </summary>
        public bool CancelButtonEnabeld
        {
            get
            {
                return btnCancel.Enabled;
            }
            set
            {
                btnCancel.Enabled = value;
            }
        }


        /// <summary>
        /// This form gives you the option to choose from a selecton of string with radiobuttons
        /// </summary>
        /// <param name="dataToselectFrom">The array if sould select one item from</param>
        public RadioButtonSelector(string[] dataToselectFrom)
        {
            // Making sure that there Data in the string
            if (String.IsNullOrEmpty(dataToselectFrom[0]) && String.IsNullOrEmpty(dataToselectFrom[1]))
            {
                throw new ArgumentNullException();
            }

            _dataToselectFrom = dataToselectFrom;

            InitializeComponent();
        }



        private void RadioButtonSelector_Load(object sender, EventArgs e)
        {
            foreach (string data in _dataToselectFrom)
            {
                // Creating the radio button
                RadioButton radioButton = new RadioButton();

                // Set the propoties of the the radio button
                radioButton.Name = "rb" + id.ToString();
                radioButton.Text = data;
                radioButton.Location = new Point(12, _buttonLocaion);

                // Addding the Radiobutton
                this.Controls.Add(radioButton);

                // Increaseing the id
                id++;

                // Adding a nother refrance to the button
                _buttonAmount++;

                // Adding the button location
                _buttonLocaion = _buttonLocaion + 23;

                // Move the buttons down
                btnCancel.Location = new Point(btnCancel.Location.X, btnCancel.Location.Y + 23);
                btnDone.Location = new Point(btnDone.Location.X, btnDone.Location.Y + 23);

                // Lower the form so everything can fit
                this.Height = this.Height + 23;

            }

            // Makeing sure that the first one or anyone for that matter is checked
            this.Controls.OfType<RadioButton>().ToArray()[0].Checked = true;
        }


        // Button Done - When we are done selecting what we need
        private void btnDone_Click(object sender, EventArgs e)
        {
            // Going true all the radio buttons
            foreach (RadioButton rb in this.Controls.OfType<RadioButton>())
            {
                // Checking if the button was selected
                if (rb.Checked == true)
                {
                    // If it was selected than return the selected result and break
                    _result = rb.Text;
                    break;
                }
            }

            // Close the form
            this.DialogResult = DialogResult.OK;
        }


        // When we want to cancel
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
